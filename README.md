# Signal Data API

This RESTful API server subscribes to the message broker. Which uses [metric generator](https://gitlab.com/ska-telescope/ska-sdp-qa-metric-generator) for submitting Signal data. This is used by the [Signal display](https://gitlab.com/ska-telescope/sdp/ska-sdp-qa-display). It is also used to generate the waterfall images of the spectrogram plots.

For more information please have a look at the [Read The Docs](https://developer.skao.int/projects/ska-sdp-qa-data-api/en/latest/index.html) page.

## Tooling Pre-requisites

Below are some tools that will be required to work with the Signal Data API:
- Python 3.10 or later versions: Install page URL: https://www.python.org/downloads/
- Poetry 1.2 or later versions: Install page URL: https://python-poetry.org/docs/#installation
- GNU make 4.2 or later versions: Install page URL: https://www.gnu.org/software/make/
- Docker 20.10 or later versions: Install page URL: https://docs.docker.com/engine/install/

## Getting Started

### Docker Setup

This is the preferred setup, everything comes up with just a single docker command.

To run the API and all its dependent systems you need to use Docker.

To get everything running run the following:

```bash
make run
```

### Local Running

If however you prefer to run things locally, then at the minimum you will need the
dependencies running in Docker.

To do that run the following:

```bash
make run-services
```

And then use the following to bring up the API:

```bash
make run-display-api
```

## Dependency Management

To manage the application's dependencies we use `poetry`. The following is some useful quick commands:

* `poetry shell` - create a new shell with a virtual environment
* `poetry install` - install the applications requirements locally. This can also be used to update the lock file.
* `poetry update` - update all libraries that still fit within the requirements.

## Helpful notes

### Linting

```bash
make python-format
```

Or run the linters with:

```bash
make python-lint
```

### Run the tests

To run the tests you need to have poetry installed on your system:

```bash
poetry install
poetry run make python-test
```

### API Endpoints

To get the endpoints provided by the api look at the files located under /src/ska_sdp_qa_data_api/server/api and main.py file under src/ska_sdp_qa_data_api/server.

### Prod vs Dev

When running the docker version of the Signal Data API, there are two modes that can be run. This is configurable in the docker-compose.yml file.
The variable that needs to be changed is services.qa-data-api.build.target. The two values are:
    - prod
        This is used when you are using the API when testing the other components of the Signal Metrics workflow.
        Prod has no code reload. And has a configurable amount of workers, defaulted 2.
    - dev
        This is used when testing changes to the Signal Data API. It can hot reload code but only has one worker.

## Helm Chart Usage

There is a helm chart that can be used to spin up this repo and it's dependencies. This chart can be found in one of two
places. Either in this repo in `charts/ska-sdp-qa`, or in the SKA helm chart library.

### Pre-requisities

Before being able to use this chart you require the following setup:

* [Docker](https://docs.docker.com/get-docker/)
* A kubernetes cluster. (For example [minikube](https://minikube.sigs.k8s.io/docs/start/))
* The [Kubernetes CLI tool](https://minikube.sigs.k8s.io/docs/start/)
* The [Helm CLI tool](https://helm.sh/docs/intro/install/.
* Have the SKA and Bitnami helm repos added:
    * SKA: `helm repo add ska https://artefact.skao.int/repository/helm-internal`
    * Bitnami: `helm repo add bitnami https://charts.bitnami.com/bitnami`

### Usage of local chart

To use the local chart you can run the following steps.

1. Make sure you have a namespace setup. `kubectl create namespace qatest`
2. Update the charts dependencies: `helm dependency build charts/ska-sdp-qa`
3. Install using default settings: `helm install qa-api charts/ska-sdp-qa --namespace qatest --wait` (remove the `--wait` to run in the background)

This should provide you with the following output:

```
NAME: qa-api
LAST DEPLOYED: Fri Jan 13 08:55:27 2023
NAMESPACE: qatest
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
The Signal Display UI can be found at: <host>/qatest/qa/display/

<host> is whatever the cluster domain name is. Common links:
1. http://<cluster IP>/qatest/qa/display/ if on your local minikube
2. https://sdhp.stfc.skao.int/qatest/qa/display/ if on the shared cluster

The Signal API can be found at: <host>/qatest/qa/api/

<host> is whatever the cluster domain name is. Common links:
1. http://<cluster IP>/qatest/qa/api/ if on your local minikube
2. https://sdhp.stfc.skao.int/qatest/qa/api/ if on the shared cluster

The Kafka host is currently set to: ska-sdp-qa-kafka.qatest.svc:9092
The Redis host is currently set to: ska-sdp-qa-redis-headless.qatest.svc

```

### Usage of released chart

Usage of the released version of the Signal chart is simpler but requires the SKA repo to be enabled in
helm before hand.

1. Create a namespace to deploy into: `kubectl create namespace qatest`
2. Install the latest version of the chart: `helm install qa-api ska/ska-sdp-qa --namespace qatest --wait` (remove the `--wait` to run in the background)

The output should be the same as above.

### Uninstall chart

When you no longer need the chart installed run the following:

1. Remove chart: `helm uninstall qa-api --namespace qatest`
2. Remove namespace: `kubectl delete namespace qatest`

## Link to Running Instance

[Click here](https://sdhp.stfc.skao.int/dp-naledi/qa/api/ping) for a link to the running instance of the API.
