include .make/base.mk
include .make/python.mk
include .make/oci.mk
include .make/helm.mk
include .make/dependencies.mk

.PHONY: build run

PROJECT_NAME = ska-sdp-qa-data-api
PROJECT_PATH = ska-telescope/sdp/ska-sdp-qa-data-api
ARTEFACT_TYPE = oci

PYTHON_VARS_AFTER_PYTEST = --cov-fail-under=85 --no-cov-on-fail -vv

PYTHON_LINE_LENGTH = 99
HELM_CHARTS_TO_PUBLISH := ska-sdp-qa
DATA_FILE ?= data.msgpack

REDIS_HOST=localhost
BROKER_INSTANCE=localhost:29092
NAMESPACE ?= dp-naledi

RECEIVE_PROCESSOR ?= registry.gitlab.com/ska-telescope/sdp/ska-sdp-realtime-receive-processors/ska-sdp-realtime-receive-processors:1e10a06e

export REDIS_HOST?=
export BROKER_INSTANCE?=


ifeq ($(MAKECMDGOALS),run-all-tasks)
MAKEFLAGS+=-j10
endif

# Run the application in a virtual environment on the host for development use.
run-display-api:
	uvicorn ska_sdp_qa_data_api.server.main:app --reload --port 8002 --host 0.0.0.0 --app-dir ./src


################# TASKS ###########################
run-task-send-data-to-kafka:
	signal-task-send-to-kafka --topic metrics-visibility-01 ${DATA_FILE}

run-task-receive-stats:
	signal-task-receive-stats --verbose

run-task-receive-spead2:
	signal-task-receive-spead2 --verbose

run-task-receive-waterfall-spectrogram:
	signal-task-receive-waterfall-spectrogram --verbose

run-task-receive-spectrum-waterfalls:
	signal-task-receive-spectrum-waterfalls --verbose

run-task-receive-waterfall-lag:
	signal-task-receive-waterfall-lag --verbose

run-task-calibration:
	signal-task-process-pointing-calibration-receive --verbose

run-task-gain-calibration:
	signal-task-process-gain-calibration-receive --verbose

run-task-redis-clean:
	signal-task-redis-clean --verbose

run-all-tasks: run-task-receive-stats run-task-receive-spead2 run-task-receive-waterfall-spectrogram run-task-receive-waterfall-lag run-task-calibration run-task-gain-calibration

################# TASKS ###########################

build:
	docker compose pull
	docker compose build

run: build run-services dev-services-setup
	docker compose up -d

run-services:
	docker compose up -d redis broker zookeeper etcd

logs:
	docker compose logs --follow cron api

clean:
	docker compose down --rmi all


dev-services-setup:
	sleep 1
	bash data_entries.sh
	sleep 1
	docker compose exec broker bash -c './opt/bitnami/kafka/bin/kafka-topics.sh --create --bootstrap-server localhost:29092 --topic metrics-amplitude-01 --partitions 5'
	docker compose exec broker bash -c './opt/bitnami/kafka/bin/kafka-topics.sh --create --bootstrap-server localhost:29092 --topic metrics-bandaveragedxcorr-01 --partitions 5'
	docker compose exec broker bash -c './opt/bitnami/kafka/bin/kafka-topics.sh --create --bootstrap-server localhost:29092 --topic metrics-lagplot-01 --partitions 5'
	docker compose exec broker bash -c './opt/bitnami/kafka/bin/kafka-topics.sh --create --bootstrap-server localhost:29092 --topic metrics-phase-01 --partitions 5'
	docker compose exec broker bash -c './opt/bitnami/kafka/bin/kafka-topics.sh --create --bootstrap-server localhost:29092 --topic metrics-spectrum-01 --partitions 5'
	docker compose exec broker bash -c './opt/bitnami/kafka/bin/kafka-topics.sh --create --bootstrap-server localhost:29092 --topic metrics-stats-01 --partitions 1'
	docker compose exec broker bash -c './opt/bitnami/kafka/bin/kafka-topics.sh --create --bootstrap-server localhost:29092 --topic metrics-uvcoverage-01 --partitions 5'
	docker compose exec broker bash -c './opt/bitnami/kafka/bin/kafka-topics.sh --create --bootstrap-server localhost:29092 --topic metrics-receiverstats-01 --partitions 1'

run-observation:
	docker run --network=ska-sdp-qa-metric-network -it \
		-v $$(pwd)/data:/data \
		-e SDP_CONFIG_HOST=etcd \
		-e SDP_PB_ID=pb-testrealtime-20240913-95421 \
		--shm-size 2gb\
		${RECEIVE_PROCESSOR} \
		realtime.receive.processors.sdp.metrics.signal_display_metrics.SignalDisplayMetrics --metrics all --input /data/61MB.ms