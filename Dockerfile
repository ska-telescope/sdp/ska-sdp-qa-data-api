FROM artefact.skao.int/ska-build-python:0.1.1 AS builder

ENV POETRY_NO_INTERACTION=1
ENV POETRY_VIRTUALENVS_IN_PROJECT=1
ENV POETRY_VIRTUALENVS_CREATE=1

RUN pip install --user --upgrade poetry>=2.0.1

WORKDIR /src
COPY pyproject.toml poetry.lock ./

# Install dependencies
RUN poetry install --only main --no-root

COPY src ./src

RUN poetry install --only main

FROM artefact.skao.int/ska-python:0.1.2 AS runner

ENV VIRTUAL_ENV=/src/.venv
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
ENV PYTHONPATH="$PYTHONPATH:/src/"
ENV PYTHONUNBUFFERED=1
ENV TZ=Etc/UTC
ENV WATERFALL_DEFAULT_IMAGE=/src/ska_sdp_qa_data_api/server/utils/Images/default_image.bmp

# User Setup
ARG USERNAME=signal_display
ARG USER_UID=1000
ARG USER_GID=1000

RUN groupadd --gid ${USER_GID} ${USERNAME} \
    && useradd -s /bin/bash --uid ${USER_UID} --gid ${USER_GID} -m ${USERNAME}

USER ${USERNAME}

COPY --from=builder ${VIRTUAL_ENV} ${VIRTUAL_ENV}

COPY crontab-entry.sh /crontab-entry.sh

WORKDIR /src

COPY src/ska_sdp_qa_data_api ./ska_sdp_qa_data_api

FROM runner AS dev

CMD ["uvicorn", "ska_sdp_qa_data_api.server.main:app", "--reload", "--host", "0.0.0.0", "--port", "8002", "--app-dir", "/usr/src"]

FROM runner AS prod

ENV WORKER_COUNT=2

# Using "bash -c" for the variable expansion, and without a single string uvicorn dies
CMD ["bash", "-c", "uvicorn ska_sdp_qa_data_api.server.main:app --workers ${WORKER_COUNT} --host 0.0.0.0 --port 8002"]
