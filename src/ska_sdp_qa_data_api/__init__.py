# -*- coding: utf-8 -*-
#
# This file is part of the ska_sdp_qa_data_api project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""
This package contains the ska_sdp_qa_data_api
"""


__version__ = "1.0.1"
