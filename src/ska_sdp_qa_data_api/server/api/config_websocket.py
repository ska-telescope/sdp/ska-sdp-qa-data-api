"""This is an interactive websocket for requesting information.

Current examples of requests:
```
{"action":"get", "type":"list_subarrays"}
{"action":"get", "type":"list_processing_blocks"}
{"action":"get", "type":"list_execution_blocks"}
{"action":"get", "type":"list_deployments"}
{"action":"get", "type":"subarray", "id":"01"}
{"action":"get", "type":"subarray_state", "id":"01"}
{"action":"get", "type":"processing_block", "id":"pb-testrealtime-20240812-42925"}
{"action":"get", "type":"processing_block_state", "id":"pb-testrealtime-20240812-42925"}
{"action":"get", "type":"execution_block", "id":"eb-test-20240812-42925"}
{"action":"get", "type":"deployment", "id":"proc-pb-testrealtime-20240812-42925-script"}
{"action":"get", "type":"deployment_state", "id":"proc-pb-testrealtime-20240812-42925-script"}

{"action":"config"}

{"action":"listen", "type":"list_subarrays"}
{"action":"listen", "type":"list_processing_blocks"}
{"action":"listen", "type":"list_execution_blocks"}
{"action":"listen", "type":"list_deployments"}
{"action":"listen", "type":"subarray", "id":"01"}
{"action":"listen", "type":"subarray_state", "id":"01"}
{"action":"listen", "type":"processing_block", "id":"pb-testrealtime-20240812-42925"}
{"action":"listen", "type":"processing_block_state", "id":"pb-testrealtime-20240812-42925"}
{"action":"listen", "type":"execution_block", "id":"eb-test-20240812-42925"}
{"action":"listen", "type":"deployment", "id":"proc-pb-testrealtime-20240812-42925-script"}
{"action":"listen", "type":"deployment_state", "id":"proc-pb-testrealtime-20240812-42925-script"}
```

"""

import json
import logging
import typing

from fastapi import APIRouter, WebSocket
from starlette.endpoints import WebSocketEndpoint

from ska_sdp_qa_data_api.server.utils.config_db import SDPConfig
from ska_sdp_qa_data_api.server.utils.helpers import get_system_config, recursive_to_dict
from ska_sdp_qa_data_api.server.utils.stats import WebsocketStats

websocket_stats = WebsocketStats()

config_ws = APIRouter()

logger = logging.getLogger(__name__)
sdp_config = SDPConfig()


@config_ws.websocket_route("/ws")
class WebsocketConsumer(WebSocketEndpoint):
    """
    Consume messages from <topicname>
    This will start a Kafka Consumer from a topic
    And this path operation will:
    * return ConsumerResponse
    """

    async def on_connect(self, websocket: WebSocket) -> None:
        """Websocket connect event"""
        # pylint: disable=attribute-defined-outside-init
        await websocket.accept()

        await websocket.send_json({"status": "connected"})
        websocket_stats.add_websocket(websocket, "ws")
        websocket_stats.access_websocket(websocket, "ws")

        # self.consumer_task = asyncio.create_task(self.send_consumer_message(websocket=websocket))

        logger.info("consumer_ctl:on_connect: connected")

    async def on_disconnect(self, websocket: WebSocket, close_code: int) -> None:
        """If the client disconnects, stop listening to Kafka."""
        websocket_stats.remove_websocket(websocket, "/ws")
        logger.info("consumer_ctl:on_disconnect: stopped, %d", close_code)

    async def on_receive(self, websocket: WebSocket, data: typing.Any) -> None:
        """Respond with received message."""
        try:
            json_data = json.loads(data)
        except Exception:  # pylint: disable=broad-exception-caught
            await websocket.send_json({"error": "JSON cannot be parsed"})
            return

        if not isinstance(json_data, dict):
            await websocket.send_json({"error": "Invalid JSON structure"})
            return
        action = json_data.get("action", None)

        output = None
        success = False

        if action == "get":
            success, output = await self._action_get(json_data)
        elif action == "config":
            success, output = True, get_system_config()
        elif action == "listen":
            success, output = await self._action_listen(websocket, json_data)
        else:
            success, output = False, {"error": "Invalid 'action' specified"}

        await websocket.send_json(
            {
                "event_type": "action_call",
                "event_input": json_data,
                "success": success and output is not None,
                "data": recursive_to_dict(output),
            }
        )
        websocket_stats.access_websocket(websocket, "ws")

    async def _action_get(self, input_data: dict) -> tuple[bool, dict]:
        obj_type = input_data.get("type", None)
        obj_id = input_data.get("id", None)

        if obj_type is None:
            return False, {"error": "missing parameter 'type'"}

        try:
            sdp_config.reconnect()
            return True, sdp_config.get_from_type_dict(obj_type, obj_id)
        except ValueError as ve:
            return False, {"error": str(ve)}
        except Exception:  # pylint: disable=broad-except
            return False, {"error": "unknown"}

    async def _action_listen(self, websocket: WebSocket, input_data: dict):
        obj_type = input_data.get("type", None)
        obj_id = input_data.get("id", None)

        sdp_config.reconnect()
        await sdp_config.watcher_thread(
            obj_type,
            obj_id,
            lambda type_name, type_id, event: self._receive_config_db_event(
                websocket, {"type": type_name, "id": type_id}, event
            ),
        )

        return True, {}

    async def _receive_config_db_event(self, websocket: WebSocket, type_info, event_data):
        output = recursive_to_dict(
            {"event": "listen_event", "event_type": type_info, "data": event_data}
        )

        await websocket.send_json(output)
        websocket_stats.access_websocket(websocket, "ws")
