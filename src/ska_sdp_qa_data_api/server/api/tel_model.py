"""Calls to the tel-model repo."""

import json
import os
from pathlib import Path

from fastapi import APIRouter

tel_model_api = APIRouter()


@tel_model_api.get(
    "/mask_data/{scan_id}/{subarray}", tags=["tel-model"], summary="Retrieve mask data"
)
def get_mask_data():
    """Returns mask data."""
    file_path = f"{os.getcwd()}/server/data/mock_data/rfi_mask.json"
    if not Path(file_path).exists():
        file_path = f"{os.getcwd()}/src/ska_sdp_qa_data_api/server/data/mock_data/rfi_mask.json"

    with open(file_path, "r", encoding="UTF-8") as f:
        return json.loads(f.read())
