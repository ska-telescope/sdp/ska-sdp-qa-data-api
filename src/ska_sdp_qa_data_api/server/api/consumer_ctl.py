"""
This will start a Kafka Consumer from a topic
"""

import asyncio
import json
import logging

import msgpack
from fastapi import APIRouter, WebSocket
from ska_sdp_dataqueues import DataQueueConsumer
from starlette.endpoints import WebSocketEndpoint

from ska_sdp_qa_data_api.server.core.config import API_DATA_FORMAT, BROKER_INSTANCE
from ska_sdp_qa_data_api.server.core.message_topic import (
    AMPLITUDE,
    BAND_AVERAGED_X_CORR,
    GAIN_CALIBRATION_OUT,
    LAG_PLOT,
    PHASE,
    PHASE_AND_AMPLITUDE,
    POINTING_OFFSET_OUT,
    RECEIVER_STATS,
    RFI,
    SPECTRUM,
    UV_COVERAGE,
    VISIBILITY_STATS,
)
from ska_sdp_qa_data_api.server.utils.stats import WebsocketStats

consumer_api = APIRouter()

logger = logging.getLogger(__name__)
websocket_stats = WebsocketStats()


# is_json flag is for workflow and stats topics.
# These topics will be changed to msgpack at a later stage.
async def _send(websocket, data, is_json=False):
    if is_json or API_DATA_FORMAT == "json":
        await websocket.send_json(data)
    else:
        await websocket.send_bytes(msgpack.packb(data))


@consumer_api.websocket_route("/{topicname}")
@consumer_api.websocket_route("/{topicname}/{partition}")
class WebsocketConsumer(WebSocketEndpoint):
    """
    Consume messages from <topicname>
    This will start a Kafka Consumer from a topic
    And this path operation will:
    * return ConsumerResponse
    """

    async def on_connect(self, websocket: WebSocket) -> None:
        path_splits = websocket["path"].split("/")
        topicname = path_splits[2]
        if len(path_splits) == 4:
            partition = int(path_splits[3])
        else:
            partition = 0

        # pylint: disable=attribute-defined-outside-init
        logger.warning("consumer_ctl: topicname = %s | partitions = %d", topicname, partition)
        self._data_queue = None
        await websocket.accept()

        topicname_test = "-".join(topicname.split("-")[:2])
        if topicname_test not in [
            f"metrics-{PHASE_AND_AMPLITUDE}",
            f"metrics-{SPECTRUM}",
            f"metrics-{RFI}",
            f"metrics-{PHASE}",
            f"metrics-{AMPLITUDE}",
            f"metrics-{POINTING_OFFSET_OUT}",
            f"metrics-{LAG_PLOT}",
            f"metrics-{UV_COVERAGE}",
            f"metrics-{GAIN_CALIBRATION_OUT}",
            f"metrics-{BAND_AVERAGED_X_CORR}",
            f"metrics-{VISIBILITY_STATS}",
            f"metrics-{RECEIVER_STATS}",
        ]:
            await _send(
                websocket,
                {
                    "status": "error",
                    "message": f"You cannot connect to {topicname}",
                },
            )
            await websocket.close()
            return

        self._topics = [(topicname, partition)]
        websocket_stats.add_websocket(websocket, topicname)
        await _send(
            websocket,
            {"status": "connected"},
            topicname_test in [f"metrics-{VISIBILITY_STATS}", f"metrics-{RECEIVER_STATS}"],
        )

        self._data_queue = DataQueueConsumer(
            server=BROKER_INSTANCE,
            set_group_id=False,
            auto_offset_reset="latest",
            set_consumer_offsets=False,
        )

        self.consumer_task = asyncio.create_task(self.send_consumer_message(websocket=websocket))

        logger.info("consumer_ctl:on_connect: connected")

    async def on_disconnect(self, websocket: WebSocket, close_code: int) -> None:
        """If the client disconnects, stop listening to Kafka."""
        # logger.info("counter: %d", self.counter)
        logger.info("consumer_ctl:on_disconnect: stopped: %d", close_code)
        websocket_stats.remove_websocket(websocket, websocket["path"].split("/")[2])

    async def send_consumer_message(self, websocket: WebSocket) -> None:
        """Send consumer message."""
        logger.info("consumer_ctl:send_consumer_message")

        async with self._data_queue:
            try:
                await self._data_queue.assign_topics(self._topics)
                async for topic, msg in self._data_queue:
                    try:
                        websocket_stats.access_websocket(websocket, topic)
                        if (
                            topic.startswith(f"metrics-{VISIBILITY_STATS}")
                            or topic.startswith(f"metrics-{RECEIVER_STATS}")
                            or API_DATA_FORMAT == "json"
                        ):
                            await websocket.send_json(json.loads(msg))
                        else:
                            await websocket.send_bytes(msg)
                    except Exception as err:  # pylint: disable=W0703
                        logger.exception(err)
            except Exception as err:  # pylint: disable=W0703
                logger.exception(err)
                raise
            finally:
                logger.info("consumer_ctl: disconnected")
