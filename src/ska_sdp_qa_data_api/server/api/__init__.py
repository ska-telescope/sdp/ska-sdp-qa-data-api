# -*- coding: utf-8 -*-
#
# This file is part of the ska_sdp_qa_data_api project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""
This subpackage contains the ska_sdp_qa_data_api.server.api
"""
from fastapi import APIRouter

from ska_sdp_qa_data_api.server.api.config import config_api
from ska_sdp_qa_data_api.server.api.config_websocket import config_ws
from ska_sdp_qa_data_api.server.api.consumer_ctl import consumer_api
from ska_sdp_qa_data_api.server.api.core import core_api
from ska_sdp_qa_data_api.server.api.tel_model import tel_model_api
from ska_sdp_qa_data_api.server.api.waterfall import waterfall_api
from ska_sdp_qa_data_api.server.api.windows import window_api

router = APIRouter()
router.include_router(core_api)
router.include_router(config_ws, prefix="/ws")
router.include_router(consumer_api, prefix="/ws")
router.include_router(waterfall_api, prefix="/spectrograms")
router.include_router(waterfall_api, prefix="/lag_plots")
router.include_router(waterfall_api, prefix="/spectrum_waterfalls")
router.include_router(config_api, prefix="/config")
router.include_router(tel_model_api, prefix="/tel_model")
router.include_router(window_api, prefix="/windows")
