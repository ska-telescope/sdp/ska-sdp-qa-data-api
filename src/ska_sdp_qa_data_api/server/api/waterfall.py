"""Provide the endpoints for the waterfall plot images.

Note that these endpoints do not use the async operator, so that we can take
advantage of the python threading in fastapi/uvicorn.
"""

# pylint: disable=too-many-arguments,too-many-positional-arguments, no-member

import logging

import orjson
from fastapi import APIRouter, Request
from fastapi.responses import Response
from redis import Redis

from ska_sdp_qa_data_api.server.core.config import API_URL_SUBDIRECTORY, REDIS_HOST, REDIS_PORT
from ska_sdp_qa_data_api.server.utils.helpers import slugify
from ska_sdp_qa_data_api.server.utils.waterfall_plots import WaterfallPlots

waterfall_api = APIRouter()

logger = logging.getLogger(__name__)

HEADERS = {"Cache-Control": "no-store", "SameSite": "None"}

waterfall_plots = WaterfallPlots(Redis(host=REDIS_HOST, port=REDIS_PORT, db=0))


def _get_type(request: Request):
    """Get the type of request based on URL"""
    return request.url.path.removeprefix(API_URL_SUBDIRECTORY).split("/")[1]


def _data_or_nothing(key: str, start=None, end=None) -> Response:
    try:
        image_data = orjson.dumps(
            waterfall_plots.retrieve_np_from_timeseries(key, start=start, end=end).tolist()
        )
        return Response(content=image_data, media_type="application/json", headers=HEADERS)
    except Exception as te_exc:  # pylint: disable=W0718
        logger.exception(te_exc)
        image_data = orjson.dumps([])
        return Response(image_data, media_type="application/json", headers=HEADERS)


@waterfall_api.get("/delta_thumbnail_pb/{processing_block}/{antenna1}/{antenna2}/{polarisation}")
def send_delta_thumbnail_array(
    request: Request,
    processing_block: str,
    antenna1: str,
    antenna2: str,
    polarisation: str,
    start: str = None,
    end: str = None,
):
    """Returns specific thumbnail array for given antenna and polarisation
    combination."""
    processing_block = slugify(text=processing_block)
    antenna1 = slugify(text=antenna1)
    antenna2 = slugify(text=antenna2)
    antenna1, antenna2 = min(antenna1, antenna2), max(antenna1, antenna2)
    polarisation = slugify(text=polarisation)
    img_type = _get_type(request)
    if img_type == "lag_plots":
        key = f"{processing_block}_{antenna1}_{antenna2}_{polarisation}_lagplots"
    else:
        # img_type == "spectrograms"
        key = f"{processing_block}_{antenna1}_{antenna2}_{polarisation}"

    return _data_or_nothing(key, start, end)


@waterfall_api.get("/thumbnail_pb/{processing_block}/{antenna1}/{antenna2}/{polarisation}")
def send_thumbnail_array(
    request: Request, processing_block: str, antenna1: str, antenna2: str, polarisation: str
):
    """Returns specific thumbnail array for given antenna and polarisation
    combination."""
    processing_block = slugify(text=processing_block)
    antenna1 = slugify(text=antenna1)
    antenna2 = slugify(text=antenna2)
    polarisation = slugify(text=polarisation)
    img_type = _get_type(request)
    if img_type == "lag_plots":
        key = f"{processing_block}_{antenna1}_{antenna2}_{polarisation}_lagplots"
    else:
        # img_type == "spectrograms"
        key = f"{processing_block}_{antenna1}_{antenna2}_{polarisation}"

    return _data_or_nothing(key)


@waterfall_api.get("/thumbnail_pb/{processing_block}/{polarisation}", tags=["image"])
def send_spectrum_thumbnail_array(request: Request, processing_block: str, polarisation: str):
    """Returns specific thumbnail array for given polarisation."""
    processing_block = slugify(text=processing_block)
    polarisation = slugify(text=polarisation)

    img_type = _get_type(request)
    if img_type == "spectrum_waterfalls":
        key = f"{processing_block}_{polarisation}_spectrum_waterfalls"
    else:
        image_data = orjson.dumps([])
        return Response(image_data, media_type="application/json", headers=HEADERS)

    return _data_or_nothing(key)


@waterfall_api.get("/delta_thumbnail_pb/{processing_block}/{polarisation}")
def send_spectrum_delta_thumbnail_array(
    request: Request, processing_block: str, polarisation: str, start: str = None, end: str = None
):
    """Returns specific thumbnail array for given polarisation."""
    processing_block = slugify(text=processing_block)
    polarisation = slugify(text=polarisation)

    img_type = _get_type(request)
    if img_type == "spectrum_waterfalls":
        key = f"{processing_block}_{polarisation}_spectrum_waterfalls"
    else:
        image_data = orjson.dumps([])
        return Response(image_data, media_type="application/json", headers=HEADERS)

    return _data_or_nothing(key, start, end)
