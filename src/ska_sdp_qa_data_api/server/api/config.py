"""New endpoints to read from the config DB"""

from itertools import combinations_with_replacement, product

from fastapi import APIRouter, HTTPException

from ska_sdp_qa_data_api.server.data.inputs import (
    RE_DEPLOYMENT,
    RE_EXECUTION_BLOCK,
    RE_PROCESSING_BLOCK,
    RE_SUBARRAY,
)
from ska_sdp_qa_data_api.server.data.responses import (
    ProcessingBlock,
    ProcessingBlockState,
    Subarray,
)
from ska_sdp_qa_data_api.server.utils.config_db import SDPConfig

config_api = APIRouter()

config = SDPConfig()


@config_api.get("/subarrays", tags=["config"])
async def get_subarrays() -> list[str]:
    """Read the configured subarrays from the config DB"""
    config.reconnect()

    return config.subarrays()


@config_api.get("/subarrays/{subarray}", tags=["config"])
async def get_subarray(subarray: str) -> Subarray:
    """Read the config for a specific subarray"""
    if not RE_SUBARRAY.match(subarray):
        raise ValueError("subarray doesn't match expected format")
    config.reconnect()
    data = config.subarray(subarray)
    if data is None:
        raise HTTPException(status_code=404, detail="Subarray doesn't exist")
    return data


@config_api.get("/subarrays/{subarray}/current_setup", tags=["config"])
async def get_subarray_state(subarray: str) -> dict:
    """Shortcut function to get the latest details about the given subarray."""
    if not RE_SUBARRAY.match(subarray):
        raise ValueError("subarray doesn't match expected format")
    config.reconnect()
    try:
        output_data = config.subarray_running_state(subarray)
        if output_data is None:
            return {}
        return output_data
    except ValueError as v_err:
        # pylint: disable=raise-missing-from
        raise HTTPException(status_code=404, detail=str(v_err))


@config_api.get("/processing_blocks", tags=["config"])
async def get_processing_blocks() -> list[str]:
    """Get the list of currently in progress processing blocks"""
    config.reconnect()
    return config.processing_blocks()


@config_api.get("/processing_blocks/{processing_block}", tags=["config"])
async def get_processing_block(processing_block: str) -> ProcessingBlock:
    """Get a specific processing block"""
    if not RE_PROCESSING_BLOCK.match(processing_block):
        raise ValueError("processing block doesn't match expected format")
    config.reconnect()
    data = config.processing_block(processing_block)
    if data is None:
        raise HTTPException(status_code=404, detail="Processing Block doesn't exist")
    return data


@config_api.get(
    "/processing_blocks/{processing_block}/state",
    tags=["config"],
)
async def get_processing_block_state(processing_block: str) -> ProcessingBlockState:
    """Get a specific processing block's state"""
    if not RE_PROCESSING_BLOCK.match(processing_block):
        raise ValueError("processing block doesn't match expected format")
    config.reconnect()
    data = config.processing_block_state(processing_block)
    if data is None:
        raise HTTPException(status_code=404, detail="Processing Block doesn't exist")
    return data


@config_api.get("/execution_blocks", tags=["config"])
async def get_execution_blocks() -> list[str]:
    """Get a list of known execution blocks"""
    config.reconnect()

    return config.execution_blocks()


@config_api.get("/execution_blocks/{execution_block}", tags=["config"])
async def get_execution_block(execution_block: str) -> dict:
    """Get a specific execution block"""
    if not RE_EXECUTION_BLOCK.match(execution_block):
        raise ValueError("execution block doesn't match expected format")
    config.reconnect()
    data = config.execution_block(execution_block)
    if data is None:
        raise HTTPException(status_code=404, detail="Execution Block doesn't exist")
    return data


@config_api.get("/execution_blocks/{execution_block}/receptors", tags=["config"])
async def get_execution_block_receptors(execution_block: str) -> dict:
    """Get a specific execution block"""
    if not RE_EXECUTION_BLOCK.match(execution_block):
        raise ValueError("execution block doesn't match expected format")
    config.reconnect()
    data = config.execution_block(execution_block)
    if data is None:
        raise HTTPException(status_code=404, detail="Execution Block doesn't exist")
    return {"receptors": data["resources"]["receptors"]}


@config_api.get("/execution_blocks/{execution_block}/baselines", tags=["config"])
async def get_execution_block_baselines(execution_block: str) -> dict:
    """Get a specific execution block"""
    if not RE_EXECUTION_BLOCK.match(execution_block):
        raise ValueError("execution block doesn't match expected format")
    config.reconnect()
    data = config.execution_block(execution_block)
    if data is None:
        raise HTTPException(status_code=404, detail="Execution Block doesn't exist")

    polarisations = data["polarisations"][0]["corr_type"]
    receptors = data["resources"]["receptors"]
    baselines = combinations_with_replacement(receptors, 2)
    baseline_pols = product(baselines, polarisations)

    return {
        "baselines": [
            f"{min(pair[0], pair[1])}_{max(pair[0], pair[1])}_{pol}" for pair, pol in baseline_pols
        ]
    }


@config_api.get("/deployments", tags=["config"])
async def get_deployments() -> list[str]:
    """Get a list of known deployments"""
    config.reconnect()
    return config.deployments()


@config_api.get("/deployments/{deployment}", tags=["config"])
async def get_deployment(deployment: str) -> dict:
    """Get a specific deployment"""
    if not RE_DEPLOYMENT.match(deployment):
        raise ValueError("deployment ID doesn't match expected format")
    config.reconnect()
    data = config.deployment(deployment)
    if data is None:
        raise HTTPException(status_code=404, detail="Deployment doesn't exist")
    return data


@config_api.get("/deployments/{deployment}/state", tags=["config"])
async def get_deployment_state(deployment: str) -> dict:
    """Get a specific deployment's state"""
    if not RE_DEPLOYMENT.match(deployment):
        raise ValueError("deployment ID doesn't match expected format")
    config.reconnect()
    data = config.deployment_state(deployment)
    if data is None:
        raise HTTPException(status_code=404, detail="Deployment doesn't exist")
    return data


@config_api.get("/flows", tags=["config"])
async def get_configured_flows(processing_block: str = None) -> list[dict]:
    """Get the list of flows that are configured for the Signal Display."""
    if processing_block is not None and not RE_PROCESSING_BLOCK.match(processing_block):
        raise ValueError("processing block doesn't match expected format")

    config.reconnect()

    flows = config.flows()

    output = []

    for flow, details in flows:
        if processing_block is not None and flow.pb_id == processing_block:
            continue

        first_source = details.sources[0] if len(details.sources) > 0 else None
        if first_source is not None and first_source.function not in [
            "SignalDisplayMetrics",
            "SignalDisplayMetrics.Stats",
            "signal_metrics.stats_receiver",
        ]:
            continue

        params = first_source.parameters if first_source is not None else {}

        output.append(
            {
                "processing_block_id": flow.pb_id,
                "name": flow.name,
                "kind": flow.kind,
                "topic": details.sink.topics,
                "format": details.sink.format,
                "metric": params.get("metric_type"),
                "nchan_avg": params.get("nchan_avg"),
                "additional_windows": params.get("additional_windows"),
                "rounding_sensitivity": params.get("rounding_sensitivity"),
                "data_model": details.data_model,
            }
        )

    return output
