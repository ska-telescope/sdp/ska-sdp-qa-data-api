"""Endpoints related to the Windows in the Config DB."""

import logging

from fastapi import APIRouter, Depends
from ska_sdp_dataqueues.schemas.signal_display_metrics import MetricDataTypes

from ska_sdp_qa_data_api.server.core.authorisation import user_profile
from ska_sdp_qa_data_api.server.data.inputs import RE_PROCESSING_BLOCK, RE_SUBARRAY, CreateWindow
from ska_sdp_qa_data_api.server.data.responses import Window
from ska_sdp_qa_data_api.server.utils.config_db import SDPConfig

window_api = APIRouter()

logger = logging.getLogger(__name__)

config = SDPConfig()


@window_api.get("/", tags=["config"], summary="Retrieve the available windows")
def get_windows(
    subarray: str = None, processing_block: str = None, metric: str = None
) -> list[Window]:
    """List the current windows that have been configured."""
    if subarray is not None and not RE_SUBARRAY.match(subarray):
        raise ValueError("subarray doesn't match expected format")
    if processing_block is not None and not RE_PROCESSING_BLOCK.match(processing_block):
        raise ValueError("processing block doesn't match expected format")
    if metric is not None and metric not in iter(MetricDataTypes):
        raise ValueError("metric doesn't match list of available metrics")
    config.reconnect()
    return config.windows(subarray, processing_block, metric)


@window_api.post("/", tags=["config"], summary="Create a new window")
async def create_window(window: CreateWindow, current_user_profile=Depends(user_profile)) -> dict:
    """Attempt to create or re-use a window."""
    user_email = current_user_profile.get("user_profile", {}).get(
        "userPrincipalName", "guest@community.skao.int"
    )
    config.reconnect()
    return config.window_create(window, user_email)
