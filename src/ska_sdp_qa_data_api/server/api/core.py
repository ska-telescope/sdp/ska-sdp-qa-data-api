"""Core api calls."""

from fastapi import APIRouter, HTTPException

from ska_sdp_qa_data_api.server.core.config import (
    GRAFANA_DASHBOARD,
    LOG_URLS,
    MONITORING_URL,
    NAMESPACE,
    PROCESSING_NAMESPACE,
    SHARED_CLUSTER_URL,
)
from ska_sdp_qa_data_api.server.data.inputs import RE_SUBARRAY
from ska_sdp_qa_data_api.server.data.responses import ApiConfig, GrafanaUrls, LogUrls, Ping
from ska_sdp_qa_data_api.server.utils.helpers import get_system_config

core_api = APIRouter()


def grafana_link(dashboard, parameters, base_url=MONITORING_URL):
    """Get the grafana link with the requested parameters to the dashboard"""
    return "".join(
        [f"{base_url}/d/{dashboard}?orgId=1&refresh=5s"]
        + list(map(lambda a: f"&{a[0]}={a[1]}", parameters.items()))
    )


def kibana_query_link(
    search_terms, column_layout: list[tuple[str, str]], base_url=SHARED_CLUSTER_URL
):
    """Get the kibana link based on search_terms, columns and dashboard"""
    search_terms = " or ".join(search_terms)
    columns = ",".join(col[0] for col in column_layout)
    grid = ",".join(f"{col[0]}:(width:{col[1]})" for col in column_layout if len(col[1]) > 0)
    return "".join(
        [
            f"{base_url}/kibana/app/discover#/?",
            "_g=(filters:!(),refreshInterval:(pause:!t,value:0),time:(from:now-15m,to:now))&",
            "_a=",
            ",".join(
                [
                    f"(columns:!({columns})",
                    "filters:!()",
                    f"grid:(columns:({grid}))",
                    "index:cbb05bec-ed81-45f8-b11a-eab26a3df6b1",
                    "interval:auto",
                    f"query:(language:kuery,query:'{search_terms}')",
                    "sort:!(!('@timestamp',desc)))",
                ]
            ),
        ]
    )


@core_api.get("/ping", tags=["core"], summary="Ping the API")
def ping() -> Ping:
    """Returns {"ping": "live"} when called"""
    return Ping()


@core_api.get(
    LOG_URLS + "/{subarray}",
    tags=["core"],
    summary="Retrieve the monitoring links",
)
def get_log_urls(subarray: str) -> LogUrls:
    """Get the log and dashboards for a specific subarray"""

    def select_processing_namespace():
        """Get the processing namespace based on subarray."""
        if subarray and RE_SUBARRAY.match(subarray):
            # Just return the PROCESSING_NAMESPACE
            # while we don't have multiple subarrays to select from.
            return PROCESSING_NAMESPACE
        return None

    processing_namespace = select_processing_namespace()
    if not processing_namespace:
        raise HTTPException(status_code=400, detail="Subarray or Namespace error.")

    search_terms = [
        f'kubernetes.namespace: "{NAMESPACE}"',
        f'kubernetes.namespace: "{processing_namespace}"',
    ]
    columns = [
        ("kubernetes.namespace", "(width:120)"),
        ("kubernetes.pod.name", "(width:150)"),
        ("kubernetes.container.name", "(width:120)"),
        ("message", ""),
    ]

    return LogUrls(
        kibana=kibana_query_link(search_terms, columns),
        grafana=GrafanaUrls(
            sdp=grafana_link(GRAFANA_DASHBOARD, {"var-namespace": NAMESPACE}),
            pipeline=grafana_link(GRAFANA_DASHBOARD, {"var-namespace": processing_namespace}),
        ),
    )


@core_api.get("/config", tags=["core"], summary="API Config")
def api_config() -> ApiConfig:
    """Get the current setup of the API."""
    return get_system_config()
