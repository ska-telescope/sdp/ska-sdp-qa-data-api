"""To be used with incoming calibration data."""

import time
from io import BytesIO

import numpy as np


class PointingOffset:
    """Helper class to use a Pointing Offset Object"""

    def __init__(self, dataset):
        self.dataset = self._structure(dataset)

    def _structure(self, dataset):

        if isinstance(dataset, np.ndarray):
            array_data = dataset
        else:
            dtype = np.dtype(
                [
                    ("antenna_name", "U8"),
                    ("last_scan_index", "f8"),
                    ("xel_offset", "f8"),
                    ("xel_offset_std", "f8"),
                    ("el_offset", "f8"),
                    ("el_offset_std", "f8"),
                    ("expected_width_h", "f8"),
                    ("expected_width_v", "f8"),
                    ("fitted_width_h", "f8"),
                    ("fitted_width_h_std", "f8"),
                    ("fitted_width_v", "f8"),
                    ("fitted_width_v_std", "f8"),
                    ("fitted_height", "f8"),
                    ("fitted_height_std", "f8"),
                ]
            )
            array_data = np.array(
                [
                    (
                        d["antenna_name"],
                        d["last_scan_index"],
                        d["xel_offset"],
                        d["xel_offset_std"],
                        d["el_offset"],
                        d["el_offset_std"],
                        d["expected_width_h"],
                        d["expected_width_v"],
                        d["fitted_width_h"],
                        d["fitted_width_h_std"],
                        d["fitted_width_v"],
                        d["fitted_width_v_std"],
                        d["fitted_height"],
                        d["fitted_height_std"],
                    )
                    for d in dataset
                ],
                dtype=dtype,
            )
        return array_data

    @property
    def antennas(self) -> list:
        """Get list of antenna labels."""
        return [i["antenna_name"] for i in self.dataset]

    @property
    def last_scan_index(self) -> list:
        """Get list of last scan indices."""
        return [i["last_scan_index"] for i in self.dataset]

    @property
    def xel_offset(self) -> list:
        """Get cross elevation offsets."""
        return [i["xel_offset"] for i in self.dataset]

    @property
    def xel_offset_std(self) -> list:
        """Standard deviation on cross elevation offsets."""
        return [i["xel_offset_std"] for i in self.dataset]

    @property
    def el_offset(self) -> list:
        """Get elevation offset."""
        return [i["el_offset"] for i in self.dataset]

    @property
    def el_offset_std(self) -> list:
        """Standard deviation on elevation offsets."""
        return [i["el_offset_std"] for i in self.dataset]

    @property
    def fitted_width_h(self) -> list:
        """Gaussian primary beam fitted width in xel."""
        return [i["fitted_width_h"] for i in self.dataset]

    @property
    def fitted_width_h_std(self) -> list:
        """Standard deviation on xel fitted width."""
        return [i["fitted_width_h_std"] for i in self.dataset]

    @property
    def expected_width_h(self) -> list:
        """Expected width on xel."""
        return [i["expected_width_h"] for i in self.dataset]

    @property
    def expected_width_v(self) -> list:
        """Gaussian primary beam fitted width in el."""
        return [i[10] for i in self.dataset]

    @property
    def fitted_width_v(self) -> list:
        """Standard deviation on el fitted width."""
        return [i["fitted_width_v"] for i in self.dataset]

    @property
    def fitted_width_v_std(self) -> list:
        """Expected width on el."""
        return [i["fitted_width_v_std"] for i in self.dataset]

    @property
    def fitted_height(self) -> list:
        """Fitted height of the primary beam in arbitrary units."""
        return [i["fitted_height"] for i in self.dataset]

    @property
    def fitted_height_std(self) -> list:
        """Standard deviation on fitted height."""
        return [i["fitted_height_std"] for i in self.dataset]


class GainCalibration:
    """Helper class to use a gainCalibration object."""

    def __init__(self, dataset):
        self.dataset = self._decode(dataset)
        self.first_channel = self.get_first_channel()

    def _decode(self, dataset):
        """Decode message ourselves to handle npy format."""
        load_bytes = BytesIO(dataset)
        return np.load(load_bytes, allow_pickle=True)

    def get_first_channel(self):
        """Only deal with first channel."""
        return [x[0] for x in self.dataset]

    @property
    def time_received(self):
        """The time the gain cal data was received"""
        return [time.time()]

    @property
    def gain_amplitudes(self) -> list:
        """Get list of gain amplitudes."""

        return [np.abs(xi).tolist() for xi in self.first_channel]

    @property
    def gain_phases(self) -> list:
        """Get list of gain phases."""

        return [np.angle(xi).tolist() for xi in self.first_channel]

    @property
    def antennas(self) -> list:  # pragma: no cover
        """Get list of antenna labels."""
        return NotImplemented

    @property
    def polarisations(self) -> list:  # pragma: no cover
        """Get list of polarisations."""
        return NotImplemented

    @property
    def frequencies(self) -> list:  # pragma: no cover
        """Get list of frequencies."""
        return NotImplemented
