# pylint: disable=W0231
# pylint: disable=W0221
"""Processing functions to create spectrum waterfall plots."""
import math

import numpy as np
from ska_sdp_dataqueues.schemas.signal_display_metrics import SpectrumPayload

from ska_sdp_qa_data_api.server.utils.waterfall_plots import WaterfallPlots

AMP_MULTIPLIER = 10
AMP_REF = 1


class SpectrumWaterfalls(WaterfallPlots):
    """Sub-class for generating waterfall spectrum plots."""

    def _convert_to_db(self, power: float) -> float:
        """Convert power value to DB."""
        if power > 0:
            return AMP_MULTIPLIER * math.log10(power / AMP_REF)
        return np.NaN

    def process_processed(self, input_data: SpectrumPayload):
        """Process pre-processed waterfall plots"""
        for plot in input_data.data:
            powers = [self._convert_to_db(power) for power in plot.power]
            self._process_spectrum_waterfalls(
                input_data.processing_block_id,
                plot.polarisation,
                np.array(powers),
            )

    def _process_spectrum_waterfalls(self, processing_block_id: str, polarisation: str, spectrum):
        """Transforms json data into iterable."""
        key = f"{processing_block_id}_{polarisation}_spectrum_waterfalls"
        self._store_np_as_timeseries(key, spectrum)
