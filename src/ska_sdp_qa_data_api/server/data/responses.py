"""The response structures of the Config endpoints."""

from dataclasses import dataclass

# pylint: disable=too-many-instance-attributes


@dataclass
class Ping:
    """Ping response."""

    ping: str = "live"


@dataclass
class GrafanaUrls:
    """URLs to the Grafana instances"""

    sdp: str
    pipeline: str


@dataclass
class LogUrls:
    """URLs tot he monitoring instances"""

    kibana: str
    grafana: GrafanaUrls


@dataclass
class ApiConfig:
    """Configuration for the API"""

    api_format: str
    app_version: str
    project_name: str
    start_time: str
    uptime: float
    paths: dict[str, str]
    topics: dict[str, str]
    waterfall_plots: dict[str, int]


@dataclass
class Subarray:
    """Subarray layout"""

    eb_id: str
    last_command: str
    last_command_time: str
    obs_state_commanded: str
    resources: dict
    state_commanded: str


@dataclass
class ProcessingBlockScript:
    """Script being used for the Processing Block"""

    kind: str
    name: str
    version: str


@dataclass
class ProcessingBlock:
    """Processing Block layout"""

    eb_id: str
    script: ProcessingBlockScript
    channels_per_port: int
    processors: list[str]
    signal_display_config: dict


@dataclass
class ProcessingBlockState:
    """The processing Block State"""

    deployments: dict[str, str]
    deployments_ready: bool
    last_updated: str
    resources_available: bool
    status: str


@dataclass
class Spead2Stream:
    """THe layout for a stream."""

    id: int
    heaps: int
    blocked: int
    incomplete_heaps: int


@dataclass
class Spead2:
    """The statistics for a SPEAD2 receiver."""

    type: str
    time: float
    scan_id: int
    state: str
    total_megabytes: float
    num_heaps: int
    num_incomplete: int
    duration: float
    streams: list[Spead2Stream]


def dict_to_spead2(data: dict) -> Spead2 | None:
    """Convert a dict to a SPEAD2 object."""
    if data is None:
        return None
    ret = Spead2(**data)
    if ret.streams:
        ret.streams = [Spead2Stream(**dat) for dat in ret.streams]
    return ret


@dataclass
class Plasma:
    """The layout of the Plasma stats object."""

    state: str
    time: float
    type: str
    processing_block_id: str
    execution_block_id: str
    subarray_id: str
    scan_id: int
    payloads_received: int
    time_slices: int
    time_since_last_payload: float


@dataclass
class Baselines:
    """The Baselines response."""

    baselines: list[str]


@dataclass
class SubWindow:
    """Generated Windows"""

    start: int
    end: int
    channels_averaged: int
    user: str
    requested_at: str


@dataclass
class Window:
    """A single configured Window."""

    topic: str
    processing_block: str
    max_window_count: int
    channels_averaged: int
    windows: list[SubWindow]
