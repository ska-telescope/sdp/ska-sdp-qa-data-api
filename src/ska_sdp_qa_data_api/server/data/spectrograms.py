# pylint: disable=W0231
"""Processes on how to calculate spectrograms."""

# pylint: disable=too-many-arguments


import logging

import numpy
from ska_sdp_dataqueues.schemas.signal_display_metrics import MetricPayload

from ska_sdp_qa_data_api.server.utils.waterfall_plots import WaterfallPlots

logger = logging.getLogger(__name__)


class SpectrogramPlots(WaterfallPlots):
    """Class to manage all access for creating waterfall plots."""

    def process_processed(self, input_data: MetricPayload):
        """Process pre-processed waterfall plots"""
        pb_id = input_data.processing_block_id

        for plot in input_data.data:
            self._process_spectrogram(
                pb_id, plot.baseline, plot.polarisation, numpy.rad2deg(plot.data)
            )

    def _process_spectrogram(
        self, processing_block_id: str, baseline: str, polarisation: str, phase
    ):
        """Transforms json data into iterable."""
        r1, r2 = baseline.split("_")
        r1, r2 = min(r1, r2), max(r1, r2)
        baseline = f"{r1}_{r2}"
        key = f"{processing_block_id}_{baseline}_{polarisation}"
        self._store_np_as_timeseries(key, phase)
