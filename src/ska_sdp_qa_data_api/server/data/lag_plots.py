# pylint: disable=W0231
# pylint: disable=W0221
"""Processing functions to create lag plots."""
import logging

import numpy as np
from ska_sdp_dataqueues.schemas.signal_display_metrics import MetricPayload

from ska_sdp_qa_data_api.server.utils.waterfall_plots import WaterfallPlots

logger = logging.getLogger(__name__)


class LagPlots(WaterfallPlots):
    """Sub-class for generating waterfall lag plots."""

    def process_processed(self, input_data: MetricPayload):
        """Process pre-processed waterfall plots"""
        for plot in input_data.data:
            self._process_lag_plot(
                input_data.processing_block_id,
                plot.baseline,
                plot.polarisation,
                np.array(plot.data),
            )

    def _process_lag_plot(
        self, processing_block_id: str, baseline: str, polarisation: str, sig_fft
    ):
        """Transforms json data into iterable."""
        r1, r2 = baseline.split("_")
        r1, r2 = min(r1, r2), max(r1, r2)
        baseline = f"{r1}_{r2}"
        key = f"{processing_block_id}_{baseline}_{polarisation}_lagplots"
        self._store_np_as_timeseries(key, sig_fft)
