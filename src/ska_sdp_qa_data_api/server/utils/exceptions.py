"""This utility class provides custom exceptions"""

from typing import Union


class NoDataException(Exception):
    """Raise this if no data is found for a specific baseline."""


class IncompatibleMessageFormatError(Exception):
    """Exception raised when unknown message format
    is used."""

    def __init__(self, topic, message="Incompatible Message Format Used."):
        self.topic = topic
        self.message = message
        super().__init__(self.message)


class AuthError(Exception):
    """
    Represents an authentication error.

    Attributes:
        message (str): A descriptive error message.
        status_code (int): The HTTP status code associated with the error.
    """

    def __init__(self, message: str, status_code: Union[int, None] = None):
        self.message = message
        self.status_code = status_code or 401  # Default to unauthorized if not provided
