"""Stats helper class with accessing and modifying stats in Redis."""

import hashlib
import logging
from datetime import datetime
from time import time

from fastapi import WebSocket
from redis import Redis

from ska_sdp_qa_data_api.server.core.config import REDIS_HOST, REDIS_PORT

logger = logging.getLogger(__name__)


def now_str():
    """Return the current date/time in ISO."""
    return datetime.now().astimezone().isoformat()


def s(value) -> str:
    """If value is a byte array decode, else return as string."""

    if value is None:
        return None

    if isinstance(value, str):
        return value

    try:
        return value.decode()
    except Exception:  # pylint: disable=broad-exception-caught
        return str(value)


class WebsocketStats:
    """The stats that is going to be used for the websockets."""

    def __init__(self):
        self.redis = None
        self._base = "websocket"

    def _get_ids_sorted(self, key):
        if self.redis.exists(key):
            data = list(self.redis.smembers(key))
            data.sort()
            return data
        return []

    def _connect(self):
        if self.redis is None:
            self.redis = Redis(host=REDIS_HOST, port=REDIS_PORT)

    def _user_from_websocket(self, websocket: WebSocket) -> str:
        client_id = websocket.headers.get("sec-websocket-key", str(id(websocket)))
        return hashlib.sha1(client_id.encode("utf-8")).hexdigest()

    def add_websocket(self, websocket: WebSocket, topic: str | None = None):
        """Add the websocket to the stats"""
        self._connect()

        client_id = self._user_from_websocket(websocket)

        data = {"last_connect_at": now_str()}
        if "x-user" in websocket.headers:
            data["user"] = websocket.headers["x-user"]
        if "x-email" in websocket.headers:
            data["email"] = websocket.headers["x-email"]

        if "x-real-ip" in websocket.headers:
            data["ip-real"] = websocket.headers["x-real-ip"]
        elif "x-forwarded-for" in websocket.headers:
            data["ip"] = websocket.headers["x-forwarded-for"]
        elif "x-original-forwarded-for" in websocket.headers:
            data["ip-original"] = websocket.headers["x-original-forwarded-for"]
        if "user-agent" in websocket.headers:
            data["user-agent"] = websocket.headers["user-agent"]
        if "sec-ch-ua-platform" in websocket.headers:
            data["platform"] = websocket.headers["sec-ch-ua-platform"]

        self._add(client_id, topic, data)

    def remove_websocket(self, websocket: WebSocket, topic: str | None = None):
        """Remove this websocket"""
        self._connect()
        client_id = self._user_from_websocket(websocket)
        logger.info("Client ID for disconnect: %s", client_id)
        logger.info("Headers: %s", websocket.headers)
        self._remove(client_id, topic)

    def access_websocket(self, websocket: WebSocket, topic: str | None = None):
        """Set access time for a socket."""
        self._connect()
        client_id = self._user_from_websocket(websocket)
        new_data = {"last_access": time()}
        if topic:
            new_data["last_access_topic"] = topic
        self.redis.hmset(f"{self._base}:{client_id}", new_data)

    def _add(self, client_id: str, topic=None, extra_info: dict[str, str] | None = None):
        self.redis.sadd(
            f"{self._base}:clients",
            client_id,
        )

        if extra_info is not None and len(extra_info) > 0:
            self.redis.hmset(f"{self._base}:{client_id}", extra_info)

        if topic is not None:
            self.redis.sadd(
                f"{self._base}:{client_id}:sockets",
                topic,
            )

    def _remove(self, client_id: str, topic=None):
        if topic is not None:
            self.redis.srem(
                f"{self._base}:{client_id}:sockets",
                topic,
            )
            size_sockets = self.redis.scard(f"{self._base}:{client_id}:sockets")
            if size_sockets == 0:
                self.redis.srem(
                    f"{self._base}:clients",
                    client_id,
                )
                self.redis.delete(f"{self._base}:{client_id}")

    def stats(self) -> dict:
        """Get the stats for the connected clients"""
        self._connect()
        data = {
            "clients": {
                s(client): {
                    "details": self.redis.hgetall(f"{self._base}:{s(client)}"),
                    "topics": [
                        s(c) for c in self._get_ids_sorted(f"{self._base}:{s(client)}:sockets")
                    ],
                }
                for client in self._get_ids_sorted(f"{self._base}:clients")
            },
            "topics": {},
        }
        for client, config in data["clients"].items():
            for topic in config["topics"]:
                if topic not in data["topics"]:
                    data["topics"][topic] = []
                data["topics"][topic].append(client)
        return data
