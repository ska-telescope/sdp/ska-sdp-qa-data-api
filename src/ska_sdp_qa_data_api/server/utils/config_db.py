"""Wrapper for the config DB with a fallback to use the stats module."""

import asyncio
import logging
import threading
from collections.abc import Callable
from dataclasses import asdict
from enum import Enum

import requests
import ska_sdp_config

from ska_sdp_qa_data_api.server.data.inputs import CreateWindow
from ska_sdp_qa_data_api.server.data.responses import (
    ProcessingBlock,
    ProcessingBlockScript,
    ProcessingBlockState,
    Subarray,
    SubWindow,
    Window,
)
from ska_sdp_qa_data_api.server.utils.stats import now_str

logger = logging.getLogger(__name__)

# pylint: disable=too-many-public-methods


class ConfigDBTypes(str, Enum):
    """The list of approved configuration types."""

    LIST_SUBARRAYS = "list_subarrays"
    LIST_PROCESSING_BLOCKS = "list_processing_blocks"
    LIST_EXECUTION_BLOCKS = "list_execution_blocks"
    LIST_DEPLOYMENTS = "list_deployments"
    SUBARRAY = "subarray"
    SUBARRAY_STATE = "subarray_state"
    PROCESSING_BLOCK = "processing_block"
    PROCESSING_BLOCK_STATE = "processing_block_state"
    EXECUTION_BLOCK = "execution_block"
    DEPLOYMENT = "deployment"
    DEPLOYMENT_STATE = "deployment_state"
    LIST_FLOW = "list_flow"
    FLOW = "flow"
    FLOW_STATE = "flow_state"


ALL_CONFIG_TYPES = [
    ConfigDBTypes.LIST_SUBARRAYS,
    ConfigDBTypes.LIST_PROCESSING_BLOCKS,
    ConfigDBTypes.LIST_EXECUTION_BLOCKS,
    ConfigDBTypes.LIST_DEPLOYMENTS,
    ConfigDBTypes.SUBARRAY,
    ConfigDBTypes.SUBARRAY_STATE,
    ConfigDBTypes.PROCESSING_BLOCK,
    ConfigDBTypes.PROCESSING_BLOCK_STATE,
    ConfigDBTypes.EXECUTION_BLOCK,
    ConfigDBTypes.DEPLOYMENT,
    ConfigDBTypes.DEPLOYMENT_STATE,
    ConfigDBTypes.LIST_FLOW,
    ConfigDBTypes.FLOW,
    ConfigDBTypes.FLOW_STATE,
]

# pylint: disable=too-many-return-statements


class SDPConfig:
    """Wrapper to the Config DB."""

    def __init__(self):
        self.config = None

    def connect(self):
        """Attempt to connect to the DB"""
        try:
            self.config = ska_sdp_config.Config()
            return True
        except requests.exceptions.ConnectionError:
            logger.warning("Unable to connect to the config database")
            self.config = None
            return False

    def reconnect(self):
        """If there is no connection reconnect"""

        if self.config is None:
            return self.connect()
        return True

    def subarrays(self) -> list[str]:
        """Fetch the current list of subarrays."""
        if self.config is not None:
            for txn in self.config.txn():
                return self._list_subarrays(txn)
        return []

    def subarray(self, subarray_id: str) -> Subarray:
        """Get a specific subarray."""
        if self.config is None:
            return None
        for txn in self.config.txn():
            return self._get_subarray(txn, subarray_id)
        return None

    def subarray_running_state(self, subarray_id: str) -> dict:
        """Retrieve the state of the given subarray."""
        if self.config is None:
            return None
        for txn in self.config.txn():
            return self._get_subarray_state(txn, subarray_id)
        return {}

    def processing_blocks(self) -> list[str]:
        """Get list of processing blocks"""
        if self.config is None:
            return None
        for txn in self.config.txn():
            return self._list_processing_blocks(txn)
        return []

    def processing_block(self, processing_block_id: str) -> ProcessingBlock:
        """Get a specific processing block"""
        if self.config is None:
            return None
        for txn in self.config.txn():
            return self._get_processing_block(txn, processing_block_id)
        return None

    def processing_block_state(self, processing_block_id: str) -> ProcessingBlockState:
        """Get the current stats of a processing block"""
        if self.config is None:
            return None
        for txn in self.config.txn():
            return self._get_processing_block_state(txn, processing_block_id)
        return None

    def execution_blocks(self) -> list[str]:
        """Get a list of execution_blocks"""
        if self.config is None:
            return []
        for txn in self.config.txn():
            return self._list_execution_blocks(txn)
        return []

    def execution_block(self, execution_block_id: str):
        """Get a specific execution block"""
        if self.config is None:
            return None
        for txn in self.config.txn():
            return self._get_execution_block(txn, execution_block_id)
        return None

    def deployments(self):
        """Get a list of current deployments"""
        if self.config is None:
            return []
        for txn in self.config.txn():
            return self._list_deployments(txn)
        return []

    def deployment(self, deployment_id: str):
        """Get a specific deployment"""
        if self.config is None:
            return None
        for txn in self.config.txn():
            return self._get_deployment(txn, deployment_id)
        return None

    def deployment_state(self, deployment_id: str):
        """Get the state of a specific deployment"""
        if self.config is None:
            return None
        for txn in self.config.txn():
            return self._get_deployment_state(txn, deployment_id)
        return None

    def flows(self, with_values: bool = False):
        """Get list of flows defined in the config DB."""
        if self.config is None:
            return None
        for txn in self.config.txn():
            return self._list_flows(txn, with_values)
        return None

    def flow(self, flow_name):
        """Get a specific flow."""
        if self.config is None:
            return None
        for txn in self.config.txn():
            return self._get_flow(txn, flow_name)
        return None

    def flow_state(self, flow_name):
        """Get a specific flow."""
        if self.config is None:
            return None
        for txn in self.config.txn():
            return self._get_flow_state(txn, flow_name)
        return None

    def windows(
        self, subarray: str = None, processing_block: str = None, metric: str = None
    ) -> list[Window]:
        """Get the list of configured windows."""
        if self.config is None:
            return []
        for txn in self.config.txn():
            return self._get_windows(txn, subarray, processing_block, metric)
        return []

    def window_create(self, window: CreateWindow, user_email: str) -> dict:
        """Attempt to create a new window."""
        state = {"success": False, "message": "Nothing to update"}
        for txn in self.config.txn():
            configured_flow = self._windows_locate_window(txn, window)
            if configured_flow is None:
                return {"success": False, "message": "No window found"}

            try:
                exists, new, updated_flow_state, index = self._windows_create_new_state(
                    txn, window, configured_flow, user_email
                )
            except ValueError as err:
                state = {"success": False, "message": str(err)}
                break

            if new:
                if exists:
                    txn.flow.state(configured_flow).update(updated_flow_state)
                else:
                    txn.flow.state(configured_flow).create(updated_flow_state)

                state = {
                    "success": True,
                    "window_id": index,
                    "message": "New window added",
                }
            else:
                state = {
                    "success": True,
                    "window_id": index,
                    "message": "Using existing window",
                }

        return state

    def _windows_locate_window(self, txn, window: CreateWindow):
        flows = self._list_flows(txn)
        for flow in flows:
            if window.processing_block is not None and flow.pb_id != window.processing_block:
                continue

            if window.subarray not in flow.name:
                continue

            if window.metric not in flow.name:
                continue

            return flow
        return None

    def _windows_create_new_state(self, txn, window, flow, user_email):
        flow_state = self._get_flow_state(txn, flow)

        if flow_state is None:
            flow_state = {}

        if "windows" not in flow_state:
            return (
                False,
                True,
                {
                    "windows": [
                        asdict(
                            SubWindow(
                                start=window.spectrum_start,
                                end=window.spectrum_end,
                                channels_averaged=window.channels_averaged,
                                user=user_email,
                                requested_at=now_str(),
                            )
                        )
                    ]
                },
                1,
            )

        actual_flow = self._get_flow(txn, flow)

        for index, flow_window in enumerate(flow_state["windows"]):
            if (
                flow_window["start"] == window.spectrum_start
                and flow_window["end"] == window.spectrum_end
                and flow_window["channels_averaged"] == window.channels_averaged
            ):
                return True, False, None, index + 1

        if len(flow_state["windows"]) >= actual_flow.sources[0].parameters["additional_windows"]:
            raise ValueError("There are too many windows already open")

        flow_state["windows"].append(
            asdict(
                SubWindow(
                    start=window.spectrum_start,
                    end=window.spectrum_end,
                    channels_averaged=window.channels_averaged,
                    user=user_email,
                    requested_at=now_str(),
                )
            )
        )

        return True, True, flow_state, len(flow_state["windows"])

    def watcher_list(self, type_name: ConfigDBTypes, type_id: str | None = None):
        """Start a watcher on the given type.

        Returns a generator with the changes as each item.
        """
        if self.config is None:
            yield None
            return

        if type_name not in ALL_CONFIG_TYPES:
            raise ValueError(f"Invalid type '{type_name}'")

        for watcher in self.config.watcher():
            for txn in watcher.txn():
                yield self._get_thing_from_transaction(txn, type_name, type_id)

    async def watcher_thread(
        self,
        type_name: ConfigDBTypes,
        type_id: str | None,
        callback: Callable[[ConfigDBTypes, str, dict | list | None], None],
    ):
        """Start a new thread to run a watcher and get data from the system"""
        return threading.Thread(
            target=self.__watcher, args=(asyncio.get_event_loop(), type_name, type_id, callback)
        ).start()

    def __watcher(
        self,
        event_loop,
        type_name: ConfigDBTypes,
        type_id: str | None,
        callback: Callable[[ConfigDBTypes, str, dict | list | None], None],
    ):
        for event in self.watcher_list(type_name, type_id):
            asyncio.run_coroutine_threadsafe(callback(type_name, type_id, event), loop=event_loop)

    def get_from_type_dict(self, type_name: ConfigDBTypes, type_id: str | None = None) -> dict:
        """Return data from given type."""
        if type_name not in ALL_CONFIG_TYPES:
            raise ValueError(f"Invalid type '{type_name}'")

        if self.config is None:
            return []

        for txn in self.config.txn():
            return self._get_thing_from_transaction(txn, type_name, type_id)

    def _get_thing_from_transaction(
        self, txn, type_name: ConfigDBTypes, type_id: str | None = None
    ):  # pylint: disable=too-many-branches
        if type_name == ConfigDBTypes.LIST_SUBARRAYS:
            return self._list_subarrays(txn)
        if type_name == ConfigDBTypes.SUBARRAY_STATE:
            return self._get_subarray_state(txn, type_id)
        if type_name == ConfigDBTypes.SUBARRAY:
            return self._get_subarray(txn, type_id)
        if type_name == ConfigDBTypes.LIST_PROCESSING_BLOCKS:
            return self._list_processing_blocks(txn)
        if type_name == ConfigDBTypes.PROCESSING_BLOCK:
            return self._get_processing_block(txn, type_id)
        if type_name == ConfigDBTypes.PROCESSING_BLOCK_STATE:
            return self._get_processing_block_state(txn, type_id)
        if type_name == ConfigDBTypes.LIST_EXECUTION_BLOCKS:
            return self._list_execution_blocks(txn)
        if type_name == ConfigDBTypes.EXECUTION_BLOCK:
            return self._get_execution_block(txn, type_id)
        if type_name == ConfigDBTypes.LIST_DEPLOYMENTS:
            return self._list_deployments(txn)
        if type_name == ConfigDBTypes.DEPLOYMENT:
            return self._get_deployment(txn, type_id)
        if type_name == ConfigDBTypes.DEPLOYMENT_STATE:
            return self._get_deployment_state(txn, type_id)
        if type_name == ConfigDBTypes.LIST_FLOW:
            return self._list_flows(txn)
        if type_name == ConfigDBTypes.FLOW:
            return self._get_flow(txn, type_id)
        if type_name == ConfigDBTypes.FLOW_STATE:
            return self._get_flow_state(txn, type_id)

        raise ValueError(f"Invalid type '{type_name}'")

    def _list_subarrays(self, txn):
        return [
            component.removeprefix("lmc-subarray-")
            for component in txn.component.query_keys(component_name_prefix="lmc-subarray")
        ]

    def _get_subarray(self, txn, subarray_id):
        data = txn.component(f"lmc-subarray-{subarray_id}").get()
        if data is not None:
            return Subarray(
                eb_id=data.get("eb_id", None),
                last_command=data.get("command", None),
                last_command_time=data.get("command_time", None),
                obs_state_commanded=data.get("obs_state_commanded", None),
                resources=data.get("resources", None),
                state_commanded=data.get("state_commanded", None),
            )
        return None

    def _get_subarray_state(self, txn, subarray_id):
        subarray_data = self._get_subarray(txn, subarray_id)
        if subarray_data is None:
            raise ValueError("Subarray doesn't exist")

        output_data = {"subarray": subarray_data}

        if subarray_data.eb_id is None:
            return output_data

        eb = self._get_execution_block(txn, subarray_data.eb_id)
        if eb is None:
            return output_data

        output_data["execution_block"] = eb
        output_data["execution_block_state"] = self._get_execution_block_state(
            txn, subarray_data.eb_id
        )

        if "pb_realtime" not in eb or len(eb["pb_realtime"]) == 0:
            return output_data

        pb = self._get_processing_block(txn, eb["pb_realtime"][0])
        pb_state = self._get_processing_block_state(txn, eb["pb_realtime"][0])
        if pb is None:
            return output_data

        output_data["processing_block"] = pb
        output_data["processing_block_state"] = pb_state

        if pb_state is None or pb_state.deployments is None:
            return output_data

        output_data["deployments"] = {
            deployment_id: {
                "state": deployment_state,
                "deployment": self._get_deployment(txn, deployment_id),
            }
            for deployment_id, deployment_state in pb_state.deployments.items()
        }
        return output_data

    def _list_processing_blocks(self, txn, with_values=False):
        if with_values:
            return txn.processing_block.list_values()
        return txn.processing_block.list_keys()

    def _get_processing_block(self, txn, processing_block_id):
        data = txn.processing_block.get(processing_block_id)
        if data is not None:
            data = data.model_dump()
            script = data.get("script", None)
            params = data.get("parameters", {})
            return ProcessingBlock(
                eb_id=data.get("eb_id", None),
                script=None if script is None else ProcessingBlockScript(**script),
                channels_per_port=params.get("channels_per_port", None),
                processors=list(params.get("processors", {}).keys()),
                signal_display_config=params.get("signal_display", {}),
            )
        return None

    def _get_processing_block_state(self, txn, processing_block_id):
        data = txn.processing_block.state(processing_block_id).get()
        if data is not None:
            return ProcessingBlockState(
                deployments=data.get("deployments", None),
                deployments_ready=data.get("deployments_ready", None),
                last_updated=data.get("last_updated", None),
                resources_available=data.get("resources_available", None),
                status=data.get("status", None),
            )
        return None

    def _list_execution_blocks(self, txn, with_values=False):
        if with_values:
            return txn.execution_block.list_values()
        return txn.execution_block.list_keys()

    def _get_execution_block(self, txn, execution_block_id):
        data = txn.execution_block.get(execution_block_id).model_dump()
        if data is not None:
            return data
        return None

    def _get_execution_block_state(self, txn, execution_block_id):
        data = txn.execution_block.state(execution_block_id).get()
        if data is not None:
            return data
        return None

    def _list_deployments(self, txn, with_values=False):
        if with_values:
            return txn.deployment.list_values()
        return txn.deployment.list_keys()

    def _get_deployment(self, txn, deployment_id):
        data = txn.deployment.get(deployment_id)
        if data is not None:
            return data.model_dump()
        return None

    def _get_deployment_state(self, txn, deployment_id):
        return txn.deployment.state(deployment_id).get()

    def _list_flows(self, txn, with_values=False):
        if with_values:
            return txn.flow.list_values(kind="data-queue")

        return txn.flow.list_keys(kind="data-queue")

    def _get_flow(self, txn, flow_name):
        data = txn.flow.get(flow_name)
        if data is not None:
            return data
        return None

    def _get_flow_state(self, txn, flow_name):
        data = txn.flow.state(flow_name).get()
        if data is not None:
            return data
        return None

    def _get_windows(
        self, txn, subarray: str = None, processing_block: str = None, metric: str = None
    ) -> list[Window]:
        windows = []
        flows = self._list_flows(txn, with_values=True)
        for key, value in flows:
            if processing_block is not None and key.pb_id != processing_block:
                continue

            if subarray is not None and subarray not in key.name:
                continue

            if metric is not None and metric not in key.name:
                continue

            if not any(source.function == "SignalDisplayMetrics" for source in value.sources):
                continue
            flow_state = self._get_flow_state(txn, key)

            if flow_state is not None and "windows" in flow_state:
                subwindows = [SubWindow(**window) for window in flow_state["windows"]]
            else:
                subwindows = []

            windows.append(
                Window(
                    topic=value.sink.topics,
                    processing_block=key.pb_id,
                    max_window_count=value.sources[0].parameters["additional_windows"],
                    channels_averaged=value.sources[0].parameters["nchan_avg"],
                    windows=subwindows,
                )
            )
        return windows
