"""Utility helper functions for Kafka."""

import logging
import re
import time
from dataclasses import asdict, is_dataclass
from typing import Union
from unicodedata import normalize

import numpy
import ska_ser_logging

from ska_sdp_qa_data_api.server.core.config import (
    API_DATA_FORMAT,
    API_VERSION,
    LOG_URLS,
    PROJECT_NAME,
    START_TIME,
    START_TIME_FORMATTED,
    WATERFALL_THUMBNAIL_MAX_HEIGHT,
    WATERFALL_THUMBNAIL_MAX_WIDTH,
)
from ska_sdp_qa_data_api.server.core.message_topic import (
    AMPLITUDE,
    BAND_AVERAGED_X_CORR,
    GAIN_CALIBRATION,
    GAIN_CALIBRATION_OUT,
    LAG_PLOT,
    PHASE,
    PHASE_AND_AMPLITUDE,
    POINTING_OFFSET_OUT,
    RECEIVER_STATS,
    RFI,
    SPECTRUM,
    UV_COVERAGE,
    VISIBILITY,
    VISIBILITY_STATS,
)
from ska_sdp_qa_data_api.server.data.responses import ApiConfig

logger = logging.getLogger(__name__)


def _s(value: Union[str, bytes]) -> str:
    if isinstance(value, str):
        return value
    return value.decode()


def slugify(text: str, delim="-"):
    """Generates an slightly worse ASCII-only slug."""
    punct_re = re.compile(r'[\t !"#$%&\'()*\-/<=>?@\[\\\]^_`{|},.:]+')
    result = []
    for word in punct_re.split(text.lower()):
        word = normalize("NFKD", word).encode("ascii", "ignore")
        if word:
            result.append(word.decode("unicode_escape"))
    return delim.join(result)


def configure_parser_and_logs(parser):
    """Add the verbosity settings and configure the logging helper for this
    script."""
    parser.add_argument("--verbose", "-v", action="store_true", help="Increase verbosity(INFO)")
    parser.add_argument("--debug", "-d", action="store_true", help="Increase verbosity(DEBUG)")
    args = parser.parse_args()

    if args.debug:
        ska_ser_logging.configure_logging(logging.DEBUG)
    elif args.verbose:
        ska_ser_logging.configure_logging(logging.INFO)
    else:
        ska_ser_logging.configure_logging(logging.WARNING)

    return args


def topic_names(subarrays=None):
    """Output the names of the topics in use"""
    if subarrays is None:
        return _topic_names()

    if isinstance(subarrays, list):
        return [_topic_names(_s(subarray)) for subarray in subarrays]

    return _topic_names(subarrays)


def _topic_names(subarray=".*"):
    return {
        PHASE_AND_AMPLITUDE: f"metrics-{PHASE_AND_AMPLITUDE}-{subarray}",
        VISIBILITY_STATS: f"metrics-{VISIBILITY_STATS}-{subarray}",
        RFI: f"metrics-{RFI}-{subarray}",
        SPECTRUM: f"metrics-{SPECTRUM}-{subarray}",
        VISIBILITY: f"metrics-{VISIBILITY}-{subarray}",
        RECEIVER_STATS: f"metrics-{RECEIVER_STATS}-{subarray}",
        PHASE: f"metrics-{PHASE}-{subarray}",
        LAG_PLOT: f"metrics-{LAG_PLOT}-{subarray}",
        AMPLITUDE: f"metrics-{AMPLITUDE}-{subarray}",
        POINTING_OFFSET_OUT: f"metrics-{POINTING_OFFSET_OUT}-{subarray}",
        UV_COVERAGE: f"metrics-{UV_COVERAGE}-{subarray}",
        GAIN_CALIBRATION: f"{GAIN_CALIBRATION}-{subarray}",
        GAIN_CALIBRATION_OUT: f"metrics-{GAIN_CALIBRATION_OUT}-{subarray}",
        BAND_AVERAGED_X_CORR: f"metrics-{BAND_AVERAGED_X_CORR}-{subarray}",
    }


class Timer:
    """Simple utility class to output a time around a piece of code."""

    def __init__(self, name=""):
        self.name = name
        self.start = 0

    def __enter__(self):
        self.start = time.time()
        return self

    def __exit__(self, type_, value, traceback):
        logger.info("[%s]: Time taken : %0.6f s", self.name, time.time() - self.start)
        return False


def json_default(obj):
    """Convert our own types to json."""
    if isinstance(obj, complex):
        return [obj.real, obj.imag]
    if isinstance(obj, numpy.float64):
        return float(obj)
    if isinstance(obj, (numpy.ndarray, numpy.number)):
        return obj.tolist()
    if isinstance(obj, set):
        return list(obj)
    if isinstance(obj, bytes):  # pragma: py3
        return obj.decode()
    raise TypeError


def size_to_str(size: int | float) -> str:
    """Convert a byte length to string."""
    if size < 1024:
        return f"{size:.1f} B"
    size /= 1024
    if size < 1024:
        return f"{size:.1f} KB"
    size /= 1024
    if size < 1024:
        return f"{size:.1f} MB"
    size /= 1024
    return f"{size:.1f} GB"


def get_system_config() -> ApiConfig:
    """Return the current system config."""
    topics = topic_names("<subarray>")

    return ApiConfig(
        api_format=API_DATA_FORMAT,
        app_version=API_VERSION,
        project_name=PROJECT_NAME,
        start_time=START_TIME_FORMATTED,
        uptime=time.time() - START_TIME,
        paths={
            "websocket": "/ws",
            "subarrays": "/stats/subarrays",
            "processing_blocks": "/stats/processing_block/blocks",
            "spead2_scans": "/stats/spead2/scans",
            "spectrogram_thumbnail_path": "/spectrograms/thumbnail_pb",
            "lag_plot_thumbnail_path": "/lag_plots/thumbnail_pb",
            "spectrum_waterfalls_thumbnail_path": "/spectrum_waterfalls/thumbnail_pb",
            "log_url": LOG_URLS,
            "mask_data": "/tel_model/mask_data",
        },
        topics={
            PHASE_AND_AMPLITUDE: topics[PHASE_AND_AMPLITUDE],
            SPECTRUM: topics[SPECTRUM],
            PHASE: topics[PHASE],
            AMPLITUDE: topics[AMPLITUDE],
            LAG_PLOT: topics[LAG_PLOT],
            UV_COVERAGE: topics[UV_COVERAGE],
            POINTING_OFFSET_OUT: topics[POINTING_OFFSET_OUT],
            GAIN_CALIBRATION: topics[GAIN_CALIBRATION],
            GAIN_CALIBRATION_OUT: topics[GAIN_CALIBRATION_OUT],
            BAND_AVERAGED_X_CORR: topics[BAND_AVERAGED_X_CORR],
            RECEIVER_STATS: topics[RECEIVER_STATS],
            VISIBILITY_STATS: topics[VISIBILITY_STATS],
            "configuration": "ws",
        },
        waterfall_plots={
            "thumbnail_width": WATERFALL_THUMBNAIL_MAX_WIDTH,
            "thumbnail_max_height": WATERFALL_THUMBNAIL_MAX_HEIGHT,
        },
    )


def recursive_to_dict(obj):
    """Recursively convert given thing to a JSONable object via dicts."""
    if is_dataclass(obj):
        return recursive_to_dict(asdict(obj))

    if isinstance(obj, dict):
        return {_s(key): recursive_to_dict(value) for key, value in obj.items()}

    if isinstance(obj, set):
        return recursive_to_dict(list(obj))

    if isinstance(obj, list):
        return [recursive_to_dict(item) for item in obj]

    if isinstance(obj, bytes):
        return obj.decode()

    return obj
