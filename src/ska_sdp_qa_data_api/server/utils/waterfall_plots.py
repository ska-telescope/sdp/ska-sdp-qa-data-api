# pylint: disable=R0903
"""Processes for creating waterfall plots."""

import struct
import time

import numpy as np

from ska_sdp_qa_data_api.server.core.config import (
    FAR_FUTURE,
    FAR_PAST,
    WATERFALL_THUMBNAIL_MAX_HEIGHT,
    WATERFALL_THUMBNAIL_MAX_WIDTH,
)
from ska_sdp_qa_data_api.server.utils.exceptions import NoDataException


def shrink_numpy_row(input_row):
    """Shrink the input data to thumbnail size."""
    current_width = np.shape(input_row)[0]
    if current_width < WATERFALL_THUMBNAIL_MAX_WIDTH:
        return input_row

    divisions = current_width // WATERFALL_THUMBNAIL_MAX_WIDTH

    main_columns = divisions * WATERFALL_THUMBNAIL_MAX_WIDTH

    output = input_row[:main_columns].reshape(-1, divisions).mean(axis=1, dtype="i4")

    if current_width > main_columns:
        output = np.append(output, input_row[main_columns:].mean(axis=0, dtype="i4"))

    return output


class WaterfallPlots:
    """Class to manage all access for creating waterfall plots."""

    def __init__(self, redis):
        self.redis = redis

    def _encode_np_array(self, np_array, timeseries=False, timestamp=1):
        """Given a numpy array,
        converts the numpy array to a bytes object.
        """

        if np.shape(np_array)[0] > WATERFALL_THUMBNAIL_MAX_WIDTH:
            if len(np.shape(np_array)) > 1:
                thumbnail = np.apply_along_axis(shrink_numpy_row, 1, np_array)
            else:
                thumbnail = np.apply_along_axis(shrink_numpy_row, 0, np_array)
        else:
            thumbnail = np_array.astype("i4")

        width = thumbnail.shape[0]
        shape = struct.pack(">I", width)
        if timeseries:
            t = struct.pack(">Q", int(timestamp * 1000000))
            encoded = t + shape + thumbnail.tobytes()
        else:
            encoded = shape + thumbnail.tobytes()
        return encoded

    def _store_np_as_timeseries(self, key, np_array):
        """Given a key and numpy array,
        converts the numpy array to a bytes object.
        Then stores the bytes object in redis time series
        with key."""
        t = time.time()
        encoded = self._encode_np_array(np_array, timeseries=True, timestamp=t)
        return self.redis.zadd("waterfall:" + key.lower(), {encoded: t})

    def retrieve_np_from_timeseries(self, key, start=None, end=None):
        """Retrieves the numpy array from the redis time series given a key and range."""

        if start is None:
            start = FAR_PAST
        if end is None:
            end = FAR_FUTURE
        encoded_list = self.redis.zrange(
            "waterfall:" + key.lower(), start=start, end=end, byscore=True
        )
        if not encoded_list:
            raise NoDataException(f"No data for baseline: {key}")
        return np.concatenate(
            [
                np.frombuffer(encoded[12:], dtype="i4").reshape(
                    1, struct.unpack(">I", encoded[8:12])[0]
                )
                for encoded in encoded_list[::-1]
            ][-WATERFALL_THUMBNAIL_MAX_HEIGHT:]
        )
