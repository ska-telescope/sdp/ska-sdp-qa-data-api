"""Kafka helper functions"""

import logging

from aiokafka.admin.client import AIOKafkaAdminClient
from aiokafka.admin.new_topic import NewTopic

from ska_sdp_qa_data_api.server.core.config import BROKER_INSTANCE

logger = logging.getLogger(__name__)


async def force_creation_of_topics(topics: list[str]):
    """Create topics if they do not exist"""
    admin = AIOKafkaAdminClient(bootstrap_servers=BROKER_INSTANCE)
    logger.info("Creating topics: %s", topics)
    await admin.start()
    await admin.create_topics(
        [NewTopic(name=topic, num_partitions=1, replication_factor=1) for topic in topics]
    )
    await admin.close()
