"""
This module provides functions for handling authentication and authorization within the
application.
"""

import logging
from typing import Optional

import httpx
from fastapi import Request
from httpx import ConnectError, HTTPStatusError, TimeoutException

from ska_sdp_qa_data_api.server.core.config import SKA_PERMISSIONS_URL
from ska_sdp_qa_data_api.server.utils.exceptions import AuthError

logger = logging.getLogger(__name__)


async def get_token_from_auth_header(request: Request) -> Optional[str]:
    """
    Obtains the Access Token from the Authorization Header.

    Raises:
        AuthError: If the Authorization header is missing, invalid, or malformed.
    """

    auth_header = request.headers.get("Authorization")

    if not auth_header:
        raise AuthError("authorization_header_missing: Authorization header is expected.", 401)

    parts = auth_header.split()

    if len(parts) != 2 or parts[0].lower() != "bearer":
        raise AuthError(
            "invalid_header: Authorization header must be in the format Bearer token.", 401
        )

    return parts[1]


async def get_token_from_request(request: Request) -> str:
    """Extracts an access token from a request.

    Args:
        request: The incoming request.

    Returns:
        The unvalidated access token.

    Raises:
        HTTPException: If token is not found.
    """
    try:
        token = await get_token_from_auth_header(request)
        return token
    except AuthError as error:
        logger.warning("No valid token found, error: %s", error)
        raise error


async def user_profile(request: Request) -> dict[str, dict[str, str]]:
    """Fetches user profile from the permissions API.

    Args:
        token: The access token.

    Returns:
        A dictionary containing the user's email, or mocked details of a guest user if no user
        is authenticated.

    Raises:
        None
    """
    try:
        token: str = await get_token_from_request(request)

        headers = {"Authorization": f"Bearer {token}"}
        async with httpx.AsyncClient(timeout=10) as client:
            permissions_api_verification_endpoint = f"{SKA_PERMISSIONS_URL}/v2/user/profile"
            response = await client.get(permissions_api_verification_endpoint, headers=headers)
            response.raise_for_status()
            return response.json()
    except (HTTPStatusError, AuthError, ConnectError, TimeoutException) as error:
        logger.warning("Error fetching user profile, using Guest profile instead: %s", error)
        return {
            "user_profile": {
                "displayName": "Guest",
                "givenName": "Guest",
                "surname": "Guest",
                "userPrincipalName": "guest@community.skao.int",
                "id": "aaaabbbb-cccc-dddd-eeee-ffffgggghhhh",
            }
        }
