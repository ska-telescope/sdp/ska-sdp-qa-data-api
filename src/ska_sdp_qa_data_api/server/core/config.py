"""
Config script for ska_sdp_qa_data_api
"""

import logging
import time
from datetime import datetime
from pathlib import Path

from starlette.config import Config

import ska_sdp_qa_data_api

ENV_FILE = Path(".env")
if not ENV_FILE.exists():  # pragma: no cover
    ENV_FILE = None

config = Config(ENV_FILE)

PROJECT_NAME: str = config("PROJECT_NAME", default="SDP Signal Metrics API")
REDIS_HOST: str = config("REDIS_HOST", default="redis")
REDIS_PORT: str = config("REDIS_PORT", default="6379")
REDIS_URL: str = f"redis://{REDIS_HOST}:{REDIS_PORT}"
BROKER_INSTANCE = config("BROKER_INSTANCE", default="localhost:9092")
WATERFALL_DEFAULT_IMAGE = config(
    "WATERFALL_DEFAULT_IMAGE", default="./server/utils/Images/default_image.bmp"
)
API_URL_SUBDIRECTORY: str = config("API_URL_SUBDIRECTORY", default="")
API_DATA_FORMAT: str = config("API_DATA_FORMAT", default="msgpack")
API_VERSION: str = config("API_VERSION", default=ska_sdp_qa_data_api.__version__)
WATERFALL_THUMBNAIL_MAX_WIDTH: int = config("WATERFALL_THUMBNAIL_MAX_WIDTH", cast=int, default=512)
WATERFALL_THUMBNAIL_MAX_HEIGHT: int = config(
    "WATERFALL_THUMBNAIL_MAX_HEIGHT", cast=int, default=164
)

DATA_EXPIRY: int = config("DATA_EXPIRY", cast=int, default=12)
WATERFALL_CLEANUP_SECONDS: int = config("WATERFALL_CLEANUP_SECONDS", cast=int, default=60 * 60)

DEBUG: bool = config("API_VERBOSE", cast=bool, default=False)
LOGGING_LEVEL = logging.DEBUG if DEBUG else logging.WARNING
ALLOW_PROFILING: bool = config("ALLOW_PROFILING", cast=bool, default=True)

START_TIME_FORMATTED = datetime.now().astimezone().isoformat()
START_TIME = time.time()

NAMESPACE = config("NAMESPACE", default="default")
PROCESSING_NAMESPACE = config("PROCESSING_NAMESPACE", default=f"{NAMESPACE}-p")

LOG_URLS = "/log-dashboards"
GRAFANA_DASHBOARD = config(
    "GRAFANA_DASHBOARD",
    default="dad0e09f-32f5-4181-b273-c7a1017221ff/kubernetes-resource-monitoring-dashboard",
)
SHARED_CLUSTER_URL = config("SHARED_CLUSTER_URL", default="https://k8s.stfc.skao.int")
MONITORING_URL = config("MONITORING_URL", default="https://monitoring.skao.int")

KAFKA_MAX_MESSAGE_SIZE_BYTES: int = config(
    "KAFKA_MAX_MESSAGE_SIZE_BYTES", default=524_288_000, cast=int
)

FAR_PAST: int = config("FAR_PAST", default=172234872, cast=int)

FAR_FUTURE: int = config("FAR_FUTURE", default=17223487240, cast=int)

# SKA Permissions API
SKA_PERMISSIONS_URL: str = config(
    "SKA_PERMISSIONS_URL",
    default="http://localhost:8000",
)
