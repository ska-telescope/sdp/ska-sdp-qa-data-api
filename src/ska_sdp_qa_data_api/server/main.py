"""This RESTful API server subscribes to the [message broker]\
(https://gitlab.com/ska-telescope/ska-sdp-qa-metric-generator) for \
receiving Signal data. This is used by the [Signal display]\
(https://gitlab.com/ska-telescope/sdp/ska-sdp-qa-display)."""

import logging
import logging.handlers

import ska_ser_logging
from fastapi import FastAPI, Request
from fastapi.responses import HTMLResponse, JSONResponse
from pyinstrument import Profiler
from starlette.middleware.cors import CORSMiddleware

from ska_sdp_qa_data_api.server.api import router as endpoint_router
from ska_sdp_qa_data_api.server.core.config import (
    ALLOW_PROFILING,
    API_URL_SUBDIRECTORY,
    API_VERSION,
    LOGGING_LEVEL,
    PROJECT_NAME,
)
from ska_sdp_qa_data_api.server.core.settings import DEFAULT_ROUTE_STR

ska_ser_logging.configure_logging(LOGGING_LEVEL)
logger = logging.getLogger(__name__)

tags_metadata = [
    {"name": "core", "description": "Core API endpoints, used for configuration and monitoring"},
    {"name": "info", "description": "Returns information on current scans"},
    {"name": "config", "description": "Retrieve information from the Config DB"},
    {"name": "metric", "description": "Returns or sends metric data"},
    {"name": "image", "description": "Retrieve an image"},
    {"name": "tel-model", "description": "Endpoints that call the tel-model repo"},
]

app = FastAPI(
    title=PROJECT_NAME,
    version=API_VERSION,
    openapi_tags=tags_metadata,
    root_path=API_URL_SUBDIRECTORY,
)

CORSMiddleware_params = {
    "allow_origins": ["*"],
    "allow_credentials": True,
    "allow_methods": ["*"],
    "allow_headers": ["*"],
}

app.add_middleware(CORSMiddleware, **CORSMiddleware_params)
app.include_router(endpoint_router, prefix=DEFAULT_ROUTE_STR)


@app.exception_handler(ValueError)
async def value_error_exception_handler(
    request: Request, exc: ValueError
):  # pylint: disable=unused-argument
    """Default handler for ValueError exceptions"""
    return JSONResponse(
        status_code=400,
        content={"message": str(exc)},
    )


if ALLOW_PROFILING:  # pragma: no cover

    @app.middleware("http")
    async def profile_request(request: Request, call_next):
        """Allow API profiling, replaces content."""
        profiling = request.query_params.get("profile", False)
        if profiling:
            profiler = Profiler(async_mode="enabled")
            profiler.start()
            await call_next(request)
            profiler.stop()
            return HTMLResponse(profiler.output_html())
        return await call_next(request)
