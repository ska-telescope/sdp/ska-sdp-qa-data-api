"""This is the task that will read from Kafka, and update redis."""

import asyncio

from redis import Redis
from ska_sdp_dataqueues.schemas.signal_display_metrics import metrics_from_dict

from ska_sdp_qa_data_api.server.core.config import REDIS_HOST, REDIS_PORT
from ska_sdp_qa_data_api.server.core.message_topic import LAG_PLOT
from ska_sdp_qa_data_api.server.data import LagPlots
from ska_sdp_qa_data_api.server.tasks.kafka_task import KafkaTask


class ReceiveWaterfallLag(KafkaTask):
    """Receive the data for the Lag Waterfall plots"""

    def __init__(self):
        super().__init__()
        self.description = "Receive and save incoming lag plots"
        self.redis = Redis(host=REDIS_HOST, port=REDIS_PORT)
        self.lag_plots = LagPlots(self.redis)
        self._metric_type = LAG_PLOT

    async def setup(self):
        asyncio.create_task(self.watch_for_flows())

    async def receive(self, topic: str, value: dict | list):
        """Receive new message."""
        data = metrics_from_dict(value)
        self.lag_plots.process_processed(data)


def main():  # pragma: no cover
    """Main function."""
    ReceiveWaterfallLag().run()


if __name__ == "__main__":  # pragma: no cover
    main()
