"""Contains a generic kafka task."""

import argparse
import asyncio
import logging

from ska_sdp_dataqueues import DataQueueConsumer, Encoding

from ska_sdp_qa_data_api.server.core.config import BROKER_INSTANCE
from ska_sdp_qa_data_api.server.utils.config_db import ConfigDBTypes, SDPConfig
from ska_sdp_qa_data_api.server.utils.helpers import configure_parser_and_logs, topic_names
from ska_sdp_qa_data_api.server.utils.kafka_helpers import force_creation_of_topics

logger = logging.getLogger(__name__)

# pylint: disable=too-many-instance-attributes


class KafkaTask:
    """This is a wrapper for kafka that makes a task consumer easier."""

    def __init__(self):
        self.topics: list[str] = []
        self.description = ""
        self.auto_offset_reset = "latest"
        self.config = None
        self._flows = []
        self.encoding = Encoding.MSGPACK_NUMPY
        self._data_queue = None
        self._force_topic_creation = False
        self._flow_function = "SignalDisplayMetrics"
        self._metric_type = None

    def run(self):  # pragma: no cover
        """This is to be run by a "main" function.

        We start the async runner here and parse any command line variables
        here.
        """
        logger.info("[%s]: Starting cron", self.__class__.__name__)
        parser = argparse.ArgumentParser(description=self.description)
        configure_parser_and_logs(parser)
        asyncio.run(self.run_consumer())

    async def run_consumer(self):
        """This runs the actual consumer, and calls user functions."""
        self._data_queue = DataQueueConsumer(
            server=BROKER_INSTANCE,
            set_group_id=False,
            auto_offset_reset=self.auto_offset_reset,
            set_consumer_offsets=False,
            encoding=self.encoding,
        )

        async with self._data_queue:
            logger.info("Kafka has connected")
            await self.setup()
            await asyncio.sleep(0.1)  # this gives a chance for the setup to finish

            try:
                logger.info("Awaiting message...")
                # Consume messages
                async for topic, message in self._data_queue:
                    logger.info(
                        "Received message on: %s",
                        topic,
                    )
                    await self.receive(topic, message)
            finally:
                # Will leave consumer group; perform autocommit if enabled.
                logger.info("We are shutdown")
                await self.on_exit()

    async def setup(self):
        """Over ride this if you require async setups."""

    async def on_exit(self):
        """Override this if you have custome shutdown logic."""

    async def receive(self, topic: str, value: dict | list):
        """Override this function to receive a new message.

        If you want to catch exception you need to handle it in your own
        function."""
        raise NotImplementedError("'receive' not overwritten")

    async def watch_for_subarrays(self, topic_name, try_once=False):
        """Wait for subarrays to become available, then subscribe."""
        if self.config is None:
            self.config = SDPConfig()

        if try_once:
            self.config.reconnect()
        else:
            while self.config.reconnect() is False:
                logger.info(
                    "[%s]: Config DB didn't connect, trying again...", self.__class__.__name__
                )
                await asyncio.sleep(1)

        if self.config.reconnect() is True:
            logger.info("[%s]: Config DB is connected, switching topics", self.__class__.__name__)
            self.topics = [topics[topic_name] for topics in topic_names(self.config.subarrays())]
            while len(self.topics) == 0:
                logger.info("[%s]: No subarrays found trying again...", self.__class__.__name__)
                await asyncio.sleep(1)
                self.topics = [
                    topics[topic_name] for topics in topic_names(self.config.subarrays())
                ]
            logger.info("[%s]: Connecting to topics <%s>", self.__class__.__name__, self.topics)

            # Old version of topics, these do not have partitions.
            if self._force_topic_creation:
                try:
                    await force_creation_of_topics(self.topics)
                except Exception as err:  # pylint: disable=broad-exception-caught
                    logger.exception(err)
            await self._data_queue.update_topics(self.topics)

    async def watch_for_flows(self):
        """Watch for new flows, and update as required."""
        if self.config is None:
            self.config = SDPConfig()

        while self.config.reconnect() is False:
            logger.info("[%s]: Config DB didn't connect, trying again...", self.__class__.__name__)
            await asyncio.sleep(1)

        logger.info(
            "[%s]: Config DB is connected, starting watcher thread", self.__class__.__name__
        )
        await self.config.watcher_thread(
            type_name=ConfigDBTypes.LIST_FLOW,
            type_id=None,
            callback=self._flow_list,
        )

    async def _flow_list(
        self, type_name: ConfigDBTypes, type_id: str | None, flow_list: list
    ):  # pylint: disable=unused-argument
        try:
            logger.info("[%s]: New flow list received", self.__class__.__name__)
            topics: list[str] = []
            flows = []
            failed = []
            for flow in flow_list:
                logger.info("[%s]: Checking flow '%s'", self.__class__.__name__, flow.name)
                config_flow = self.config.flow(flow)
                if config_flow is None:
                    logger.error(
                        "[%s]: Received a flow with no content: %s [Filter:%s|%s]",
                        self.__class__.__name__,
                        flow.name,
                        self._flow_function,
                        self._metric_type,
                    )
                    failed.append(flow)
                    continue

                for source in config_flow.sources:
                    if self._flow_function is not None and (
                        source.function != self._flow_function
                    ):
                        continue

                    if (
                        self._metric_type is not None
                        and source.parameters["metric_type"] != self._metric_type
                    ):
                        continue

                    topics.append(config_flow.sink.topics)
                    flows.append(config_flow)

            for flow in failed:
                logger.error(
                    "[%s]: Unable to retrieve flow: %s [Filter:%s|%s]",
                    self.__class__.__name__,
                    flow.name,
                    self._flow_function,
                    self._metric_type,
                )
            self.topics = topics
            self._flows = flows

            # New style topics, these use partitions. And we only care about the low
            # resolution flows, so we can use partition=0, which by convection is the
            # low resolution partition
            logger.info("[%s]: Connecting to topics <%s>", self.__class__.__name__, self.topics)
            await self._data_queue.assign_topics([(topic, 0) for topic in self.topics])
        except Exception as exc:  # pylint: disable=broad-exception-caught
            logger.exception(exc)
