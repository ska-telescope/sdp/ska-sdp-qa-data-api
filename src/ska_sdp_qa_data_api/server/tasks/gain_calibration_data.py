"""Process incoming calibration data."""

import asyncio
import logging

from ska_sdp_dataqueues import DataQueueProducer, Encoding

from ska_sdp_qa_data_api.server.core.config import BROKER_INSTANCE, KAFKA_MAX_MESSAGE_SIZE_BYTES
from ska_sdp_qa_data_api.server.core.message_topic import GAIN_CALIBRATION, GAIN_CALIBRATION_OUT
from ska_sdp_qa_data_api.server.data import GainCalibration
from ska_sdp_qa_data_api.server.tasks.kafka_task import KafkaTask
from ska_sdp_qa_data_api.server.utils.helpers import Timer, topic_names

logger = logging.getLogger(__name__)

# pylint: disable=protected-access,duplicate-code


class ReceiveGainCalibrationData(KafkaTask):
    """Process incoming gain calibration data.

    Note that this cannot use flows as of yet, as the process doesn't support
    flows."""

    def __init__(self):
        super().__init__()
        self.description = "Receive and process gain calibration data"
        self._force_topic_creation = True
        self.encoding = None
        self._producer = DataQueueProducer(
            server=BROKER_INSTANCE,
            message_max_bytes=KAFKA_MAX_MESSAGE_SIZE_BYTES,
            encoding=Encoding.MSGPACK_NUMPY,
        )

    async def setup(self):
        await self._producer._producer_start()
        asyncio.create_task(self.watch_for_subarrays(GAIN_CALIBRATION))

    async def on_exit(self):
        await self._producer._producer_stop()

    async def receive(self, topic: str, value: bytes):
        with Timer("Import Gain Calibration"):
            gain_calibration = GainCalibration(value)
        with Timer("Process Gain Calibration"):
            await self._process_calibration_data(topic, gain_calibration)

    async def _process_calibration_data(self, topic, gain_calibration):
        topics = topic_names(topic.split("-")[-1])

        gain_calibration_dict = {
            "time": gain_calibration.time_received,
            "gains": gain_calibration.gain_amplitudes,
            "phases": gain_calibration.gain_phases,
        }

        with Timer("Output Gain Calibrations"):
            await self._producer.send(
                data=gain_calibration_dict,
                topic=topics[GAIN_CALIBRATION_OUT],
            )


def main():  # pragma: no cover
    """Main function."""
    ReceiveGainCalibrationData().run()


if __name__ == "__main__":  # pragma: no cover
    main()
