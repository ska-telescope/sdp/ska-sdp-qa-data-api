"""Process incoming calibration data."""

import asyncio
import logging

from ska_sdp_dataqueues import DataQueueProducer, Encoding

from ska_sdp_qa_data_api.server.core.config import BROKER_INSTANCE, KAFKA_MAX_MESSAGE_SIZE_BYTES
from ska_sdp_qa_data_api.server.core.message_topic import POINTING_OFFSET_OUT
from ska_sdp_qa_data_api.server.data import PointingOffset
from ska_sdp_qa_data_api.server.tasks.kafka_task import KafkaTask
from ska_sdp_qa_data_api.server.utils.helpers import Timer, topic_names

logger = logging.getLogger(__name__)

# pylint: disable=protected-access,duplicate-code


class ReceiveCalibrationData(KafkaTask):
    """Process incoming calibration data."""

    def __init__(self):
        super().__init__()
        self.description = "Receive and process calibration data"
        self._force_topic_creation = True
        self._producer = DataQueueProducer(
            server=BROKER_INSTANCE,
            message_max_bytes=KAFKA_MAX_MESSAGE_SIZE_BYTES,
            encoding=Encoding.MSGPACK_NUMPY,
        )
        self._flow_function = "ska-sdp-wflow-pointing-offset:pointing-offset"

    async def setup(self):
        await self._producer._producer_start()
        asyncio.create_task(self.watch_for_flows())

    async def on_exit(self):
        await self._producer._producer_stop()

    async def receive(self, topic: str, value: dict | list):
        with Timer("Import Pointing Offset"):
            pointing_offset = PointingOffset(value)
        with Timer("Process Pointing Offset"):
            await self._process_calibration_data(topic, pointing_offset)

    async def _process_calibration_data(self, topic, pointing_offset):
        topics = topic_names(topic.split("-")[-1])
        pointing_offset_dict = {
            "antenna_name": pointing_offset.antennas,
            "xel_offset": pointing_offset.xel_offset,
            "xel_offset_std": pointing_offset.xel_offset_std,
            "el_offset": pointing_offset.el_offset,
            "el_offset_std": pointing_offset.el_offset_std,
            "expected_width_h": pointing_offset.expected_width_h,
            "expected_width_v": pointing_offset.expected_width_v,
            "fitted_width_h": pointing_offset.fitted_width_h,
            "fitted_width_h_std": pointing_offset.fitted_width_h_std,
            "fitted_width_v": pointing_offset.fitted_width_v,
            "fitted_width_v_std": pointing_offset.fitted_width_v_std,
            "fitted_height": pointing_offset.fitted_height,
            "fitted_height_std": pointing_offset.fitted_height_std,
        }

        with Timer("Output Pointing Offsets"):
            await self._producer.send(
                data=pointing_offset_dict,
                topic=topics[POINTING_OFFSET_OUT],
            )


def main():  # pragma: no cover
    """Main function."""
    ReceiveCalibrationData().run()


if __name__ == "__main__":  # pragma: no cover
    main()
