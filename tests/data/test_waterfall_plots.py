"""Test for the waterfall plots task."""

# pylint: disable=protected-access

import csv
import filecmp
import json
import re
import time

import fakeredis
import numpy as np
import pytest

from ska_sdp_qa_data_api.server.data import LagPlots, SpectrogramPlots, SpectrumWaterfalls
from ska_sdp_qa_data_api.server.utils.exceptions import NoDataException
from tests.utils.test_utils import REFERENCE_DICT, TEST_DATA_CSV, remove_if_exists

TEST_JSON_MSG = "tests/test_data/test_json.txt"
OUTPUT_THUMBNAIL_REFERENCE_BMP = "tests/test_data/thumbnail_test_file_output.bmp"
OUTPUT_THUMBNAIL_REFERENCE_BMP_22 = "tests/test_data/thumbnail_test_file_22_output.bmp"
OUTPUT_FULL_IMAGE_REFERENCE_BMP = "tests/test_data/full_image_test_file_output.bmp"
FULL_IMAGE_REFERENCE_BMP = "tests/test_data/full_image_test_file.bmp"
THUMBNAIL_REFERENCE_BMP = "tests/test_data/thumbnail_test_file.bmp"
THUMBNAIL_REFERENCE_BMP_22 = "tests/test_data/thumbnail_test_file_22.bmp"

REDIS_KEY = "111111_m033_m033_XX"


def test_transform_json_messages_no_key():
    """Test to see if test message is correctly transformed into iterable
    if no key exists."""
    local_redis = fakeredis.FakeStrictRedis()
    with open(TEST_JSON_MSG, "r", encoding="UTF-8") as file_data:
        input_message = json.load(file_data)

    with open(REFERENCE_DICT, "r", encoding="UTF-8") as file_data:
        reference_dict = json.loads(file_data.read())

    image_creator = SpectrogramPlots(local_redis)
    for spectrogram in input_message["spectrogram"]:
        image_creator._process_spectrogram(
            spectrogram["processing_block_id"],
            spectrogram["baseline"],
            spectrogram["polarisation"],
            np.array(spectrogram["phase"], dtype="i4"),
        )
    list_to_compare = image_creator.retrieve_np_from_timeseries(REDIS_KEY).tolist()
    compare_string = re.sub(
        r"[\[\] ]",
        "",
        ",".join(str(e) for e in list_to_compare),
    )

    assert compare_string == reference_dict[REDIS_KEY][0]


@pytest.mark.parametrize(
    "func",
    [(SpectrogramPlots), (LagPlots), (SpectrumWaterfalls)],
)
def test_retrieve_np_from_redis_no_data(func):
    """Test to see if test message is correctly transformed into iterable
    if key exists."""
    local_redis = fakeredis.FakeStrictRedis()
    image_creator = func(local_redis)
    local_redis.zadd("INVALID_KEY", {"a": 1})
    with pytest.raises(NoDataException) as exc_info:
        image_creator.retrieve_np_from_timeseries("INVALID_KEY")
    assert exc_info.type is NoDataException


def test_transform_json_messages_key_exists():
    """Test to see if test message is correctly transformed into iterable
    if key exists."""
    local_redis = fakeredis.FakeStrictRedis()
    with open(TEST_JSON_MSG, "r", encoding="UTF-8") as file_data:
        input_message = json.load(file_data)

    with open(REFERENCE_DICT, "r", encoding="UTF-8") as file_data:
        reference_dict = json.loads(file_data.read())

    image_creator = SpectrogramPlots(local_redis)
    list_to_store = np.full(shape=1024, fill_value=140, dtype=np.int32)
    image_creator._store_np_as_timeseries(REDIS_KEY, list_to_store)
    for spectrogram in input_message["spectrogram"]:
        image_creator._process_spectrogram(
            spectrogram["processing_block_id"],
            spectrogram["baseline"],
            spectrogram["polarisation"],
            np.array(spectrogram["phase"], dtype="i4"),
        )

    all_data = image_creator.retrieve_np_from_timeseries(REDIS_KEY).tolist()
    # Check that old data still exists
    list_to_compare = all_data[0]
    compare_string = re.sub(
        r"[\[\] ]",
        "",
        ",".join(str(e) for e in list_to_compare),
    )

    assert compare_string == reference_dict[REDIS_KEY][0]

    # Check that new data also exists
    list_to_compare = all_data[1]
    compare_string = re.sub(
        r"[\[\] ]",
        "",
        ",".join(str(e) for e in list_to_compare),
    )
    assert compare_string == reference_dict[REDIS_KEY][0].replace("180", "140").replace("60", "46")


@pytest.mark.parametrize(
    "func",
    [(SpectrogramPlots), (LagPlots), (SpectrumWaterfalls)],
)
def test_create_thumbnail_byte_array_less_than_19_records(func):
    """Test to see if thumbnail image is correctly created
    and pads thumbnail."""
    local_redis = fakeredis.FakeStrictRedis()
    remove_if_exists(OUTPUT_THUMBNAIL_REFERENCE_BMP)
    with open(TEST_DATA_CSV, "r", encoding="UTF-8", newline="") as csv_file:
        data_iter = csv.reader(csv_file, delimiter=",", quotechar='"')
        data_iter = list(data_iter)
    image_creator = func(local_redis)

    for phase_data in data_iter[-15:-1]:
        image_creator._store_np_as_timeseries(REDIS_KEY, np.array(phase_data, dtype="i4"))

    thumbnail_image_bytes = image_creator.retrieve_np_from_timeseries(REDIS_KEY)
    with open(OUTPUT_THUMBNAIL_REFERENCE_BMP, "wb") as bmp_file:
        bmp_file.write(thumbnail_image_bytes)
    assert filecmp.cmp(THUMBNAIL_REFERENCE_BMP, OUTPUT_THUMBNAIL_REFERENCE_BMP, shallow=False)
    remove_if_exists(OUTPUT_THUMBNAIL_REFERENCE_BMP)


@pytest.mark.parametrize(
    "func",
    [(SpectrogramPlots), (LagPlots), (SpectrumWaterfalls)],
)
def test_create_thumbnail_byte_array(func):
    """Test to see if thumbnail image is correctly created."""
    local_redis = fakeredis.FakeStrictRedis()
    remove_if_exists(OUTPUT_THUMBNAIL_REFERENCE_BMP_22)
    with open(TEST_DATA_CSV, "r", encoding="UTF-8", newline="") as csv_file:
        data_iter = csv.reader(csv_file, delimiter=",", quotechar='"')
        data_iter = list(data_iter)
    image_creator = func(local_redis)

    for phase_data in data_iter:
        image_creator._store_np_as_timeseries(REDIS_KEY, np.array(phase_data, dtype="i4"))

    thumbnail_image_bytes = image_creator.retrieve_np_from_timeseries(REDIS_KEY)
    with open(OUTPUT_THUMBNAIL_REFERENCE_BMP_22, "wb") as bmp_file:
        bmp_file.write(thumbnail_image_bytes)
    assert filecmp.cmp(
        THUMBNAIL_REFERENCE_BMP_22,
        OUTPUT_THUMBNAIL_REFERENCE_BMP_22,
        shallow=False,
    )
    remove_if_exists(OUTPUT_THUMBNAIL_REFERENCE_BMP_22)


@pytest.mark.parametrize(
    "func",
    [(SpectrogramPlots), (LagPlots), (SpectrumWaterfalls)],
)
def test_create_thumbnail_byte_array_with_timestamp(func):
    """
    Test to see if thumbnail image is correctly created when
    retrieved from redis based on time.
    """
    local_redis = fakeredis.FakeStrictRedis()
    remove_if_exists(OUTPUT_THUMBNAIL_REFERENCE_BMP_22)
    with open(TEST_DATA_CSV, "r", encoding="UTF-8", newline="") as csv_file:
        data_iter = csv.reader(csv_file, delimiter=",", quotechar='"')
        data_iter = list(data_iter)
    image_creator = func(local_redis)

    for phase_data in data_iter:
        image_creator._store_np_as_timeseries(REDIS_KEY, np.array(phase_data, dtype="i4"))

    thumbnail_image_bytes = image_creator.retrieve_np_from_timeseries(
        REDIS_KEY, start=None, end=time.time()
    )

    with open(OUTPUT_THUMBNAIL_REFERENCE_BMP_22, "wb") as bmp_file:
        bmp_file.write(thumbnail_image_bytes)
    assert filecmp.cmp(
        THUMBNAIL_REFERENCE_BMP_22,
        OUTPUT_THUMBNAIL_REFERENCE_BMP_22,
        shallow=False,
    )
    remove_if_exists(OUTPUT_THUMBNAIL_REFERENCE_BMP_22)
