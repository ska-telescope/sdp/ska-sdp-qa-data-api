"""Tests for the Signal Display Config DB wrapper"""

# pylint: disable=protected-access,unused-argument,too-many-lines,duplicate-code
# pylint: disable=unnecessary-dunder-call

from collections import namedtuple
from dataclasses import asdict
from unittest.mock import MagicMock, call, patch

import pytest
import requests
from ska_sdp_dataqueues.schemas.signal_display_metrics import MetricDataTypes

from ska_sdp_qa_data_api.server.data.inputs import CreateWindow
from ska_sdp_qa_data_api.server.data.responses import SubWindow, Window
from ska_sdp_qa_data_api.server.utils.config_db import ConfigDBTypes, SDPConfig


class FakeDumpModel:
    """Dump-able Config DB Model"""

    def __init__(self, content):
        self.content = content

    def model_dump(self):
        """Return the content as a dictionary."""
        return self.content

    def __getattr__(self, name):
        return self.content[name]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_connect(mock_config):
    """Test connect"""
    config = SDPConfig()
    assert config.connect() is True
    assert mock_config.mock_calls == [call()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_reconnect(mock_config):
    """Test reconnect"""
    config = SDPConfig()
    assert config.reconnect() is True
    assert mock_config.mock_calls == [call()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_reconnect_connected(mock_config):
    """Test reconnect after connect"""
    config = SDPConfig()
    assert config.connect() is True
    assert config.reconnect() is True
    assert mock_config.mock_calls == [call()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_subarrays_with_db_and_transactions(mock_config):
    """Normal usage"""
    transaction = MagicMock()
    transaction.component.query_keys.return_value = ["lmc-subarray-AA"]
    mock_config.return_value.txn.return_value = [transaction]
    config = SDPConfig()
    config.connect()

    assert config.subarrays() == ["AA"]

    assert mock_config.mock_calls == [call(), call().txn()]
    assert transaction.mock_calls == [
        call.component.query_keys(component_name_prefix="lmc-subarray")
    ]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_subarrays_with_db_and_no_transactions(mock_config):
    """Can connect, but no transactions are available"""
    mock_config.return_value.txn.return_value = []
    config = SDPConfig()
    config.connect()

    assert config.subarrays() == []

    assert mock_config.mock_calls == [call(), call().txn()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_subarrays_with_no_db(mock_config):
    """Config DB cannot be connected"""
    mock_config.side_effect = requests.exceptions.ConnectionError
    config = SDPConfig()
    config.connect()

    assert config.subarrays() == []

    assert mock_config.mock_calls == [call()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_subarray_with_db_and_transactions(mock_config):
    """Normal usage"""
    transaction = MagicMock()
    transaction.component.return_value.get.return_value = {
        "eb_id": "eb",
        "command": "startup",
        "command_time": 123.456,
        "obs_state_commanded": "READY",
        "resources": [{"csp_links": [1, 2, 3], "receive_nodes": 2, "receptors": ["R1", "R2"]}],
        "state_commanded": "IDLE",
    }
    mock_config.return_value.txn.return_value = [transaction]
    config = SDPConfig()
    config.connect()

    assert asdict(config.subarray("QQ")) == {
        "eb_id": "eb",
        "last_command": "startup",
        "last_command_time": 123.456,
        "obs_state_commanded": "READY",
        "resources": [{"csp_links": [1, 2, 3], "receive_nodes": 2, "receptors": ["R1", "R2"]}],
        "state_commanded": "IDLE",
    }

    assert mock_config.mock_calls == [call(), call().txn()]
    assert transaction.mock_calls == [call.component("lmc-subarray-QQ"), call.component().get()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_subarray_with_db_and_no_transactions(mock_config):
    """Can connect, but no transactions are available"""
    mock_config.return_value.txn.return_value = []
    config = SDPConfig()
    config.connect()

    data = config.subarray("QQ")

    assert data is None

    assert mock_config.mock_calls == [call(), call().txn()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_subarray_with_no_db(mock_config):
    """Config DB cannot be connected"""
    mock_config.side_effect = requests.exceptions.ConnectionError
    config = SDPConfig()
    config.connect()

    data = config.subarray("QQ")

    assert data is None

    assert mock_config.mock_calls == [call()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_processing_blocks_with_db_and_transactions(mock_config):
    """Normal usage"""
    transaction = MagicMock()
    transaction.processing_block.list_keys.return_value = ["AA"]
    mock_config.return_value.txn.return_value = [transaction]
    config = SDPConfig()
    config.connect()

    assert config.processing_blocks() == ["AA"]

    assert mock_config.mock_calls == [call(), call().txn()]
    assert transaction.mock_calls == [call.processing_block.list_keys()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_processing_blocks_with_db_and_no_transactions(mock_config):
    """Can connect, but no transactions are available"""
    mock_config.return_value.txn.return_value = []
    config = SDPConfig()
    config.connect()

    assert config.processing_blocks() == []

    assert mock_config.mock_calls == [call(), call().txn()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_processing_blocks_with_no_db(mock_config):
    """Config DB cannot be connected"""
    mock_config.side_effect = requests.exceptions.ConnectionError
    config = SDPConfig()
    config.connect()

    assert config.processing_blocks() is None

    assert mock_config.mock_calls == [call()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_processing_block_with_db_and_transactions(mock_config):
    """Normal usage"""

    transaction = MagicMock()
    transaction.processing_block.get.return_value = FakeDumpModel(
        {
            "eb_id": "eb",
            "script": {"kind": "vis", "name": "script", "version": "v1"},
            "parameters": {"channels_per_port": 1024, "processors": {"p1": {}, "p2": {}}},
        }
    )
    mock_config.return_value.txn.return_value = [transaction]
    config = SDPConfig()
    config.connect()

    assert asdict(config.processing_block("QQ")) == {
        "eb_id": "eb",
        "script": {"kind": "vis", "name": "script", "version": "v1"},
        "channels_per_port": 1024,
        "processors": ["p1", "p2"],
        "signal_display_config": {},
    }

    assert mock_config.mock_calls == [call(), call().txn()]
    assert transaction.mock_calls == [call.processing_block.get("QQ")]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_processing_block_with_db_and_no_transactions(mock_config):
    """Can connect, but no transactions are available"""
    mock_config.return_value.txn.return_value = []
    config = SDPConfig()
    config.connect()

    data = config.processing_block("QQ")

    assert data is None

    assert mock_config.mock_calls == [call(), call().txn()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_processing_block_with_no_db(mock_config):
    """Config DB cannot be connected"""
    mock_config.side_effect = requests.exceptions.ConnectionError
    config = SDPConfig()
    config.connect()

    data = config.processing_block("QQ")

    assert data is None

    assert mock_config.mock_calls == [call()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_processing_block_state_with_db_and_transactions(mock_config):
    """Normal usage"""

    transaction = MagicMock()
    transaction.processing_block.state.return_value.get.return_value = {
        "deployments": "deployments",
        "deployments_ready": False,
        "last_updated": 123.123,
        "resources_available": "qwe",
        "status": "not_read",
    }
    mock_config.return_value.txn.return_value = [transaction]
    config = SDPConfig()
    config.connect()

    assert asdict(config.processing_block_state("QQ")) == {
        "deployments": "deployments",
        "deployments_ready": False,
        "last_updated": 123.123,
        "resources_available": "qwe",
        "status": "not_read",
    }

    assert mock_config.mock_calls == [call(), call().txn()]
    assert transaction.mock_calls == [
        call.processing_block.state("QQ"),
        call.processing_block.state().get(),
    ]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_processing_block_state_with_db_and_no_transactions(mock_config):
    """Can connect, but no transactions are available"""
    mock_config.return_value.txn.return_value = []
    config = SDPConfig()
    config.connect()

    data = config.processing_block_state("QQ")

    assert data is None

    assert mock_config.mock_calls == [call(), call().txn()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_processing_block_state_with_no_db(mock_config):
    """Config DB cannot be connected"""
    mock_config.side_effect = requests.exceptions.ConnectionError
    config = SDPConfig()
    config.connect()

    data = config.processing_block_state("QQ")

    assert data is None

    assert mock_config.mock_calls == [call()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_execution_blocks_with_db_and_transactions(mock_config):
    """Normal usage"""

    transaction = MagicMock()
    transaction.execution_block.list_keys.return_value = ["eb1", "rb2"]
    mock_config.return_value.txn.return_value = [transaction]
    config = SDPConfig()
    config.connect()

    assert config.execution_blocks() == ["eb1", "rb2"]

    assert mock_config.mock_calls == [call(), call().txn()]
    assert transaction.mock_calls == [call.execution_block.list_keys()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_execution_blocks_with_db_and_no_transactions(mock_config):
    """Can connect, but no transactions are available"""
    mock_config.return_value.txn.return_value = []
    config = SDPConfig()
    config.connect()

    assert config.execution_blocks() == []

    assert mock_config.mock_calls == [call(), call().txn()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_execution_blocks_with_no_db(mock_config):
    """Config DB cannot be connected"""
    mock_config.side_effect = requests.exceptions.ConnectionError
    config = SDPConfig()
    config.connect()

    assert config.execution_blocks() == []

    assert mock_config.mock_calls == [call()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_execution_block_with_db_and_transactions(mock_config):
    """Normal usage"""

    transaction = MagicMock()
    transaction.execution_block.get.return_value.model_dump.return_value = {"Block": "details"}
    mock_config.return_value.txn.return_value = [transaction]
    config = SDPConfig()
    config.connect()

    assert config.execution_block("eb1") == {"Block": "details"}

    assert mock_config.mock_calls == [call(), call().txn()]
    assert transaction.mock_calls == [
        call.execution_block.get("eb1"),
        call.execution_block.get().model_dump(),
    ]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_execution_block_with_db_and_no_transactions(mock_config):
    """Can connect, but no transactions are available"""
    mock_config.return_value.txn.return_value = []
    config = SDPConfig()
    config.connect()

    data = config.execution_block("eb1")

    assert data is None

    assert mock_config.mock_calls == [call(), call().txn()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_execution_block_with_no_db(mock_config):
    """Config DB cannot be connected"""
    mock_config.side_effect = requests.exceptions.ConnectionError
    config = SDPConfig()
    config.connect()

    data = config.execution_block("eb1")

    assert data is None

    assert mock_config.mock_calls == [call()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_deployments_with_db_and_transactions(mock_config):
    """Normal usage"""

    transaction = MagicMock()
    transaction.deployment.list_keys.return_value = ["D1", "D2"]
    mock_config.return_value.txn.return_value = [transaction]
    config = SDPConfig()
    config.connect()

    assert config.deployments() == ["D1", "D2"]

    assert mock_config.mock_calls == [call(), call().txn()]
    assert transaction.mock_calls == [call.deployment.list_keys()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_deployments_with_db_and_no_transactions(mock_config):
    """Can connect, but no transactions are available"""
    mock_config.return_value.txn.return_value = []
    config = SDPConfig()
    config.connect()

    assert config.deployments() == []

    assert mock_config.mock_calls == [call(), call().txn()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_deployments_with_no_db(mock_config):
    """Config DB cannot be connected"""
    mock_config.side_effect = requests.exceptions.ConnectionError
    config = SDPConfig()
    config.connect()

    assert config.deployments() == []

    assert mock_config.mock_calls == [call()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_deployment_with_db_and_transactions(mock_config):
    """Normal usage"""

    transaction = MagicMock()
    transaction.deployment.get.return_value = FakeDumpModel({"deployment": "details"})
    mock_config.return_value.txn.return_value = [transaction]
    config = SDPConfig()
    config.connect()

    assert config.deployment("dep1") == {"deployment": "details"}

    assert mock_config.mock_calls == [call(), call().txn()]
    assert transaction.mock_calls == [call.deployment.get("dep1")]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_deployment_with_db_and_no_transactions(mock_config):
    """Can connect, but no transactions are available"""
    mock_config.return_value.txn.return_value = []
    config = SDPConfig()
    config.connect()

    data = config.deployment("dep1")

    assert data is None

    assert mock_config.mock_calls == [call(), call().txn()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_deployment_with_no_db(mock_config):
    """Config DB cannot be connected"""
    mock_config.side_effect = requests.exceptions.ConnectionError
    config = SDPConfig()
    config.connect()

    data = config.deployment("dep1")

    assert data is None

    assert mock_config.mock_calls == [call()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_deployment_state_with_db_and_transactions(mock_config):
    """Normal usage"""

    transaction = MagicMock()
    transaction.deployment.state.return_value.get.return_value = {"deployment": "state"}
    mock_config.return_value.txn.return_value = [transaction]
    config = SDPConfig()
    config.connect()

    assert config.deployment_state("dep1") == {"deployment": "state"}

    assert mock_config.mock_calls == [call(), call().txn()]
    assert transaction.mock_calls == [call.deployment.state("dep1"), call.deployment.state().get()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_deployment_state_with_db_and_no_transactions(mock_config):
    """Can connect, but no transactions are available"""
    mock_config.return_value.txn.return_value = []
    config = SDPConfig()
    config.connect()

    data = config.deployment_state("dep1")

    assert data is None

    assert mock_config.mock_calls == [call(), call().txn()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_deployment_state_with_no_db(mock_config):
    """Config DB cannot be connected"""
    mock_config.side_effect = requests.exceptions.ConnectionError
    config = SDPConfig()
    config.connect()

    data = config.deployment_state("dep1")

    assert data is None

    assert mock_config.mock_calls == [call()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_subarray_state_no_db(mock_config):
    """Test when the subarray doesn't exist"""

    transaction = MagicMock()
    mock_config.return_value.txn.return_value = [transaction]
    config = SDPConfig()

    config._get_subarray = MagicMock(return_value=None)
    config._get_execution_block = MagicMock(return_value=None)
    config._get_processing_block = MagicMock(return_value=None)
    config._get_processing_block_state = MagicMock(return_value=None)
    config._get_deployment = MagicMock(return_value=None)

    assert config.subarray_running_state("01") is None

    assert mock_config.mock_calls == []
    assert config._get_subarray.mock_calls == []
    assert config._get_execution_block.mock_calls == []
    assert config._get_processing_block.mock_calls == []
    assert config._get_processing_block_state.mock_calls == []
    assert config._get_deployment.mock_calls == []


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_subarray_state_no_transaction(mock_config):
    """Test when the subarray doesn't exist"""

    mock_config.return_value.txn.return_value = []
    config = SDPConfig()

    config._get_subarray = MagicMock(return_value=None)
    config._get_execution_block = MagicMock(return_value=None)
    config._get_processing_block = MagicMock(return_value=None)
    config._get_processing_block_state = MagicMock(return_value=None)
    config._get_deployment = MagicMock(return_value=None)

    config.connect()

    assert config.subarray_running_state("01") == {}

    assert mock_config.mock_calls == [call(), call().txn()]
    assert config._get_subarray.mock_calls == []
    assert config._get_execution_block.mock_calls == []
    assert config._get_processing_block.mock_calls == []
    assert config._get_processing_block_state.mock_calls == []
    assert config._get_deployment.mock_calls == []


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_subarray_state_no_subarray(mock_config):
    """Test when the subarray doesn't exist"""

    transaction = MagicMock()
    mock_config.return_value.txn.return_value = [transaction]
    config = SDPConfig()

    config._get_subarray = MagicMock(return_value=None)
    config._get_execution_block = MagicMock(return_value=None)
    config._get_processing_block = MagicMock(return_value=None)
    config._get_processing_block_state = MagicMock(return_value=None)
    config._get_deployment = MagicMock(return_value=None)

    config.connect()

    with pytest.raises(ValueError) as exc_info:
        config.subarray_running_state("01")

    assert str(exc_info.value) == "Subarray doesn't exist"

    assert mock_config.mock_calls == [call(), call().txn()]
    assert config._get_subarray.mock_calls == [call(transaction, "01")]
    assert config._get_execution_block.mock_calls == []
    assert config._get_processing_block.mock_calls == []
    assert config._get_processing_block_state.mock_calls == []
    assert config._get_deployment.mock_calls == []


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_subarray_state_no_execution_block(mock_config):
    """Test when there is no execution block in subarray"""

    Subarray = namedtuple("Subarray", "eb_id")

    transaction = MagicMock()
    mock_config.return_value.txn.return_value = [transaction]
    config = SDPConfig()

    config._get_subarray = MagicMock(return_value=Subarray(eb_id=None))
    config._get_execution_block = MagicMock(return_value=None)
    config._get_processing_block = MagicMock(return_value=None)
    config._get_processing_block_state = MagicMock(return_value=None)
    config._get_deployment = MagicMock(return_value=None)

    config.connect()

    assert config.subarray_running_state("01") == {"subarray": Subarray(eb_id=None)}

    assert mock_config.mock_calls == [call(), call().txn()]
    assert config._get_subarray.mock_calls == [call(transaction, "01")]
    assert config._get_execution_block.mock_calls == []
    assert config._get_processing_block.mock_calls == []
    assert config._get_processing_block_state.mock_calls == []
    assert config._get_deployment.mock_calls == []


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_subarray_state_no_execution_block_2(mock_config):
    """Test when there is no execution block returned"""

    Subarray = namedtuple("Subarray", "eb_id")

    transaction = MagicMock()
    mock_config.return_value.txn.return_value = [transaction]
    config = SDPConfig()

    config._get_subarray = MagicMock(return_value=Subarray(eb_id="eb-1"))
    config._get_execution_block = MagicMock(return_value=None)
    config._get_processing_block = MagicMock(return_value=None)
    config._get_processing_block_state = MagicMock(return_value=None)
    config._get_deployment = MagicMock(return_value=None)

    config.connect()

    assert config.subarray_running_state("01") == {"subarray": Subarray(eb_id="eb-1")}

    assert mock_config.mock_calls == [call(), call().txn()]
    assert config._get_subarray.mock_calls == [call(transaction, "01")]
    assert config._get_execution_block.mock_calls == [call(transaction, "eb-1")]
    assert config._get_processing_block.mock_calls == []
    assert config._get_processing_block_state.mock_calls == []
    assert config._get_deployment.mock_calls == []


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_subarray_state_no_pb_id(mock_config):
    """Test when there is no PB ID"""

    Subarray = namedtuple("Subarray", "eb_id")

    transaction = MagicMock()
    mock_config.return_value.txn.return_value = [transaction]
    config = SDPConfig()

    config._get_subarray = MagicMock(return_value=Subarray(eb_id="eb-1"))
    config._get_execution_block = MagicMock(return_value={"name": "1"})
    config._get_execution_block_state = MagicMock(
        return_value={"scan_id": 1, "scan_type": "science", "scans": [], "status": "ACTIVE"}
    )
    config._get_processing_block = MagicMock(return_value=None)
    config._get_processing_block_state = MagicMock(return_value=None)
    config._get_deployment = MagicMock(return_value=None)

    config.connect()

    assert config.subarray_running_state("01") == {
        "subarray": Subarray(eb_id="eb-1"),
        "execution_block": {"name": "1"},
        "execution_block_state": {
            "scan_id": 1,
            "scan_type": "science",
            "scans": [],
            "status": "ACTIVE",
        },
    }

    assert mock_config.mock_calls == [call(), call().txn()]
    assert config._get_subarray.mock_calls == [call(transaction, "01")]
    assert config._get_execution_block.mock_calls == [call(transaction, "eb-1")]
    assert config._get_execution_block_state.mock_calls == [call(transaction, "eb-1")]
    assert config._get_processing_block.mock_calls == []
    assert config._get_processing_block_state.mock_calls == []
    assert config._get_deployment.mock_calls == []


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_subarray_state_blank_pb_id(mock_config):
    """Test when the PB ID is blank"""

    Subarray = namedtuple("Subarray", "eb_id")

    transaction = MagicMock()
    mock_config.return_value.txn.return_value = [transaction]
    config = SDPConfig()

    config._get_subarray = MagicMock(return_value=Subarray(eb_id="eb-1"))
    config._get_execution_block = MagicMock(return_value={"name": "1", "pb_realtime": []})
    config._get_execution_block_state = MagicMock(
        return_value={"scan_id": 1, "scan_type": "science", "scans": [], "status": "ACTIVE"}
    )
    config._get_processing_block = MagicMock(return_value=None)
    config._get_processing_block_state = MagicMock(return_value=None)
    config._get_deployment = MagicMock(return_value=None)

    config.connect()

    assert config.subarray_running_state("01") == {
        "subarray": Subarray(eb_id="eb-1"),
        "execution_block": {"name": "1", "pb_realtime": []},
        "execution_block_state": {
            "scan_id": 1,
            "scan_type": "science",
            "scans": [],
            "status": "ACTIVE",
        },
    }

    assert mock_config.mock_calls == [call(), call().txn()]
    assert config._get_subarray.mock_calls == [call(transaction, "01")]
    assert config._get_execution_block.mock_calls == [call(transaction, "eb-1")]
    assert config._get_execution_block_state.mock_calls == [call(transaction, "eb-1")]
    assert config._get_processing_block.mock_calls == []
    assert config._get_processing_block_state.mock_calls == []
    assert config._get_deployment.mock_calls == []


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_subarray_state_no_processing_block(mock_config):
    """Test where no processing block is returned"""

    Subarray = namedtuple("Subarray", "eb_id")

    transaction = MagicMock()
    mock_config.return_value.txn.return_value = [transaction]
    config = SDPConfig()

    config._get_subarray = MagicMock(return_value=Subarray(eb_id="eb-1"))
    config._get_execution_block = MagicMock(return_value={"name": "1", "pb_realtime": ["pb-1"]})
    config._get_execution_block_state = MagicMock(
        return_value={"scan_id": 1, "scan_type": "science", "scans": [], "status": "ACTIVE"}
    )
    config._get_processing_block = MagicMock(return_value=None)
    config._get_processing_block_state = MagicMock(return_value=None)
    config._get_deployment = MagicMock(return_value=None)

    config.connect()

    assert config.subarray_running_state("01") == {
        "subarray": Subarray(eb_id="eb-1"),
        "execution_block": {"name": "1", "pb_realtime": ["pb-1"]},
        "execution_block_state": {
            "scan_id": 1,
            "scan_type": "science",
            "scans": [],
            "status": "ACTIVE",
        },
    }

    assert mock_config.mock_calls == [call(), call().txn()]
    assert config._get_subarray.mock_calls == [call(transaction, "01")]
    assert config._get_execution_block.mock_calls == [call(transaction, "eb-1")]
    assert config._get_execution_block_state.mock_calls == [call(transaction, "eb-1")]
    assert config._get_processing_block.mock_calls == [call(transaction, "pb-1")]
    assert config._get_processing_block_state.mock_calls == [call(transaction, "pb-1")]
    assert config._get_deployment.mock_calls == []


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_subarray_state_no_processing_block_state(mock_config):
    """Test where not processing block state is returned"""

    Subarray = namedtuple("Subarray", "eb_id")

    transaction = MagicMock()
    mock_config.return_value.txn.return_value = [transaction]
    config = SDPConfig()

    config._get_subarray = MagicMock(return_value=Subarray(eb_id="eb-1"))
    config._get_execution_block = MagicMock(return_value={"name": "1", "pb_realtime": ["pb-1"]})
    config._get_execution_block_state = MagicMock(
        return_value={"scan_id": 1, "scan_type": "science", "scans": [], "status": "ACTIVE"}
    )
    config._get_processing_block = MagicMock(return_value={"name": "pb"})
    config._get_processing_block_state = MagicMock(return_value=None)
    config._get_deployment = MagicMock(return_value=None)

    config.connect()

    assert config.subarray_running_state("01") == {
        "subarray": Subarray(eb_id="eb-1"),
        "execution_block": {"name": "1", "pb_realtime": ["pb-1"]},
        "execution_block_state": {
            "scan_id": 1,
            "scan_type": "science",
            "scans": [],
            "status": "ACTIVE",
        },
        "processing_block": {"name": "pb"},
        "processing_block_state": None,
    }

    assert mock_config.mock_calls == [call(), call().txn()]
    assert config._get_subarray.mock_calls == [call(transaction, "01")]
    assert config._get_execution_block.mock_calls == [call(transaction, "eb-1")]
    assert config._get_execution_block_state.mock_calls == [call(transaction, "eb-1")]
    assert config._get_processing_block.mock_calls == [call(transaction, "pb-1")]
    assert config._get_processing_block_state.mock_calls == [call(transaction, "pb-1")]
    assert config._get_deployment.mock_calls == []


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_subarray_state_no_processing_block_state_deployments(mock_config):
    """Test where no deployments are in the state"""

    Subarray = namedtuple("Subarray", "eb_id")
    ProcessingBlockState = namedtuple("ProcessingBlockState", "deployments")

    transaction = MagicMock()
    mock_config.return_value.txn.return_value = [transaction]
    config = SDPConfig()

    config._get_subarray = MagicMock(return_value=Subarray(eb_id="eb-1"))
    config._get_execution_block = MagicMock(return_value={"name": "1", "pb_realtime": ["pb-1"]})
    config._get_execution_block_state = MagicMock(
        return_value={"scan_id": 1, "scan_type": "science", "scans": [], "status": "ACTIVE"}
    )
    config._get_processing_block = MagicMock(return_value={"name": "pb"})
    config._get_processing_block_state = MagicMock(
        return_value=ProcessingBlockState(deployments=None)
    )
    config._get_deployment = MagicMock(return_value=None)

    config.connect()

    assert config.subarray_running_state("01") == {
        "subarray": Subarray(eb_id="eb-1"),
        "execution_block": {"name": "1", "pb_realtime": ["pb-1"]},
        "execution_block_state": {
            "scan_id": 1,
            "scan_type": "science",
            "scans": [],
            "status": "ACTIVE",
        },
        "processing_block": {"name": "pb"},
        "processing_block_state": ProcessingBlockState(deployments=None),
    }

    assert mock_config.mock_calls == [call(), call().txn()]
    assert config._get_subarray.mock_calls == [call(transaction, "01")]
    assert config._get_execution_block.mock_calls == [call(transaction, "eb-1")]
    assert config._get_execution_block_state.mock_calls == [call(transaction, "eb-1")]
    assert config._get_processing_block.mock_calls == [call(transaction, "pb-1")]
    assert config._get_processing_block_state.mock_calls == [call(transaction, "pb-1")]
    assert config._get_deployment.mock_calls == []


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_subarray_state_blank_full(mock_config):
    """Full test"""

    Subarray = namedtuple("Subarray", "eb_id")
    ProcessingBlockState = namedtuple("ProcessingBlockState", "deployments")

    transaction = MagicMock()
    mock_config.return_value.txn.return_value = [transaction]
    config = SDPConfig()

    config._get_subarray = MagicMock(return_value=Subarray(eb_id="eb-1"))
    config._get_execution_block = MagicMock(return_value={"name": "1", "pb_realtime": ["pb-1"]})
    config._get_execution_block_state = MagicMock(
        return_value={"scan_id": 1, "scan_type": "science", "scans": [], "status": "ACTIVE"}
    )
    config._get_processing_block = MagicMock(return_value={"name": "pb"})
    config._get_processing_block_state = MagicMock(
        return_value=ProcessingBlockState(deployments={"dep-1": "running"})
    )
    config._get_deployment = MagicMock(return_value={"deployment": "name"})

    config.connect()

    assert config.subarray_running_state("01") == {
        "subarray": Subarray(eb_id="eb-1"),
        "execution_block": {"name": "1", "pb_realtime": ["pb-1"]},
        "execution_block_state": {
            "scan_id": 1,
            "scan_type": "science",
            "scans": [],
            "status": "ACTIVE",
        },
        "processing_block": {"name": "pb"},
        "processing_block_state": ProcessingBlockState(deployments={"dep-1": "running"}),
        "deployments": {"dep-1": {"state": "running", "deployment": {"deployment": "name"}}},
    }

    assert mock_config.mock_calls == [call(), call().txn()]
    assert config._get_subarray.mock_calls == [call(transaction, "01")]
    assert config._get_execution_block.mock_calls == [call(transaction, "eb-1")]
    assert config._get_execution_block_state.mock_calls == [call(transaction, "eb-1")]
    assert config._get_processing_block.mock_calls == [call(transaction, "pb-1")]
    assert config._get_processing_block_state.mock_calls == [call(transaction, "pb-1")]
    assert config._get_deployment.mock_calls == [call(transaction, "dep-1")]


@pytest.mark.parametrize(
    "type_name,type_id",
    [
        (ConfigDBTypes.LIST_SUBARRAYS, None),
        (ConfigDBTypes.SUBARRAY_STATE, "id-01"),
        (ConfigDBTypes.SUBARRAY, "id-01"),
        (ConfigDBTypes.LIST_PROCESSING_BLOCKS, None),
        (ConfigDBTypes.PROCESSING_BLOCK, "id-01"),
        (ConfigDBTypes.PROCESSING_BLOCK_STATE, "id-01"),
        (ConfigDBTypes.LIST_EXECUTION_BLOCKS, None),
        (ConfigDBTypes.EXECUTION_BLOCK, "id-01"),
        (ConfigDBTypes.LIST_DEPLOYMENTS, None),
        (ConfigDBTypes.DEPLOYMENT, "id-01"),
        (ConfigDBTypes.DEPLOYMENT_STATE, "id-01"),
    ],
)
@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_from_type_dict(mock_config, type_name, type_id):
    """Test list types from data fetchers"""

    transaction = MagicMock()
    mock_config.return_value.txn.return_value = [transaction]
    config = SDPConfig()
    config.connect()
    lists = [
        ConfigDBTypes.LIST_SUBARRAYS,
        ConfigDBTypes.LIST_PROCESSING_BLOCKS,
        ConfigDBTypes.LIST_EXECUTION_BLOCKS,
        ConfigDBTypes.LIST_DEPLOYMENTS,
    ]

    config._list_subarrays = MagicMock(return_value=ConfigDBTypes.LIST_SUBARRAYS)
    config._get_subarray_state = MagicMock(return_value=ConfigDBTypes.SUBARRAY_STATE)
    config._get_subarray = MagicMock(return_value=ConfigDBTypes.SUBARRAY)
    config._list_processing_blocks = MagicMock(return_value=ConfigDBTypes.LIST_PROCESSING_BLOCKS)
    config._get_processing_block = MagicMock(return_value=ConfigDBTypes.PROCESSING_BLOCK)
    config._get_processing_block_state = MagicMock(
        return_value=ConfigDBTypes.PROCESSING_BLOCK_STATE
    )
    config._list_execution_blocks = MagicMock(return_value=ConfigDBTypes.LIST_EXECUTION_BLOCKS)
    config._get_execution_block = MagicMock(return_value=ConfigDBTypes.EXECUTION_BLOCK)
    config._list_deployments = MagicMock(return_value=ConfigDBTypes.LIST_DEPLOYMENTS)
    config._get_deployment = MagicMock(return_value=ConfigDBTypes.DEPLOYMENT)
    config._get_deployment_state = MagicMock(return_value=ConfigDBTypes.DEPLOYMENT_STATE)

    types_to_functions = {
        ConfigDBTypes.LIST_SUBARRAYS: config._list_subarrays,
        ConfigDBTypes.SUBARRAY_STATE: config._get_subarray_state,
        ConfigDBTypes.SUBARRAY: config._get_subarray,
        ConfigDBTypes.LIST_PROCESSING_BLOCKS: config._list_processing_blocks,
        ConfigDBTypes.PROCESSING_BLOCK: config._get_processing_block,
        ConfigDBTypes.PROCESSING_BLOCK_STATE: config._get_processing_block_state,
        ConfigDBTypes.LIST_EXECUTION_BLOCKS: config._list_execution_blocks,
        ConfigDBTypes.EXECUTION_BLOCK: config._get_execution_block,
        ConfigDBTypes.LIST_DEPLOYMENTS: config._list_deployments,
        ConfigDBTypes.DEPLOYMENT: config._get_deployment,
        ConfigDBTypes.DEPLOYMENT_STATE: config._get_deployment_state,
    }

    assert config.get_from_type_dict(type_name, type_id) == type_name

    for name, function in types_to_functions.items():
        if name == type_name:
            if name in lists:
                assert function.mock_calls == [
                    call(
                        transaction,
                    )
                ]
            else:
                assert function.mock_calls == [call(transaction, type_id)]
        else:
            assert function.mock_calls == []


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_from_type_dict_no_config(mock_config):
    """Test list types from data fetchers"""

    config = SDPConfig()

    assert config.get_from_type_dict(ConfigDBTypes.LIST_SUBARRAYS, None) == []


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_from_type_dict_invalid_type(mock_config):
    """Test list types from data fetchers"""

    config = SDPConfig()

    with pytest.raises(ValueError) as exc_info:
        config.get_from_type_dict("thing", None)

    assert str(exc_info.value) == "Invalid type 'thing'"


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_flows_with_db_and_transactions(mock_config):
    """Normal usage for flow list"""

    transaction = MagicMock()
    transaction.flow.list_keys.return_value = []
    mock_config.return_value.txn.return_value = [transaction]
    config = SDPConfig()
    config.connect()

    assert config.flows() == []

    assert mock_config.mock_calls == [call(), call().txn()]
    assert transaction.mock_calls == [call.flow.list_keys(kind="data-queue")]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_flows_with_db_and_no_transactions(mock_config):
    """No data to show"""

    config = SDPConfig()
    config.connect()

    assert config.flows() is None

    assert mock_config.mock_calls == [call(), call().txn(), call().txn().__iter__()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_flows_with_no_db(mock_config):
    """Config DB cannot be connected"""
    mock_config.side_effect = requests.exceptions.ConnectionError
    config = SDPConfig()
    config.connect()

    data = config.flows()

    assert data is None

    assert mock_config.mock_calls == [call()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_flow_with_db_and_transactions(mock_config):
    """Normal usage for getting a flow"""

    flow = FakeDumpModel({"flow": "details"})
    transaction = MagicMock()
    transaction.flow.get.return_value = flow
    mock_config.return_value.txn.return_value = [transaction]
    config = SDPConfig()
    config.connect()

    assert config.flow("flow-id") == flow

    assert mock_config.mock_calls == [call(), call().txn()]
    assert transaction.mock_calls == [call.flow.get("flow-id")]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_flow_with_db_and_no_transactions(mock_config):
    """Normal usage, no data"""

    config = SDPConfig()
    config.connect()

    assert config.flow("flow-id") is None

    assert mock_config.mock_calls == [call(), call().txn(), call().txn().__iter__()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_flow_with_no_db(mock_config):
    """Config DB cannot be connected, no data"""
    mock_config.side_effect = requests.exceptions.ConnectionError
    config = SDPConfig()
    config.connect()

    data = config.flow("flow-id")

    assert data is None

    assert mock_config.mock_calls == [call()]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_windows_basic(mock_config):
    """Test get windows with no filtering"""

    flow = FakeDumpModel({"kind": "data-queue", "name": "metrics-spectrum-01", "pb_id": "pb-id"})
    flow_value = FakeDumpModel(
        {
            "sink": FakeDumpModel({"topics": "metrics-spectrum-01"}),
            "sources": [
                FakeDumpModel(
                    {
                        "function": "SignalDisplayMetrics",
                        "parameters": {"additional_windows": 5, "nchan_avg": 25},
                    }
                )
            ],
        }
    )
    transaction = MagicMock()
    transaction.flow.list_values.return_value = [(flow, flow_value)]
    transaction.flow.state.return_value.get.return_value = None

    mock_config.return_value.txn.return_value = [transaction]

    config = SDPConfig()
    config.connect()

    assert config.windows(None, None, None) == [
        Window(
            topic="metrics-spectrum-01",
            processing_block="pb-id",
            max_window_count=5,
            channels_averaged=25,
            windows=[],
        )
    ]

    assert mock_config.mock_calls == [call(), call().txn()]
    assert transaction.mock_calls == [
        call.flow.list_values(kind="data-queue"),
        call.flow.state(flow),
        call.flow.state().get(),
    ]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_windows_basic_with_subwindows(mock_config):
    """Test get windows with a subwindow"""

    flow = FakeDumpModel({"kind": "data-queue", "name": "metrics-spectrum-01", "pb_id": "pb-id"})
    flow_value = FakeDumpModel(
        {
            "sink": FakeDumpModel({"topics": "metrics-spectrum-01"}),
            "sources": [
                FakeDumpModel(
                    {
                        "function": "SignalDisplayMetrics",
                        "parameters": {"additional_windows": 5, "nchan_avg": 25},
                    }
                )
            ],
        }
    )
    transaction = MagicMock()
    transaction.flow.list_values.return_value = [(flow, flow_value)]
    transaction.flow.state.return_value.get.return_value = {
        "windows": [
            {
                "start": 100,
                "end": 200,
                "channels_averaged": 1,
                "user": "faked",
                "requested_at": "2024-01-01...",
            }
        ]
    }

    mock_config.return_value.txn.return_value = [transaction]

    config = SDPConfig()
    config.connect()

    assert config.windows(None, None, None) == [
        Window(
            topic="metrics-spectrum-01",
            processing_block="pb-id",
            max_window_count=5,
            channels_averaged=25,
            windows=[
                SubWindow(
                    start=100,
                    end=200,
                    channels_averaged=1,
                    user="faked",
                    requested_at="2024-01-01...",
                )
            ],
        )
    ]

    assert mock_config.mock_calls == [call(), call().txn()]
    assert transaction.mock_calls == [
        call.flow.list_values(kind="data-queue"),
        call.flow.state(flow),
        call.flow.state().get(),
    ]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_get_windows_filtered_results(mock_config):
    """Test get filtered windows"""

    transaction = MagicMock()
    transaction.flow.list_values.return_value = [
        (
            FakeDumpModel({"kind": "data-queue", "name": "metrics-spectrum-01", "pb_id": "other"}),
            {},
        ),
        (
            FakeDumpModel({"kind": "data-queue", "name": "metrics-spectrum-03", "pb_id": "pb-id"}),
            {},
        ),
        (
            FakeDumpModel({"kind": "data-queue", "name": "metrics-spectrum-01", "pb_id": "pb-id"}),
            {},
        ),
    ]

    mock_config.return_value.txn.return_value = [transaction]

    config = SDPConfig()
    config.connect()

    assert config.windows("01", "pb-id", "amplitude") == []

    assert mock_config.mock_calls == [call(), call().txn()]
    assert transaction.mock_calls == [
        call.flow.list_values(kind="data-queue"),
    ]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
@patch("ska_sdp_qa_data_api.server.utils.config_db.now_str", autospec=True)
def test_create_window_first(mock_now, mock_config):
    """Create a new window, being the first window."""
    mock_now.return_value = "todays-date"
    create_window = CreateWindow(
        processing_block="pb-id",
        subarray="01",
        metric=MetricDataTypes.SPECTRUM,
        spectrum_start=100,
        spectrum_end=10000,
        channels_averaged=1,
    )
    flow = FakeDumpModel({"kind": "data-queue", "name": "metrics-spectrum-01", "pb_id": "pb-id"})
    flow_value = FakeDumpModel(
        {
            "sink": FakeDumpModel({"topics": "metrics-spectrum-01"}),
            "sources": [FakeDumpModel({"parameters": {"additional_windows": 5, "nchan_avg": 25}})],
        }
    )

    transaction = MagicMock()
    transaction.flow.list_keys.return_value = [flow]
    mock_config.return_value.txn.return_value = [transaction]
    transaction.flow.get.return_value = flow_value
    transaction.flow.state.return_value.get.return_value = None

    config = SDPConfig()
    config.connect()

    assert config.window_create(create_window, "test@email.com") == {
        "success": True,
        "message": "New window added",
        "window_id": 1,
    }

    assert mock_config.mock_calls == [call(), call().txn()]
    assert transaction.mock_calls == [
        call.flow.list_keys(kind="data-queue"),
        call.flow.state(flow),  # get state
        call.flow.state(flow).get(),
        call.flow.state(flow),  # set state
        call.flow.state().create(
            {
                "windows": [
                    {
                        "start": create_window.spectrum_start,
                        "end": create_window.spectrum_end,
                        "channels_averaged": create_window.channels_averaged,
                        "user": "test@email.com",
                        "requested_at": "todays-date",
                    }
                ]
            }
        ),
    ]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
@patch("ska_sdp_qa_data_api.server.utils.config_db.now_str", autospec=True)
def test_create_window_append(mock_now, mock_config):
    """Create a window, with an existing window"""
    mock_now.return_value = "todays-date"
    create_window = CreateWindow(
        processing_block="pb-id",
        subarray="01",
        metric=MetricDataTypes.SPECTRUM,
        spectrum_start=100,
        spectrum_end=10000,
        channels_averaged=1,
    )
    flow = FakeDumpModel({"kind": "data-queue", "name": "metrics-spectrum-01", "pb_id": "pb-id"})

    transaction = MagicMock()
    transaction.flow.list_keys.return_value = [flow]
    mock_config.return_value.txn.return_value = [transaction]
    transaction.flow.get.return_value = FakeDumpModel(
        {
            "sink": FakeDumpModel({"topics": "metrics-spectrum-01"}),
            "sources": [FakeDumpModel({"parameters": {"additional_windows": 5, "nchan_avg": 25}})],
        }
    )
    transaction.flow.state.return_value.get.return_value = {
        "windows": [
            {
                "start": 0,
                "end": 100,
                "channels_averaged": 0,
                "user": "person1",
                "requested_at": "a different time",
            }
        ]
    }

    config = SDPConfig()
    config.connect()

    assert config.window_create(create_window, "test@email.com") == {
        "success": True,
        "message": "New window added",
        "window_id": 2,
    }

    assert mock_config.mock_calls == [call(), call().txn()]
    assert transaction.mock_calls == [
        call.flow.list_keys(kind="data-queue"),
        call.flow.state(flow),  # get state
        call.flow.state(flow).get(),
        call.flow.get(flow),  # set state
        call.flow.state(flow),
        call.flow.state().update(
            {
                "windows": [
                    {
                        "start": 0,
                        "end": 100,
                        "channels_averaged": 0,
                        "user": "person1",
                        "requested_at": "a different time",
                    },
                    {
                        "start": 100,
                        "end": 10000,
                        "channels_averaged": 1,
                        "user": "test@email.com",
                        "requested_at": "todays-date",
                    },
                ]
            }
        ),
    ]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_create_window_reuse_with_maximum(mock_config):
    """Create a new window, using an existing window, while being at the maximum allowed windows"""
    create_window = CreateWindow(
        processing_block="pb-id",
        subarray="01",
        metric=MetricDataTypes.SPECTRUM,
        spectrum_start=100,
        spectrum_end=10000,
        channels_averaged=1,
    )
    flow = FakeDumpModel({"kind": "data-queue", "name": "metrics-spectrum-01", "pb_id": "pb-id"})

    transaction = MagicMock()
    transaction.flow.list_keys.return_value = [flow]
    mock_config.return_value.txn.return_value = [transaction]
    transaction.flow.get.return_value = FakeDumpModel(
        {
            "sink": FakeDumpModel({"topics": "metrics-spectrum-01"}),
            "sources": [FakeDumpModel({"parameters": {"additional_windows": 1, "nchan_avg": 25}})],
        }
    )
    transaction.flow.state.return_value.get.return_value = {
        "windows": [
            {
                "start": create_window.spectrum_start,
                "end": create_window.spectrum_end,
                "channels_averaged": create_window.channels_averaged,
            }
        ]
    }

    config = SDPConfig()
    config.connect()

    assert config.window_create(create_window, "test@email.com") == {
        "success": True,
        "message": "Using existing window",
        "window_id": 1,
    }

    assert mock_config.mock_calls == [call(), call().txn()]
    assert transaction.mock_calls == [
        call.flow.list_keys(kind="data-queue"),
        call.flow.state(flow),  # get state
        call.flow.state(flow).get(),
        call.flow.get(flow),  # set state
    ]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_create_window_reuse(mock_config):
    """Reuse an existing window."""
    create_window = CreateWindow(
        processing_block="pb-id",
        subarray="01",
        metric=MetricDataTypes.SPECTRUM,
        spectrum_start=100,
        spectrum_end=10000,
        channels_averaged=1,
    )
    flow = FakeDumpModel({"kind": "data-queue", "name": "metrics-spectrum-01", "pb_id": "pb-id"})

    transaction = MagicMock()
    transaction.flow.list_keys.return_value = [flow]
    mock_config.return_value.txn.return_value = [transaction]
    transaction.flow.get.return_value = FakeDumpModel(
        {
            "sink": FakeDumpModel({"topics": "metrics-spectrum-01"}),
            "sources": [FakeDumpModel({"parameters": {"additional_windows": 5, "nchan_avg": 25}})],
        }
    )
    transaction.flow.state.return_value.get.return_value = {
        "windows": [
            {
                "start": create_window.spectrum_start,
                "end": create_window.spectrum_end,
                "channels_averaged": create_window.channels_averaged,
            }
        ]
    }

    config = SDPConfig()
    config.connect()

    assert config.window_create(create_window, "test@email.com") == {
        "success": True,
        "message": "Using existing window",
        "window_id": 1,
    }

    assert mock_config.mock_calls == [call(), call().txn()]
    assert transaction.mock_calls == [
        call.flow.list_keys(kind="data-queue"),
        call.flow.state(flow),  # get state
        call.flow.state(flow).get(),
        call.flow.get(flow),  # set state
    ]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_create_window_maximum_reached(mock_config):
    """Create a window, while already being at the maximum."""
    create_window = CreateWindow(
        processing_block="pb-id",
        subarray="01",
        metric=MetricDataTypes.SPECTRUM,
        spectrum_start=100,
        spectrum_end=10000,
        channels_averaged=1,
    )
    flow = FakeDumpModel({"kind": "data-queue", "name": "metrics-spectrum-01", "pb_id": "pb-id"})

    transaction = MagicMock()
    transaction.flow.list_keys.return_value = [flow]
    mock_config.return_value.txn.return_value = [transaction]
    transaction.flow.get.return_value = FakeDumpModel(
        {
            "sink": FakeDumpModel({"topics": "metrics-spectrum-01"}),
            "sources": [FakeDumpModel({"parameters": {"additional_windows": 1, "nchan_avg": 25}})],
        }
    )
    transaction.flow.state.return_value.get.return_value = {
        "windows": [
            {
                "start": 1,
                "end": 2,
                "channels_averaged": 1,
            }
        ]
    }

    config = SDPConfig()
    config.connect()

    assert config.window_create(create_window, "test@email.com") == {
        "success": False,
        "message": "There are too many windows already open",
    }

    assert mock_config.mock_calls == [call(), call().txn()]
    assert transaction.mock_calls == [
        call.flow.list_keys(kind="data-queue"),
        call.flow.state(flow),  # get state
        call.flow.state(flow).get(),
        call.flow.get(flow),  # set state
    ]


@patch("ska_sdp_qa_data_api.server.utils.config_db.ska_sdp_config.Config", autospec=True)
def test_create_window_no_window_found(mock_config):
    """Try creating a window, but no flow is found."""
    create_window = CreateWindow(
        processing_block="pb-id",
        subarray="01",
        metric=MetricDataTypes.SPECTRUM,
        spectrum_start=100,
        spectrum_end=10000,
        channels_averaged=1,
    )

    transaction = MagicMock()
    transaction.flow.list_keys.return_value = [
        FakeDumpModel({"kind": "data-queue", "name": "metrics-spectrum-01", "pb_id": "pb-wrong"}),
        FakeDumpModel({"kind": "data-queue", "name": "metrics-spectrum-wrong", "pb_id": "pb-id"}),
        FakeDumpModel({"kind": "data-queue", "name": "metrics-wrong-01", "pb_id": "pb-id"}),
    ]
    mock_config.return_value.txn.return_value = [transaction]

    config = SDPConfig()
    config.connect()

    assert config.window_create(create_window, "test@email.com") == {
        "success": False,
        "message": "No window found",
    }

    assert mock_config.mock_calls == [call(), call().txn()]
    assert transaction.mock_calls == [
        call.flow.list_keys(kind="data-queue"),
    ]
