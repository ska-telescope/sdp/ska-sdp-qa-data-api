"""Test the helper functions"""

import json
from dataclasses import dataclass
from time import sleep

import numpy
import pytest

from ska_sdp_qa_data_api.server.utils.helpers import (
    Timer,
    _s,
    json_default,
    recursive_to_dict,
    size_to_str,
    topic_names,
)


def test_topic_names_subarray_none():
    """Test default list"""
    assert topic_names() == {
        "phase_and_amplitude": "metrics-phase_and_amplitude-.*",
        "stats": "metrics-stats-.*",
        "rfi": "metrics-rfi-.*",
        "spectrum": "metrics-spectrum-.*",
        "visibility": "metrics-visibility-.*",
        "phase": "metrics-phase-.*",
        "lagplot": "metrics-lagplot-.*",
        "amplitude": "metrics-amplitude-.*",
        "pointing_offset_out": "metrics-pointing_offset_out-.*",
        "receiverstats": "metrics-receiverstats-.*",
        "uvcoverage": "metrics-uvcoverage-.*",
        "bandpass-gains": "bandpass-gains-.*",
        "gain_calibration_out": "metrics-gain_calibration_out-.*",
        "bandaveragedxcorr": "metrics-bandaveragedxcorr-.*",
    }


def test_topic_names_subarray_specific():
    """Test with given subarray"""
    assert topic_names("11") == {
        "phase_and_amplitude": "metrics-phase_and_amplitude-11",
        "stats": "metrics-stats-11",
        "rfi": "metrics-rfi-11",
        "spectrum": "metrics-spectrum-11",
        "visibility": "metrics-visibility-11",
        "phase": "metrics-phase-11",
        "lagplot": "metrics-lagplot-11",
        "amplitude": "metrics-amplitude-11",
        "pointing_offset_out": "metrics-pointing_offset_out-11",
        "receiverstats": "metrics-receiverstats-11",
        "uvcoverage": "metrics-uvcoverage-11",
        "bandpass-gains": "bandpass-gains-11",
        "gain_calibration_out": "metrics-gain_calibration_out-11",
        "bandaveragedxcorr": "metrics-bandaveragedxcorr-11",
    }


def test_topic_names_subarray_list():
    """Test with a list of subarrays"""
    assert topic_names(["12", "13"]) == [
        {
            "phase_and_amplitude": "metrics-phase_and_amplitude-12",
            "stats": "metrics-stats-12",
            "rfi": "metrics-rfi-12",
            "spectrum": "metrics-spectrum-12",
            "visibility": "metrics-visibility-12",
            "phase": "metrics-phase-12",
            "lagplot": "metrics-lagplot-12",
            "amplitude": "metrics-amplitude-12",
            "pointing_offset_out": "metrics-pointing_offset_out-12",
            "receiverstats": "metrics-receiverstats-12",
            "uvcoverage": "metrics-uvcoverage-12",
            "bandpass-gains": "bandpass-gains-12",
            "gain_calibration_out": "metrics-gain_calibration_out-12",
            "bandaveragedxcorr": "metrics-bandaveragedxcorr-12",
        },
        {
            "phase_and_amplitude": "metrics-phase_and_amplitude-13",
            "stats": "metrics-stats-13",
            "rfi": "metrics-rfi-13",
            "spectrum": "metrics-spectrum-13",
            "visibility": "metrics-visibility-13",
            "phase": "metrics-phase-13",
            "lagplot": "metrics-lagplot-13",
            "amplitude": "metrics-amplitude-13",
            "pointing_offset_out": "metrics-pointing_offset_out-13",
            "receiverstats": "metrics-receiverstats-13",
            "uvcoverage": "metrics-uvcoverage-13",
            "bandpass-gains": "bandpass-gains-13",
            "gain_calibration_out": "metrics-gain_calibration_out-13",
            "bandaveragedxcorr": "metrics-bandaveragedxcorr-13",
        },
    ]


@pytest.mark.parametrize(
    "test_input,expected",
    [
        (200, "200.0 B"),
        (1_500, "1.5 KB"),
        (999_500, "976.1 KB"),
        (1_500_000, "1.4 MB"),
        (1_500_000_000, "1.4 GB"),
    ],
)
def test_size_to_str_bytes(test_input, expected):
    """Check conversion from bytes to string"""
    assert size_to_str(test_input) == expected


@dataclass
class FakeClass:
    """Fake class for tests"""

    thing: str


def test_recursive_to_dict():
    """Test Recursive converter to dict"""
    assert recursive_to_dict(
        [
            {"key": set(["thing"]), "obj": FakeClass("value")},
        ]
    ) == [{"key": ["thing"], "obj": {"thing": "value"}}]


def string_conversion():
    """Test that string conversion works on strings"""
    assert _s("asd") == "asd"


def string_conversion_bytes():
    """Test that string conversion works on bytes"""
    assert _s(bytes("asd")) == "asd"


def test_timer():
    """Test time class"""
    with Timer("My Name") as timer:
        sleep(0.1)
    assert timer.name == "My Name"


def test_json_conversion_complex():
    """Test complex numbers conversion"""
    assert json.dumps([complex(1, 2)], default=json_default) == "[[1.0, 2.0]]"


def test_json_conversion_float64():
    """Test float64 numbers conversion"""
    assert json.dumps([numpy.float64(5.9975)], default=json_default) == "[5.9975]"


def test_json_conversion_ndarray():
    """Test array conversion"""
    assert json.dumps(numpy.zeros(3), default=json_default) == "[0.0, 0.0, 0.0]"


def test_json_conversion_set():
    """Test set conversion"""
    assert json.dumps(set([1, 2, 3]), default=json_default) == "[1, 2, 3]"


def test_json_conversion_bytes():
    """Test bytes conversion"""
    assert json.dumps([bytes("123", encoding="utf-8")], default=json_default) == '["123"]'
