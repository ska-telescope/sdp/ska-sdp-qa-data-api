"""Test the utility functions getting used for waterfall plots."""

import numpy as np
import pytest

from ska_sdp_qa_data_api.server.core.config import WATERFALL_THUMBNAIL_MAX_WIDTH
from ska_sdp_qa_data_api.server.utils.waterfall_plots import shrink_numpy_row


def test_shrink_numpy_row_exact_size():
    """Test exactly the same size"""

    input_row = np.random.rand(WATERFALL_THUMBNAIL_MAX_WIDTH)
    output_row = shrink_numpy_row(input_row)

    assert np.shape(input_row) == np.shape(output_row)


def test_shrink_numpy_row_shrink_by_2():
    """Test exactly the same size"""

    input_row = np.random.rand(WATERFALL_THUMBNAIL_MAX_WIDTH * 2)
    output_row = shrink_numpy_row(input_row)

    assert np.shape(output_row) == (WATERFALL_THUMBNAIL_MAX_WIDTH,)


@pytest.mark.parametrize(
    "sizes",
    [
        (14148, (513,)),  # example from dataset
        (130, (130,)),  # too small
        (513, (513,)),  # 1 more than the smallest width
    ],
)
def test_shrink_numpy_row_check_sizes(sizes):
    """Test exactly the same size"""

    input_row = np.random.rand(sizes[0])
    output_sizing = shrink_numpy_row(input_row)

    assert np.shape(output_sizing) == sizes[1]
