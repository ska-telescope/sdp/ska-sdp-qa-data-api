#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tests for the ska_sdp_qa_data_api utils bitmap."""

import os

from ska_sdp_qa_data_api.server.utils.helpers import slugify

TEST_DATA_CSV = "tests/test_data/test_data.csv"
REFERENCE_BMP = "tests/test_data/test_file.bmp"
OUTPUT_REFERENCE_BMP = "tests/test_data/test_file_output.bmp"

TEST_STRING = "/hello/.exe/how?/"
REFERENCE_STRING = "hello-exe-how"

REFERENCE_DICT = "tests/test_data/output_dict.txt"


def remove_if_exists(filename):
    """Shorthand to remove a file if it exists"""
    try:
        os.remove(filename)
    except OSError:
        pass


def test_slugify_main():
    """Test to see if slugify works correctly."""
    output = slugify(TEST_STRING)
    assert output == REFERENCE_STRING
