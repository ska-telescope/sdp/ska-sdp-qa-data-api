"""Basic test for the waterfall plot endpoints."""

# pylint: disable=too-many-arguments,too-many-positional-arguments

from unittest.mock import MagicMock, call

import numpy as np
import pytest
from fastapi import Request

from ska_sdp_qa_data_api.server.api.waterfall import _get_type, waterfall_plots


def test_get_type_valid_url():
    """Mock request with a URL."""
    request = Request(
        scope={
            "type": "http",
            "method": "GET",
            "path": "/test/v1/123",
            "headers": {},
            "query_string": b"",
            "server": ("testserver", 80),
            "client": ("testclient", 80),
            "scheme": "http",
            "root_path": "",
        }
    )

    assert _get_type(request) == "test"


@pytest.mark.parametrize(
    "img_type, processing_block, polarisation, start, end, expected_status_code, "
    "expected_media_type",
    [
        ("lag_plots", "latest", "XX", None, None, 200, "application/json"),
        ("spectrum_waterfalls", "latest", "XX", "123456788", "123456789", 200, "application/json"),
    ],
)
def test_send_spectrum_delta_thumbnail_array(
    img_type,
    processing_block,
    polarisation,
    start,
    end,
    expected_status_code,
    expected_media_type,
    test_app,
):
    """Test spectrum waterfall endpoint."""

    waterfall_plots.retrieve_np_from_timeseries = MagicMock(
        return_value=np.array([[1, 2, 3], [4, 5, 6]])
    )
    url = f"/{img_type}/delta_thumbnail_pb/{processing_block}/{polarisation}"
    params = {}
    if start:
        params["start"] = start
    if end:
        params["end"] = end

    response = test_app.get(url, params=params)

    assert response.status_code == expected_status_code
    assert response.headers["content-type"] == expected_media_type
    if img_type == "spectrum_waterfalls":
        assert response.json() == [[1, 2, 3], [4, 5, 6]]
        assert waterfall_plots.retrieve_np_from_timeseries.call_args_list == [
            call("latest_xx_spectrum_waterfalls", start=start, end=end)
        ]
    else:
        assert response.json() == []
        assert waterfall_plots.retrieve_np_from_timeseries.call_args_list == []


@pytest.mark.parametrize(
    "img_type, processing_block, polarisation, start, end, expected_status_code, "
    "expected_media_type",
    [
        ("lag_plots", "latest", "XX", None, None, 200, "application/json"),
        ("spectrum_waterfalls", "latest", "XX", "123456788", "123456789", 200, "application/json"),
    ],
)
def test_send_delta_thumbnail_array(
    img_type,
    processing_block,
    polarisation,
    start,
    end,
    expected_status_code,
    expected_media_type,
    test_app,
):
    """Test general waterfall endpoint."""

    waterfall_plots.retrieve_np_from_timeseries = MagicMock(
        return_value=np.array([[1, 2, 3], [4, 5, 6]])
    )
    url = f"/{img_type}/delta_thumbnail_pb/{processing_block}/C1/C1/{polarisation}"
    params = {}
    if start:
        params["start"] = start
    if end:
        params["end"] = end

    response = test_app.get(url, params=params)

    assert response.status_code == expected_status_code
    assert response.headers["content-type"] == expected_media_type
    assert response.json() == [[1, 2, 3], [4, 5, 6]]
    assert waterfall_plots.retrieve_np_from_timeseries.call_args_list == [
        call(
            (
                "latest_c1_c1_xx"
                if img_type == "spectrum_waterfalls"
                else "latest_c1_c1_xx_lagplots"
            ),
            start=start,
            end=end,
        )
    ]


@pytest.mark.parametrize(
    "img_type, processing_block, polarisation, expected_status_code, expected_media_type",
    [
        ("lag_plots", "latest", "XX", 200, "application/json"),
        ("spectrum_waterfalls", "latest", "XX", 200, "application/json"),
    ],
)
def test_send_spectrum_thumbnail_array(
    img_type,
    processing_block,
    polarisation,
    expected_status_code,
    expected_media_type,
    test_app,
):
    """Test spectrum waterfall endpoint."""

    waterfall_plots.retrieve_np_from_timeseries = MagicMock(
        return_value=np.array([[1, 2, 3], [4, 5, 6]])
    )
    url = f"/{img_type}/thumbnail_pb/{processing_block}/{polarisation}"

    response = test_app.get(url)

    assert response.status_code == expected_status_code
    assert response.headers["content-type"] == expected_media_type
    if img_type == "spectrum_waterfalls":
        assert response.json() == [[1, 2, 3], [4, 5, 6]]
        assert waterfall_plots.retrieve_np_from_timeseries.call_args_list == [
            call("latest_xx_spectrum_waterfalls", start=None, end=None)
        ]
    else:
        assert response.json() == []
        assert waterfall_plots.retrieve_np_from_timeseries.call_args_list == []


@pytest.mark.parametrize(
    "img_type, processing_block, polarisation, expected_status_code, expected_media_type",
    [
        ("lag_plots", "latest", "XX", 200, "application/json"),
        ("spectrum_waterfalls", "latest", "XX", 200, "application/json"),
    ],
)
def test_send_thumbnail_array(
    img_type,
    processing_block,
    polarisation,
    expected_status_code,
    expected_media_type,
    test_app,
):
    """Test general waterfall endpoint."""

    waterfall_plots.retrieve_np_from_timeseries = MagicMock(
        return_value=np.array([[1, 2, 3], [4, 5, 6]])
    )
    url = f"/{img_type}/thumbnail_pb/{processing_block}/C1/C1/{polarisation}"

    response = test_app.get(url)

    assert response.status_code == expected_status_code
    assert response.headers["content-type"] == expected_media_type
    assert response.json() == [[1, 2, 3], [4, 5, 6]]
    assert waterfall_plots.retrieve_np_from_timeseries.call_args_list == [
        call(
            (
                "latest_c1_c1_xx"
                if img_type == "spectrum_waterfalls"
                else "latest_c1_c1_xx_lagplots"
            ),
            start=None,
            end=None,
        )
    ]


def test_send_delta_thumbnail_array_exception(test_app):
    """Test if the exception case is picked up properly."""
    waterfall_plots.retrieve_np_from_timeseries = MagicMock(
        side_effect=Exception("mocked exception")
    )
    url = "/lag_plots/delta_thumbnail_pb/latest/C1/C1/XX"

    response = test_app.get(url)

    assert response.status_code == 200
    assert response.headers["content-type"] == "application/json"
    assert response.json() == []


def test_send_thumbnail_array_exception(test_app):
    """Test if the exception case is picked up properly."""
    waterfall_plots.retrieve_np_from_timeseries = MagicMock(
        side_effect=Exception("mocked exception")
    )
    url = "/lag_plots/thumbnail_pb/latest/C1/C1/XX"

    response = test_app.get(url)

    assert response.status_code == 200
    assert response.headers["content-type"] == "application/json"
    assert response.json() == []


def test_send_spectrum_thumbnail_array_exception(test_app):
    """Test if the exception case is picked up properly."""
    waterfall_plots.retrieve_np_from_timeseries = MagicMock(
        side_effect=Exception("mocked exception")
    )
    url = "/lag_plots/thumbnail_pb/latest/XX"

    response = test_app.get(url)

    assert response.status_code == 200
    assert response.headers["content-type"] == "application/json"
    assert response.json() == []


def test_send_spectrum_delta_thumbnail_array_exception(test_app):
    """Test if the exception case is picked up properly."""
    waterfall_plots.retrieve_np_from_timeseries = MagicMock(
        side_effect=Exception("mocked exception")
    )
    url = "/lag_plots/delta_thumbnail_pb/latest/XX"

    response = test_app.get(url)

    assert response.status_code == 200
    assert response.headers["content-type"] == "application/json"
    assert response.json() == []
