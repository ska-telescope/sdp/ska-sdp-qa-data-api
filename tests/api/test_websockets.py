"""Test the websocket endpoints."""

# pylint: disable=duplicate-code

from unittest.mock import MagicMock, call

import fakeredis

from ska_sdp_qa_data_api.server.api.config_websocket import sdp_config, websocket_stats
from ska_sdp_qa_data_api.server.core.config import (
    API_DATA_FORMAT,
    PROJECT_NAME,
    WATERFALL_THUMBNAIL_MAX_HEIGHT,
    WATERFALL_THUMBNAIL_MAX_WIDTH,
)


def test_websocket_config(test_app):
    """Get the current configuration from the websocket"""
    websocket_stats.redis = fakeredis.FakeStrictRedis()
    sdp_config.reconnect = MagicMock()
    sdp_config.get_from_type_dict = MagicMock(return_value={"return": "value"})
    with test_app.websocket_connect("/ws/ws") as websocket:
        data = websocket.receive_json()
        assert data == {"status": "connected"}

        websocket.send_json({"action": "config"})
        data = websocket.receive_json()
        assert data["event_type"] == "action_call"
        assert data["event_input"] == {"action": "config"}
        assert data["success"] is True

        assert data["data"]["paths"] == {
            "processing_blocks": "/stats/processing_block/blocks",
            "spead2_scans": "/stats/spead2/scans",
            "spectrogram_thumbnail_path": "/spectrograms/thumbnail_pb",
            "lag_plot_thumbnail_path": "/lag_plots/thumbnail_pb",
            "spectrum_waterfalls_thumbnail_path": "/spectrum_waterfalls/thumbnail_pb",
            "subarrays": "/stats/subarrays",
            "websocket": "/ws",
            "log_url": "/log-dashboards",
            "mask_data": "/tel_model/mask_data",
        }
        assert data["data"]["waterfall_plots"] == {
            "thumbnail_width": WATERFALL_THUMBNAIL_MAX_WIDTH,
            "thumbnail_max_height": WATERFALL_THUMBNAIL_MAX_HEIGHT,
        }
        assert data["data"]["api_format"] == API_DATA_FORMAT
        assert data["data"]["project_name"] == PROJECT_NAME
        assert "app_version" in data["data"].keys()
        assert "start_time" in data["data"].keys()
        assert "uptime" in data["data"].keys()


def test_websocket_invalid_action(test_app):
    """Send an invalid action"""
    websocket_stats.redis = fakeredis.FakeStrictRedis()
    sdp_config.reconnect = MagicMock()
    sdp_config.get_from_type_dict = MagicMock(return_value={"return": "value"})
    with test_app.websocket_connect("/ws/ws") as websocket:
        data = websocket.receive_json()
        assert data == {"status": "connected"}

        websocket.send_json({"action": "invalid"})
        data = websocket.receive_json()
        assert data == {
            "event_type": "action_call",
            "event_input": {"action": "invalid"},
            "success": False,
            "data": {"error": "Invalid 'action' specified"},
        }


def test_websocket_missing_action(test_app):
    """Send a dictionary without an action"""
    websocket_stats.redis = fakeredis.FakeStrictRedis()
    sdp_config.reconnect = MagicMock()
    sdp_config.get_from_type_dict = MagicMock(return_value={"return": "value"})
    with test_app.websocket_connect("/ws/ws") as websocket:
        data = websocket.receive_json()
        assert data == {"status": "connected"}

        websocket.send_json({"junk": "invalid"})
        data = websocket.receive_json()
        assert data == {
            "event_type": "action_call",
            "event_input": {"junk": "invalid"},
            "success": False,
            "data": {"error": "Invalid 'action' specified"},
        }


def test_websocket_wrong_type_list(test_app):
    """Send a list instead of a dictionary"""
    websocket_stats.redis = fakeredis.FakeStrictRedis()
    sdp_config.reconnect = MagicMock()
    sdp_config.get_from_type_dict = MagicMock(return_value={"return": "value"})
    with test_app.websocket_connect("/ws/ws") as websocket:
        data = websocket.receive_json()
        assert data == {"status": "connected"}

        websocket.send_json(["my", "list"])
        data = websocket.receive_json()
        assert data == {"error": "Invalid JSON structure"}


def test_websocket_wrong_type(test_app):
    """Send a string instead of a dictionary"""
    websocket_stats.redis = fakeredis.FakeStrictRedis()
    sdp_config.reconnect = MagicMock()
    sdp_config.get_from_type_dict = MagicMock(return_value={"return": "value"})
    with test_app.websocket_connect("/ws/ws") as websocket:
        data = websocket.receive_json()
        assert data == {"status": "connected"}

        websocket.send_json("plain_string")
        data = websocket.receive_json()
        assert data == {"error": "Invalid JSON structure"}


def test_websocket_wrong_type_invalid_json(test_app):
    """Send malformed JSON"""
    websocket_stats.redis = fakeredis.FakeStrictRedis()
    sdp_config.reconnect = MagicMock()
    sdp_config.get_from_type_dict = MagicMock(return_value={"return": "value"})
    with test_app.websocket_connect("/ws/ws") as websocket:
        data = websocket.receive_json()
        assert data == {"status": "connected"}

        websocket.send_text('{"asd}')
        data = websocket.receive_json()
        assert data == {"error": "JSON cannot be parsed"}


def test_websocket_invalid_type_send_string(test_app):
    """Send an invalid action, as a string"""
    websocket_stats.redis = fakeredis.FakeStrictRedis()
    sdp_config.reconnect = MagicMock()
    sdp_config.get_from_type_dict = MagicMock(return_value={"return": "value"})
    with test_app.websocket_connect("/ws/ws") as websocket:
        data = websocket.receive_json()
        assert data == {"status": "connected"}

        websocket.send_text('{"action":"invalid"}')
        data = websocket.receive_json()
        assert data == {
            "event_type": "action_call",
            "event_input": {"action": "invalid"},
            "success": False,
            "data": {"error": "Invalid 'action' specified"},
        }


def test_websocket_get_missing_object(test_app):
    """Request data with not type"""
    websocket_stats.redis = fakeredis.FakeStrictRedis()
    sdp_config.reconnect = MagicMock()
    sdp_config.get_from_type_dict = MagicMock(return_value={"return": "value"})
    with test_app.websocket_connect("/ws/ws") as websocket:
        data = websocket.receive_json()
        assert data == {"status": "connected"}

        websocket.send_json({"action": "get"})
        data = websocket.receive_json()
        assert data == {
            "event_type": "action_call",
            "event_input": {"action": "get"},
            "success": False,
            "data": {"error": "missing parameter 'type'"},
        }


def test_websocket_get_invalid_type(test_app):
    """Request an invalid type"""
    websocket_stats.redis = fakeredis.FakeStrictRedis()
    sdp_config.reconnect = MagicMock()
    sdp_config.get_from_type_dict = MagicMock(side_effect=ValueError("mocked valueerror"))
    with test_app.websocket_connect("/ws/ws") as websocket:
        data = websocket.receive_json()
        assert data == {"status": "connected"}

        websocket.send_json({"action": "get", "type": "invalid-type"})
        data = websocket.receive_json()
        assert data == {
            "event_type": "action_call",
            "event_input": {"action": "get", "type": "invalid-type"},
            "success": False,
            "data": {"error": "mocked valueerror"},
        }


def test_websocket_get_other_exceptions(test_app):
    """Request for data with an unknown exception"""
    websocket_stats.redis = fakeredis.FakeStrictRedis()
    sdp_config.reconnect = MagicMock()
    sdp_config.get_from_type_dict = MagicMock(side_effect=Exception("mocked exception"))
    with test_app.websocket_connect("/ws/ws") as websocket:
        data = websocket.receive_json()
        assert data == {"status": "connected"}

        websocket.send_json({"action": "get", "type": "invalid-type"})
        data = websocket.receive_json()
        assert data == {
            "event_type": "action_call",
            "event_input": {"action": "get", "type": "invalid-type"},
            "success": False,
            "data": {"error": "unknown"},
        }


def test_websocket_get_list(test_app):
    """Request a list of data"""
    websocket_stats.redis = fakeredis.FakeStrictRedis()
    sdp_config.reconnect = MagicMock()
    sdp_config.get_from_type_dict = MagicMock(return_value=["01", "02"])
    with test_app.websocket_connect("/ws/ws") as websocket:
        data = websocket.receive_json()
        assert data == {"status": "connected"}

        websocket.send_json({"action": "get", "type": "list"})
        data = websocket.receive_json()
        assert data == {
            "event_type": "action_call",
            "event_input": {"action": "get", "type": "list"},
            "success": True,
            "data": ["01", "02"],
        }

    assert sdp_config.get_from_type_dict.mock_calls == [call("list", None)]


def test_websocket_get_specific(test_app):
    """Request specific data"""
    websocket_stats.redis = fakeredis.FakeStrictRedis()
    sdp_config.reconnect = MagicMock()
    sdp_config.get_from_type_dict = MagicMock(return_value={"thing": "value"})
    with test_app.websocket_connect("/ws/ws") as websocket:
        data = websocket.receive_json()
        assert data == {"status": "connected"}

        websocket.send_json({"action": "get", "type": "get_thing", "id": "id-1"})
        data = websocket.receive_json()
        assert data == {
            "event_type": "action_call",
            "event_input": {"action": "get", "type": "get_thing", "id": "id-1"},
            "success": True,
            "data": {"thing": "value"},
        }

    assert sdp_config.get_from_type_dict.mock_calls == [call("get_thing", "id-1")]
