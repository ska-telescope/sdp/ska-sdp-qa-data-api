"""Run tests on the new config sub API"""

# pylint: disable=use-implicit-booleaness-not-comparison

from dataclasses import dataclass
from unittest.mock import MagicMock, call

from ska_sdp_qa_data_api.server.api.config import config
from ska_sdp_qa_data_api.server.data.responses import (
    ProcessingBlock,
    ProcessingBlockState,
    Subarray,
)


@dataclass
class Flow:
    """Test representation of a Flow object"""

    pb_id: str
    name: str
    kind: str


@dataclass
class FlowSource:
    """Test representation of a Flow Source object"""

    function: str
    parameters: dict


@dataclass
class FlowSink:
    """Test representation of a Flow Sink object"""

    topics: str
    format: str


@dataclass
class FlowEntry:
    """Test representation of a Flow Entry object"""

    sources: list[FlowSource]
    sink: FlowSink
    data_model: str


def test_subarrays(test_app):
    """Test subarray endpoint uses correct methods"""
    config.reconnect = MagicMock()
    config.subarrays = MagicMock(return_value=["AA"])

    response = test_app.get("/config/subarrays")

    assert response.status_code == 200
    assert response.json() == ["AA"]

    assert config.reconnect.call_args_list == [call()]
    assert config.subarrays.call_args_list == [call()]


def test_get_subarray(test_app):
    """Test subarray with correct match"""
    config.reconnect = MagicMock()
    config.subarray = MagicMock(
        return_value=Subarray(
            eb_id="eb-123",
            last_command="Assign",
            last_command_time="2024",
            obs_state_commanded="ON",
            resources={},
            state_commanded="ON",
        )
    )

    response = test_app.get("/config/subarrays/01")

    assert response.status_code == 200
    assert response.json() == {
        "eb_id": "eb-123",
        "last_command": "Assign",
        "last_command_time": "2024",
        "obs_state_commanded": "ON",
        "resources": {},
        "state_commanded": "ON",
    }

    assert config.reconnect.call_args_list == [call()]
    assert config.subarray.call_args_list == [call("01")]


def test_get_subarray_no_match(test_app):
    """Test subarray where nonsense is passed through"""
    config.reconnect = MagicMock()
    config.subarray = MagicMock()

    response = test_app.get("/config/subarrays/thing")

    assert response.status_code == 400
    assert response.json() == {"message": "subarray doesn't match expected format"}

    assert config.reconnect.call_args_list == []
    assert config.subarray.call_args_list == []


def test_get_subarray_doesnt_exist(test_app):
    """Test subarray where no subarray can be found"""
    config.reconnect = MagicMock()
    config.subarray = MagicMock(return_value=None)

    response = test_app.get("/config/subarrays/99")

    assert response.status_code == 404
    assert response.json() == {"detail": "Subarray doesn't exist"}

    assert config.reconnect.call_args_list == [call()]
    assert config.subarray.call_args_list == [call("99")]


def test_get_processing_blocks(test_app):
    """Test the Processing Block list endpoint"""
    config.reconnect = MagicMock()
    config.processing_blocks = MagicMock(return_value=["pb-123-123-123"])

    response = test_app.get("/config/processing_blocks")

    assert response.status_code == 200
    assert response.json() == ["pb-123-123-123"]

    assert config.reconnect.call_args_list == [call()]
    assert config.processing_blocks.call_args_list == [call()]


def test_get_processing_block(test_app):
    """Test processing block happy path"""
    config.reconnect = MagicMock()
    config.processing_block = MagicMock(
        return_value=ProcessingBlock(
            eb_id="eb-123",
            script={},
            channels_per_port=10,
            processors=["p1", "p2"],
            signal_display_config={"metrics": ["all"]},
        )
    )

    response = test_app.get("/config/processing_blocks/pb-123-123")

    assert response.status_code == 200
    assert response.json() == {
        "eb_id": "eb-123",
        "script": {},
        "channels_per_port": 10,
        "processors": ["p1", "p2"],
        "signal_display_config": {
            "metrics": [
                "all",
            ],
        },
    }

    assert config.reconnect.call_args_list == [call()]
    assert config.processing_block.call_args_list == [call("pb-123-123")]


def test_get_processing_block_no_match(test_app):
    """Test processing block when a nonsense processing block is sent"""
    config.reconnect = MagicMock()
    config.processing_block = MagicMock()

    response = test_app.get("/config/processing_blocks/!@#")

    assert response.status_code == 400
    assert response.json() == {"message": "processing block doesn't match expected format"}

    assert config.reconnect.call_args_list == []
    assert config.processing_block.call_args_list == []


def test_get_processing_block_doesnt_exist(test_app):
    """Test processing block that cannot be found"""
    config.reconnect = MagicMock()
    config.processing_block = MagicMock(return_value=None)

    response = test_app.get("/config/processing_blocks/pb-123-123")

    assert response.status_code == 404
    assert response.json() == {"detail": "Processing Block doesn't exist"}

    assert config.reconnect.call_args_list == [call()]
    assert config.processing_block.call_args_list == [call("pb-123-123")]


def test_get_processing_block_state(test_app):
    """Test the processing block state happy path"""
    config.reconnect = MagicMock()
    config.processing_block_state = MagicMock(
        return_value=ProcessingBlockState(
            deployments={},
            deployments_ready=True,
            last_updated="123",
            resources_available=False,
            status="READ",
        )
    )

    response = test_app.get("/config/processing_blocks/pb-123-123/state")

    assert response.status_code == 200
    assert response.json() == {
        "deployments": {},
        "deployments_ready": True,
        "last_updated": "123",
        "resources_available": False,
        "status": "READ",
    }

    assert config.reconnect.call_args_list == [call()]
    assert config.processing_block_state.call_args_list == [call("pb-123-123")]


def test_get_processing_block_state_no_match(test_app):
    """Test the processing block state where a nonsense ID is sent"""
    config.reconnect = MagicMock()
    config.processing_block_state = MagicMock()

    response = test_app.get("/config/processing_blocks/._./state")

    assert response.status_code == 400
    assert response.json() == {"message": "processing block doesn't match expected format"}

    assert config.reconnect.call_args_list == []
    assert config.processing_block_state.call_args_list == []


def test_get_processing_block_state_doesnt_exist(test_app):
    """Test the processing block state where none can be found"""
    config.reconnect = MagicMock()
    config.processing_block_state = MagicMock(return_value=None)

    response = test_app.get("/config/processing_blocks/pb-123-123/state")

    assert response.status_code == 404
    assert response.json() == {"detail": "Processing Block doesn't exist"}

    assert config.reconnect.call_args_list == [call()]
    assert config.processing_block_state.call_args_list == [call("pb-123-123")]


def test_get_execution_blocks(test_app):
    """Test the execution block list endpoint"""
    config.reconnect = MagicMock()
    config.execution_blocks = MagicMock(return_value=["eb-123-123"])

    response = test_app.get("/config/execution_blocks")

    assert response.status_code == 200
    assert response.json() == ["eb-123-123"]

    assert config.reconnect.call_args_list == [call()]
    assert config.execution_blocks.call_args_list == [call()]


def test_get_execution_block(test_app):
    """Test the execution block happy path"""
    config.reconnect = MagicMock()
    config.execution_block = MagicMock(return_value={"execution_block": "thing"})

    response = test_app.get("/config/execution_blocks/eb-123-123-123")

    assert response.status_code == 200
    assert response.json() == {"execution_block": "thing"}

    assert config.reconnect.call_args_list == [call()]
    assert config.execution_block.call_args_list == [call("eb-123-123-123")]


def test_get_execution_block_baselines(test_app):
    """Test the execution block happy path"""
    config.reconnect = MagicMock()
    config.execution_block = MagicMock(
        return_value={
            "resources": {"receptors": ["C1"]},
            "polarisations": {0: {"corr_type": ["XX", "XY", "YX", "YY"]}},
        }
    )

    response = test_app.get("/config/execution_blocks/eb-123-123-123/baselines")

    assert response.status_code == 200
    assert response.json() == {"baselines": ["C1_C1_XX", "C1_C1_XY", "C1_C1_YX", "C1_C1_YY"]}

    assert config.reconnect.call_args_list == [call()]
    assert config.execution_block.call_args_list == [call("eb-123-123-123")]


def test_get_execution_block_baselines_bad_input(test_app):
    """Test the execution block happy path"""
    config.reconnect = MagicMock()
    config.execution_block = MagicMock()

    response = test_app.get("/config/execution_blocks/._./baselines")

    assert response.status_code == 400
    assert response.json() == {"message": "execution block doesn't match expected format"}

    assert config.reconnect.call_args_list == []
    assert config.execution_block.call_args_list == []


def test_get_execution_block_baselines_doesnt_exist(test_app):
    """Test the execution block happy path"""
    config.reconnect = MagicMock()
    config.execution_block = MagicMock(return_value=None)

    response = test_app.get("/config/execution_blocks/eb-123-123-123/baselines")

    assert response.status_code == 404
    assert response.json() == {"detail": "Execution Block doesn't exist"}

    assert config.reconnect.call_args_list == [call()]
    assert config.execution_block.call_args_list == [call("eb-123-123-123")]


def test_get_execution_block_receptors(test_app):
    """Test the execution block happy path"""
    config.reconnect = MagicMock()
    config.execution_block = MagicMock(return_value={"resources": {"receptors": ["C1"]}})

    response = test_app.get("/config/execution_blocks/eb-123-123-123/receptors")

    assert response.status_code == 200
    assert response.json() == {"receptors": ["C1"]}

    assert config.reconnect.call_args_list == [call()]
    assert config.execution_block.call_args_list == [call("eb-123-123-123")]


def test_get_execution_block_receptors_bad_input(test_app):
    """Test the execution block happy path"""
    config.reconnect = MagicMock()
    config.execution_block = MagicMock(return_value={"resources": {"receptors": ["C1"]}})

    response = test_app.get("/config/execution_blocks/._./receptors")

    assert response.status_code == 400
    assert response.json() == {"message": "execution block doesn't match expected format"}

    assert config.reconnect.call_args_list == []
    assert config.execution_block.call_args_list == []


def test_get_execution_block_receptors_doesnt_exist(test_app):
    """Test the execution block happy path"""
    config.reconnect = MagicMock()
    config.execution_block = MagicMock(return_value=None)

    response = test_app.get("/config/execution_blocks/eb-123-123-123/receptors")

    assert response.status_code == 404
    assert response.json() == {"detail": "Execution Block doesn't exist"}

    assert config.reconnect.call_args_list == [call()]
    assert config.execution_block.call_args_list == [call("eb-123-123-123")]


def test_get_execution_block_no_match(test_app):
    """Test the execution block where a nonsense ID is used"""
    config.reconnect = MagicMock()
    config.execution_block = MagicMock(return_value=["AA"])

    response = test_app.get("/config/execution_blocks/._.")

    assert response.status_code == 400
    assert response.json() == {"message": "execution block doesn't match expected format"}

    assert config.reconnect.call_args_list == []
    assert config.execution_block.call_args_list == []


def test_get_execution_block_doesnt_exist(test_app):
    """Test the execution block where none exists"""
    config.reconnect = MagicMock()
    config.execution_block = MagicMock(return_value=None)

    response = test_app.get("/config/execution_blocks/eb-123-123-123")

    assert response.status_code == 404
    assert response.json() == {"detail": "Execution Block doesn't exist"}

    assert config.reconnect.call_args_list == [call()]
    assert config.execution_block.call_args_list == [call("eb-123-123-123")]


def test_get_deployments(test_app):
    """Test the deployment list endpoint"""
    config.reconnect = MagicMock()
    config.deployments = MagicMock(return_value=["dep-123"])

    response = test_app.get("/config/deployments")

    assert response.status_code == 200
    assert response.json() == ["dep-123"]

    assert config.reconnect.call_args_list == [call()]
    assert config.deployments.call_args_list == [call()]


def test_get_deployment(test_app):
    """Test the deployment happy path"""
    config.reconnect = MagicMock()
    config.deployment = MagicMock(return_value={"deployment": "thing"})

    response = test_app.get("/config/deployments/dep-123")

    assert response.status_code == 200
    assert response.json() == {"deployment": "thing"}

    assert config.reconnect.call_args_list == [call()]
    assert config.deployment.call_args_list == [call("dep-123")]


def test_get_deployment_no_match(test_app):
    """Test the deployment with a nonsense ID"""
    config.reconnect = MagicMock()
    config.deployment = MagicMock()

    response = test_app.get("/config/deployments/._.")

    assert response.status_code == 400
    assert response.json() == {"message": "deployment ID doesn't match expected format"}

    assert config.reconnect.call_args_list == []
    assert config.deployment.call_args_list == []


def test_get_deployment_doesnt_exist(test_app):
    """Test the deployment where none exist"""
    config.reconnect = MagicMock()
    config.deployment = MagicMock(return_value=None)

    response = test_app.get("/config/deployments/dep-123")

    assert response.status_code == 404
    assert response.json() == {"detail": "Deployment doesn't exist"}

    assert config.reconnect.call_args_list == [call()]
    assert config.deployment.call_args_list == [call("dep-123")]


def test_get_deployment_state(test_app):
    """Test the deployment state happy path"""
    config.reconnect = MagicMock()
    config.deployment_state = MagicMock(return_value={"deployment_state": "thing"})

    response = test_app.get("/config/deployments/dep-123/state")

    assert response.status_code == 200
    assert response.json() == {"deployment_state": "thing"}

    assert config.reconnect.call_args_list == [call()]
    assert config.deployment_state.call_args_list == [call("dep-123")]


def test_get_deployment_state_no_match(test_app):
    """Test the deployment state with a nonsense ID"""
    config.reconnect = MagicMock()
    config.deployment_state = MagicMock()

    response = test_app.get("/config/deployments/._./state")

    assert response.status_code == 400
    assert response.json() == {"message": "deployment ID doesn't match expected format"}

    assert config.reconnect.call_args_list == []
    assert config.deployment_state.call_args_list == []


def test_get_deployment_state_doesnt_exist(test_app):
    """Test the deployment state where none exist"""
    config.reconnect = MagicMock()
    config.deployment_state = MagicMock(return_value=None)

    response = test_app.get("/config/deployments/dep-123/state")

    assert response.status_code == 404
    assert response.json() == {"detail": "Deployment doesn't exist"}

    assert config.reconnect.call_args_list == [call()]
    assert config.deployment_state.call_args_list == [call("dep-123")]


def test_get_subarray_state(test_app):
    """Test the subarray state helper endpoint."""

    config.reconnect = MagicMock()
    config.subarray_running_state = MagicMock(
        return_value={
            "subarray": {
                "eb_id": "eb-123",
                "last_command": "Assign",
                "last_command_time": "2024",
                "obs_state_commanded": "ON",
                "resources": {},
                "state_commanded": "ON",
            },
            "execution_block": {"execution_block": "thing", "pb_realtime": ["pb-123"]},
            "execution_block_state": {
                "scan_id": 1,
                "scan_type": "science",
                "scans": [],
                "status": "ACTIVE",
            },
            "processing_block": {
                "eb_id": "eb-123",
                "script": {},
                "channels_per_port": 10,
                "processors": ["p1", "p2"],
                "signal_display_config": {"metrics": ["all"]},
            },
            "processing_block_state": {
                "deployments": {"dep-123": "READY"},
                "deployments_ready": True,
                "last_updated": "123",
                "resources_available": False,
                "status": "READ",
            },
            "deployments": {"dep-123": {"state": "READY", "deployment": {"deployment": "thing"}}},
        }
    )

    response = test_app.get("/config/subarrays/01/current_setup")

    assert config.reconnect.call_args_list == [call()]
    assert response.status_code == 200
    assert response.json() == {
        "subarray": {
            "eb_id": "eb-123",
            "last_command": "Assign",
            "last_command_time": "2024",
            "obs_state_commanded": "ON",
            "resources": {},
            "state_commanded": "ON",
        },
        "execution_block": {"execution_block": "thing", "pb_realtime": ["pb-123"]},
        "execution_block_state": {
            "scan_id": 1,
            "scan_type": "science",
            "scans": [],
            "status": "ACTIVE",
        },
        "processing_block": {
            "eb_id": "eb-123",
            "script": {},
            "channels_per_port": 10,
            "processors": ["p1", "p2"],
            "signal_display_config": {"metrics": ["all"]},
        },
        "processing_block_state": {
            "deployments": {"dep-123": "READY"},
            "deployments_ready": True,
            "last_updated": "123",
            "resources_available": False,
            "status": "READ",
        },
        "deployments": {"dep-123": {"state": "READY", "deployment": {"deployment": "thing"}}},
    }


def test_get_subarray_state_no_data(test_app):
    """Test the subarray state endpoint where there is no data."""

    config.reconnect = MagicMock()
    config.subarray_running_state = MagicMock(return_value=None)

    response = test_app.get("/config/subarrays/01/current_setup")

    assert config.reconnect.call_args_list == [call()]
    assert response.status_code == 200
    assert response.json() == {}


def test_get_subarray_state_error(test_app):
    """Test the subarray state endpoint when there is an exception."""

    config.reconnect = MagicMock()
    config.subarray_running_state = MagicMock(side_effect=ValueError("local_error"))

    response = test_app.get("/config/subarrays/01/current_setup")

    assert config.reconnect.call_args_list == [call()]
    assert response.status_code == 404
    assert response.json() == {"detail": "local_error"}


def test_get_subarray_invalid_subarray(test_app):
    """Test the subarray state endpoint when there is an invalid subarray provided."""

    config.reconnect = MagicMock()
    config.subarray_running_state = MagicMock(side_effect=ValueError("local_error"))

    response = test_app.get("/config/subarrays/AA/current_setup")

    assert config.reconnect.call_args_list == []
    assert response.status_code == 400
    assert response.json() == {"message": "subarray doesn't match expected format"}


def test_get_flows_no_flows(test_app):
    """Get the list of flows where none is specified."""
    config.reconnect = MagicMock()
    config.flows = MagicMock(return_value=[])

    response = test_app.get("/config/flows")

    assert config.reconnect.call_args_list == [call()]
    assert response.status_code == 200
    assert response.json() == []


def test_get_flows_default_list(test_app):
    """Get the list of flows with no filtering."""
    config.reconnect = MagicMock()
    config.flows = MagicMock(
        return_value=[
            (
                Flow(pb_id="pb-test-123456", name="metrics-spectrum-01", kind="data-queue"),
                FlowEntry(
                    sources=[
                        FlowSource(
                            function="SignalDisplayMetrics",
                            parameters={
                                "metric_type": "spectrum",
                                "nchan_avg": 50,
                                "additional_windows": 5,
                                "rounding_sensitivity": 10,
                            },
                        )
                    ],
                    sink=FlowSink(topics="topic-metrics-spectrum-01", format="msgpack_numpy"),
                    data_model="MetricPayload",
                ),
            ),
            (
                Flow(pb_id="pb-test-654321", name="metrics-spectrum-02", kind="data-queue"),
                FlowEntry(
                    sources=[
                        FlowSource(
                            function="SignalDisplayMetrics",
                            parameters={
                                "metric_type": "spectrum",
                                "nchan_avg": 50,
                                "additional_windows": 5,
                                "rounding_sensitivity": 10,
                            },
                        )
                    ],
                    sink=FlowSink(topics="topic-metrics-spectrum-01", format="msgpack_numpy"),
                    data_model="MetricPayload",
                ),
            ),
            (
                Flow(pb_id="pb-test-123456", name="unknown", kind="data-queue"),
                FlowEntry(
                    sources=[FlowSource(function="unknown", parameters={})],
                    sink=FlowSink(topics="unknown", format="msgpack_numpy"),
                    data_model="unknown",
                ),
            ),
        ]
    )

    response = test_app.get("/config/flows")

    assert config.reconnect.call_args_list == [call()]
    assert response.status_code == 200
    assert response.json() == [
        {
            "processing_block_id": "pb-test-123456",
            "name": "metrics-spectrum-01",
            "kind": "data-queue",
            "topic": "topic-metrics-spectrum-01",
            "format": "msgpack_numpy",
            "metric": "spectrum",
            "nchan_avg": 50,
            "additional_windows": 5,
            "rounding_sensitivity": 10,
            "data_model": "MetricPayload",
        },
        {
            "processing_block_id": "pb-test-654321",
            "name": "metrics-spectrum-02",
            "kind": "data-queue",
            "topic": "topic-metrics-spectrum-01",
            "format": "msgpack_numpy",
            "metric": "spectrum",
            "nchan_avg": 50,
            "additional_windows": 5,
            "rounding_sensitivity": 10,
            "data_model": "MetricPayload",
        },
    ]


def test_get_flows_filter_by_pb(test_app):
    """Get the list of flows and filter by processing block ID."""
    config.reconnect = MagicMock()
    config.flows = MagicMock(
        return_value=[
            (
                Flow(pb_id="pb-test-123456", name="metrics-spectrum-01", kind="data-queue"),
                FlowEntry(
                    sources=[
                        FlowSource(
                            function="SignalDisplayMetrics",
                            parameters={
                                "metric_type": "spectrum",
                                "nchan_avg": 50,
                                "additional_windows": 5,
                                "rounding_sensitivity": 10,
                            },
                        )
                    ],
                    sink=FlowSink(topics="topic-metrics-spectrum-01", format="msgpack_numpy"),
                    data_model="MetricPayload",
                ),
            ),
            (
                Flow(pb_id="pb-test-654321", name="metrics-spectrum-02", kind="data-queue"),
                FlowEntry(
                    sources=[
                        FlowSource(
                            function="SignalDisplayMetrics",
                            parameters={
                                "metric_type": "spectrum",
                                "nchan_avg": 50,
                                "additional_windows": 5,
                                "rounding_sensitivity": 10,
                            },
                        )
                    ],
                    sink=FlowSink(topics="topic-metrics-spectrum-01", format="msgpack_numpy"),
                    data_model="MetricPayload",
                ),
            ),
            (
                Flow(pb_id="pb-test-123456", name="unknown", kind="data-queue"),
                FlowEntry(
                    sources=[FlowSource(function="unknown", parameters={})],
                    sink=FlowSink(topics="unknown", format="msgpack_numpy"),
                    data_model="unknown",
                ),
            ),
        ]
    )

    response = test_app.get("/config/flows?processing_block=pb-test-123456")

    assert config.reconnect.call_args_list == [call()]
    assert response.status_code == 200
    assert response.json() == [
        {
            "processing_block_id": "pb-test-654321",
            "name": "metrics-spectrum-02",
            "kind": "data-queue",
            "topic": "topic-metrics-spectrum-01",
            "format": "msgpack_numpy",
            "metric": "spectrum",
            "nchan_avg": 50,
            "additional_windows": 5,
            "rounding_sensitivity": 10,
            "data_model": "MetricPayload",
        }
    ]


def test_get_flows_bad_pb(test_app):
    """Get the list of flows by providing a bad processing block ID."""
    config.reconnect = MagicMock()
    config.flows = MagicMock(return_value=[])

    response = test_app.get("/config/flows?processing_block=qwe.123")

    assert config.reconnect.call_args_list == []
    assert response.status_code == 400
    assert response.json() == {"message": "processing block doesn't match expected format"}
