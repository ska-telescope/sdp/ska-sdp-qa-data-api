# pylint: disable=R0801
"""Basic test for the ska_sdp_qa_data_api fastapi module."""


def test_get_mask_data_main(test_app):
    """Can we hit and retrieve data from the mask data endpoint"""
    response = test_app.get("/tel_model/mask_data/scan_id/subarray")
    assert response.status_code == 200
    assert response.json()["data"] is not None
