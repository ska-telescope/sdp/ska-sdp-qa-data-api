"""Basic test for the ska_sdp_qa_data_api fastapi module."""

from ska_sdp_qa_data_api.server.api.core import grafana_link, kibana_query_link
from ska_sdp_qa_data_api.server.core.config import (
    API_DATA_FORMAT,
    PROJECT_NAME,
    WATERFALL_THUMBNAIL_MAX_HEIGHT,
    WATERFALL_THUMBNAIL_MAX_WIDTH,
)

# pylint: disable=duplicate-code


def test_ping_main(test_app):
    """Can we hit the ping endpoint"""
    response = test_app.get("/ping")
    assert response.status_code == 200
    assert response.json() == {"ping": "live"}


def test_config(test_app):
    """Test the config endpoint."""
    response = test_app.get("/config")
    assert response.status_code == 200
    data = response.json()
    assert data["paths"] == {
        "processing_blocks": "/stats/processing_block/blocks",
        "spead2_scans": "/stats/spead2/scans",
        "spectrogram_thumbnail_path": "/spectrograms/thumbnail_pb",
        "lag_plot_thumbnail_path": "/lag_plots/thumbnail_pb",
        "spectrum_waterfalls_thumbnail_path": "/spectrum_waterfalls/thumbnail_pb",
        "subarrays": "/stats/subarrays",
        "websocket": "/ws",
        "log_url": "/log-dashboards",
        "mask_data": "/tel_model/mask_data",
    }
    assert data["waterfall_plots"] == {
        "thumbnail_width": WATERFALL_THUMBNAIL_MAX_WIDTH,
        "thumbnail_max_height": WATERFALL_THUMBNAIL_MAX_HEIGHT,
    }
    assert data["api_format"] == API_DATA_FORMAT
    assert data["project_name"] == PROJECT_NAME
    assert "app_version" in data.keys()
    assert "start_time" in data.keys()
    assert "uptime" in data.keys()


def test_get_log_urls(test_app):
    """Test the log configs endpoint."""
    response = test_app.get("/log-dashboards/01")
    assert response.status_code == 200
    data = response.json()
    assert "kibana" in data.keys()
    assert "grafana" in data.keys()
    assert len(data["grafana"].keys()) == 2


def test_get_log_urls_no_subarray(test_app):
    """Test the log configs endpoint."""
    response = test_app.get("/log-dashboards/AA")
    assert response.status_code == 400
    assert response.json() == {"detail": "Subarray or Namespace error."}


def test_grafana_url_generation():
    """Test that the grafana url generator builds the url correctly"""
    url = grafana_link("test-dashboard", {"var": "value"}, "https://test.test")
    expected_url = "https://test.test/d/test-dashboard?orgId=1&refresh=5s&var=value"
    assert url == expected_url


def test_kibana_url_generation():
    """Test that the grafana url generator builds the url correctly"""
    search_terms = ["term: this", "term: that", "term2: other"]
    columns = ["this", "that"]
    url = kibana_query_link(search_terms, columns, "https://test.test")
    expected_url = (
        "https://test.test/kibana/app/discover#/?_g=(filters:!(),refreshInterval:"
        "(pause:!t,value:0),time:(from:now-15m,to:now))&_a=(columns:!(t,t),filters:!(),grid:"
        "(columns:(t:(width:h),t:(width:h))),index:cbb05bec-ed81-45f8-b11a-eab26a3df6b1,interval:"
        "auto,query:(language:kuery,query:'term: this or term: that or term2: other'),sort:"
        "!(!('@timestamp',desc)))"
    )
    assert url == expected_url
