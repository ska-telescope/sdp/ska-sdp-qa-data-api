"""Run tests for the data Websockets."""

from unittest.mock import patch

import fakeredis

from ska_sdp_qa_data_api.server.api.consumer_ctl import websocket_stats
from tests.test_data.helpers import FakeDataQueue


@patch("ska_sdp_qa_data_api.server.api.consumer_ctl.API_DATA_FORMAT", "json")
def test_websocket_not_allowed(test_app):
    """Test that the basic JSON response works."""

    websocket_stats.redis = fakeredis.FakeStrictRedis()

    with test_app.websocket_connect("/ws/metrics-bad-0") as websocket:
        data = websocket.receive_json()
        assert data == {
            "status": "error",
            "message": "You cannot connect to metrics-bad-0",
        }


@patch("ska_sdp_qa_data_api.server.api.consumer_ctl.API_DATA_FORMAT", "json")
def test_websocket_json(test_app, monkeypatch):
    """Test that the basic JSON response works."""

    websocket_stats.redis = fakeredis.FakeStrictRedis()

    # pylint: disable=unused-argument
    def mock_get(server, set_group_id, auto_offset_reset, set_consumer_offsets):
        return FakeDataQueue("metrics-spectrum-0", '{"id": "q"}')

    monkeypatch.setattr("ska_sdp_qa_data_api.server.api.consumer_ctl.DataQueueConsumer", mock_get)
    with test_app.websocket_connect("/ws/metrics-spectrum-0") as websocket:
        data = websocket.receive_json()
        assert data == {"status": "connected"}
        data = websocket.receive_json()
        assert data == {"id": "q"}


@patch("ska_sdp_qa_data_api.server.api.consumer_ctl.API_DATA_FORMAT", "json")
def test_websocket_json_many(test_app, monkeypatch):
    """Test that we can receive multiple messages on the websocket."""

    websocket_stats.redis = fakeredis.FakeStrictRedis()

    # pylint: disable=W0613
    def mock_get(server, set_group_id, auto_offset_reset, set_consumer_offsets):
        return FakeDataQueue("metrics-spectrum-0", '{"id": "q"}', count=5)

    monkeypatch.setattr("ska_sdp_qa_data_api.server.api.consumer_ctl.DataQueueConsumer", mock_get)
    with test_app.websocket_connect("/ws/metrics-spectrum-0") as websocket:
        data = websocket.receive_json()
        assert data == {"status": "connected"}
        for _ in range(5):
            data = websocket.receive_json()
            assert data == {"id": "q"}
