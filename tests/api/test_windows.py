"""Test that the window endpoints send the data to the config call correctly."""

from unittest.mock import MagicMock, call

from ska_sdp_dataqueues.schemas.signal_display_metrics import MetricDataTypes

from ska_sdp_qa_data_api.server.api.windows import config
from ska_sdp_qa_data_api.server.data.inputs import CreateWindow
from ska_sdp_qa_data_api.server.data.responses import Window


def test_get_basic_call(test_app):
    """Send the GET call without any filters"""
    config.reconnect = MagicMock()
    config.windows = MagicMock(
        return_value=[
            Window(
                topic="topic",
                processing_block="pb-id",
                max_window_count=5,
                channels_averaged=25,
                windows=[],
            )
        ]
    )

    response = test_app.get("/windows/")

    assert response.status_code == 200
    assert response.json() == [
        {
            "topic": "topic",
            "processing_block": "pb-id",
            "max_window_count": 5,
            "channels_averaged": 25,
            "windows": [],
        }
    ]

    assert config.reconnect.call_args_list == [call()]
    assert config.windows.call_args_list == [call(None, None, None)]


def test_get_send_filters(test_app):
    """Send through the filters"""
    config.reconnect = MagicMock()
    config.windows = MagicMock(
        return_value=[
            Window(
                topic="topic",
                processing_block="pb-id",
                max_window_count=5,
                channels_averaged=25,
                windows=[],
            )
        ]
    )

    response = test_app.get("/windows/?subarray=01&processing_block=pb-id&metric=spectrum")

    assert response.status_code == 200
    assert response.json() == [
        {
            "topic": "topic",
            "processing_block": "pb-id",
            "max_window_count": 5,
            "channels_averaged": 25,
            "windows": [],
        }
    ]

    assert config.reconnect.call_args_list == [call()]
    assert config.windows.call_args_list == [call("01", "pb-id", "spectrum")]


def test_get_invalid_subarray(test_app):
    """Try filter using an invalid subarray"""
    config.reconnect = MagicMock()
    config.windows = MagicMock()

    response = test_app.get("/windows/?subarray=AA")

    assert response.status_code == 400
    assert response.json() == {"message": "subarray doesn't match expected format"}

    assert not config.reconnect.call_args_list
    assert not config.windows.call_args_list


def test_get_invalid_processing_block(test_app):
    """Try filter using an invalid processing block ID"""
    config.reconnect = MagicMock()
    config.windows = MagicMock()

    response = test_app.get("/windows/?processing_block=..")

    assert response.status_code == 400
    assert response.json() == {"message": "processing block doesn't match expected format"}

    assert not config.reconnect.call_args_list
    assert not config.windows.call_args_list


def test_get_invalid_metric(test_app):
    """Try filter using an invalid metric"""
    config.reconnect = MagicMock()
    config.windows = MagicMock()

    response = test_app.get("/windows/?metric=AA")

    assert response.status_code == 400
    assert response.json() == {"message": "metric doesn't match list of available metrics"}

    assert not config.reconnect.call_args_list
    assert not config.windows.call_args_list


def test_create_full(test_app):
    """Send the full correct data to create"""
    config.reconnect = MagicMock()
    config.window_create = MagicMock(
        return_value={
            "success": True,
            "window_id": 1,
            "message": "New window added",
        }
    )

    response = test_app.post(
        "/windows/",
        json={
            "metric": "spectrum",
            "subarray": "01",
            "spectrum_start": 100,
            "spectrum_end": 500,
            "channels_averaged": 3,
            "processing_block": "pb-id",
        },
    )

    assert response.status_code == 200
    assert response.json() == {
        "success": True,
        "window_id": 1,
        "message": "New window added",
    }

    assert config.reconnect.call_args_list == [call()]
    assert config.window_create.call_args_list == [
        call(
            CreateWindow(
                metric=MetricDataTypes.SPECTRUM,
                subarray="01",
                processing_block="pb-id",
                spectrum_start=100,
                spectrum_end=500,
                channels_averaged=3,
            ),
            "guest@community.skao.int",
        )
    ]


def test_create_basic(test_app):
    """Send the minimum (and most likely to be used)"""
    config.reconnect = MagicMock()
    config.window_create = MagicMock(
        return_value={
            "success": True,
            "window_id": 1,
            "message": "New window added",
        }
    )

    response = test_app.post(
        "/windows/",
        json={"metric": "spectrum", "subarray": "01", "spectrum_start": 100, "spectrum_end": 500},
    )

    assert response.status_code == 200
    assert response.json() == {
        "success": True,
        "window_id": 1,
        "message": "New window added",
    }

    assert config.reconnect.call_args_list == [call()]
    assert config.window_create.call_args_list == [
        call(
            CreateWindow(
                metric=MetricDataTypes.SPECTRUM,
                subarray="01",
                processing_block=None,
                spectrum_start=100,
                spectrum_end=500,
                channels_averaged=1,
            ),
            "guest@community.skao.int",
        )
    ]


def test_create_missing_required(test_app):
    """Skip required parameters"""
    config.reconnect = MagicMock()
    config.window_create = MagicMock()

    response = test_app.post(
        "/windows/",
        json={"metric": "spectrum"},
    )

    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "type": "missing",
                "loc": ["body", "subarray"],
                "msg": "Field required",
                "input": {"metric": "spectrum"},
            },
            {
                "type": "missing",
                "loc": ["body", "spectrum_start"],
                "msg": "Field required",
                "input": {"metric": "spectrum"},
            },
            {
                "type": "missing",
                "loc": ["body", "spectrum_end"],
                "msg": "Field required",
                "input": {"metric": "spectrum"},
            },
        ]
    }

    assert not config.reconnect.call_args_list
    assert not config.window_create.call_args_list


def test_create_fail_validation(test_app):
    """Fail the input validation"""
    config.reconnect = MagicMock()
    config.window_create = MagicMock(
        return_value={
            "success": True,
            "window_id": 1,
            "message": "New window added",
        }
    )

    response = test_app.post(
        "/windows/",
        json={
            "metric": "bad-input",
            "subarray": "A",
            "spectrum_start": 0,
            "spectrum_end": -500,
            "channels_averaged": 0,
            "processing_block": "...",
        },
    )

    metrics = [f"'{s.value}'" for s in iter(MetricDataTypes)]

    options = ", ".join(metrics[:-1]) + f" or {metrics[-1]}"

    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "type": "enum",
                "loc": ["body", "metric"],
                "msg": f"Input should be {options}",
                "input": "bad-input",
                "ctx": {"expected": f"{options}"},
            },
            {
                "type": "string_pattern_mismatch",
                "loc": ["body", "subarray"],
                "msg": "String should match pattern '^[0-9]{2}$'",
                "input": "A",
                "ctx": {"pattern": "^[0-9]{2}$"},
            },
            {
                "type": "string_pattern_mismatch",
                "loc": ["body", "processing_block"],
                "msg": "String should match pattern '^[A-Za-z0-9\\-_]+$'",
                "input": "...",
                "ctx": {"pattern": "^[A-Za-z0-9\\-_]+$"},
            },
        ]
    }

    assert not config.reconnect.call_args_list
    assert not config.window_create.call_args_list
