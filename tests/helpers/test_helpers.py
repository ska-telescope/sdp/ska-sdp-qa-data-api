"""Basic test for the helper functions."""

import time

import pytest

from ska_sdp_qa_data_api.server.utils.helpers import Timer, _s, size_to_str


def test_s_with_string_input():
    """Test for str input."""
    assert _s("") == ""
    assert _s("Test") == "Test"


def test_s_with_bytes_input():
    """Test for bytes input."""
    assert _s(b"") == ""
    assert _s(b"Test") == "Test"


def test_s_with_non_utf8_bytes():
    """Test for non-ASCII bytes input."""
    assert _s(b"\xe2\x98\x83") == "☃"


def test_s_with_invalid_input():
    """Test for invalid input types."""
    with pytest.raises(AttributeError):
        _s(12345)


def test_timer_duration():
    """Simulate a task that takes some time."""
    with Timer("test") as timer:
        time.sleep(0.1)

    # Check if the duration measured is approximately 0.1 seconds
    assert 0.09 <= time.time() - timer.start <= 0.11


def test_size_to_str_bytes():
    """Test cases for different byte sizes."""
    assert size_to_str(512) == "512.0 B"
    assert size_to_str(0) == "0.0 B"


def test_size_to_str_kilobytes():
    """Test cases for different byte sizes."""
    assert size_to_str(1536) == "1.5 KB"
    assert size_to_str(2047) == "2.0 KB"


def test_size_to_str_megabytes():
    """Test cases for different byte sizes."""
    assert size_to_str(1024 * 1024) == "1.0 MB"
    assert size_to_str(1.5 * 1024 * 1024) == "1.5 MB"


def test_size_to_str_gigabytes():
    """Test cases for different byte sizes."""
    assert size_to_str(1024 * 1024 * 1024) == "1.0 GB"
    assert size_to_str(2 * 1024 * 1024 * 1024) == "2.0 GB"


# Test for fractional sizes
def test_size_to_str_fractional_bytes():
    """Test cases for different byte sizes."""
    assert size_to_str(512.5) == "512.5 B"
    assert size_to_str(1024 + 512.5) == "1.5 KB"
