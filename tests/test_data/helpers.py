"""Utility functions for tests."""

import gzip
import json
from pathlib import Path

import msgpack
import msgpack_numpy

msgpack_numpy.patch()


# pylint: disable=R0903
class Msg:
    """Fake message to be received."""

    def __init__(self, topic, value):
        self.first = True
        self.topic = topic
        self.value = value


class FakeDataQueue:
    """Mocked Kafka Client."""

    def __init__(self, topic, message, count=1):
        self.first = True
        self._topic = topic
        self._message = message
        self._count = count
        self.calls = []

    async def __aenter__(self):
        """Use class as contextmanager"""
        self.calls.append(("aenter",))
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        """Stop consumer and producer upon exit"""
        self.calls.append(("aexit",))

    async def consumer_start(
        self, topics: list[str], auto_offset_reset="earliest", set_group_id=True
    ):
        """Start consumer"""
        self.calls.append(("consumer", (topics, auto_offset_reset, set_group_id)))

    async def close(self):
        """Run close"""
        self.calls.append(("close",))

    async def __aiter__(self):
        """Read from the queue"""
        self.calls.append(("load",))

        for _ in range(self._count):
            yield (self._topic, self._message)

    async def update_topics(self, topics: list[str]):
        """Update topic list"""
        self.calls.append(("update", topics))

    async def assign_topics(self, topics: list[tuple[str, int]]):
        """Update topic list"""
        self.calls.append(("assign", topics))


# pylint: disable=super-init-not-called
class KafkaFakeFile(FakeDataQueue):
    """Mocked Kafka Client"""

    def __init__(self, topic: str, file: Path, data_type="msgpack"):
        super().__init__(topic, "", 0)
        with gzip.open(file, mode="r") as infile:
            if data_type == "msgpack":
                data = msgpack.unpack(infile)
            else:
                data = json.load(infile)
        self._it = iter([(topic, record) for record in data])

    async def __aiter__(self):
        """Load from queue"""
        self.calls.append(("load",))
        for i in self._it:
            yield i
