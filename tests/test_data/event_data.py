"""Creates data for the various meta data layers."""

# pylint: disable=too-many-arguments,too-many-positional-arguments

from time import time


def spead2_empty():
    """Generate empty packet."""
    return {
        "type": "receive_stats",
        "time": time(),
        "scan_id": -1,
        "state": "no_stats",
        "total_megabytes": 0.0,
        "num_heaps": 0,
        "num_incomplete": 0,
        "duration": 0.0,
        "streams": [],
    }


def spead2_progress(scan_id=123456, iteration=1, start_seconds_ago=1):
    """Generate in progress packat."""
    return {
        "type": "receive_stats",
        "time": time(),
        "scan_id": scan_id,
        "state": "stats",
        "total_megabytes": iteration * 1.32159,
        "num_heaps": iteration * 4,
        "num_incomplete": 0,
        "duration": time() - start_seconds_ago,
        "streams": [
            {
                "id": 0,
                "heaps": iteration,
                "blocked": 0,
                "incomplete_heaps": 0,
            },
            {
                "id": 1,
                "heaps": iteration,
                "blocked": 0,
                "incomplete_heaps": 0,
            },
        ],
    }


def plasma_state_update(
    processing_block_id="test-123-456",
    scan_id=123456,
    execution_block_id="eb-test-1",
    subarray_id="subarray_1",
    iteration=1,
    state="receiving",
):
    """Generic message for all states."""
    return {
        "type": "visibility_receive",
        "time": time(),
        "state": state,
        "processing_block_id": processing_block_id,
        "execution_block_id": execution_block_id,
        "subarray_id": subarray_id,
        "scan_id": scan_id,
        "payloads_received": iteration,
        "time_slices": 1,
        "time_since_last_payload": 0.9,
    }
