"""Test Task Receive Waterfall Spectrograms"""

import asyncio
import gzip
from pathlib import Path
from unittest.mock import ANY, call, patch

import msgpack
import msgpack_numpy
import pytest
from ska_sdp_dataqueues.schemas.signal_display_metrics import metrics_from_dict

from ska_sdp_qa_data_api.server.tasks.receive_waterfall_spectrogram import (
    ReceiveWaterfallSpectrograms,
)
from tests.test_data.helpers import KafkaFakeFile

msgpack_numpy.patch()

# pylint: disable=protected-access


@pytest.mark.asyncio
@pytest.mark.parametrize("telescope", ["mid", "low"])
@patch("ska_sdp_qa_data_api.server.tasks.kafka_task.DataQueueConsumer")
@patch("ska_sdp_qa_data_api.server.tasks.kafka_task.SDPConfig", autospec=True)
@patch(
    "ska_sdp_qa_data_api.server.tasks.receive_waterfall_spectrogram.SpectrogramPlots",
    autospec=True,
)
async def test_full(mock_plots, mock_config, mock_data_queue, telescope: str):
    file = Path(f"tests/test_data/{telescope}.spectrograms.gz")
    with gzip.open(file, mode="r") as infile:
        data = [metrics_from_dict(record) for record in msgpack.unpack(infile)]
    mock_data_queue.return_value = KafkaFakeFile("", file)

    task = ReceiveWaterfallSpectrograms()

    async def set_topics():
        task.topics = [f"metrics-{task._metric_type}-01"]
        await task._data_queue.update_topics(task.topics)

    task.watch_for_flows = set_topics

    await task.run_consumer()
    await asyncio.sleep(1)

    # Using the default, because of the background task
    assert task.topics == ["metrics-phase-01"]
    assert mock_config.mock_calls == []

    calls = [call(ANY, ANY)]
    for record in data:
        calls.append(call().process_processed(record))

    size_got, size_expected = len(mock_plots.mock_calls), len(calls)
    assert size_got == size_expected

    check_sameness = mock_plots.mock_calls[1:] == calls[1:]
    assert check_sameness
    assert mock_data_queue.return_value.calls == [
        ("aenter",),
        ("update", ["metrics-phase-01"]),
        ("load",),
        ("aexit",),
    ]
