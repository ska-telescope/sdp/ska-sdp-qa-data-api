"""Test the Redis Cleanup."""

from unittest.mock import call, patch

from ska_sdp_qa_data_api.server.tasks.redis_cleanup import run_clean_procedure

# pylint: disable=unnecessary-dunder-call


@patch("ska_sdp_qa_data_api.server.tasks.redis_cleanup.time")
@patch("ska_sdp_qa_data_api.server.tasks.redis_cleanup.Redis", autospec=True)
def test_run_clean_procedure_none(mock_redis, mock_time):
    """Test the clean procure with no data."""

    mock_time.return_value = 12345

    mock_redis.return_value.keys.return_value = ["waterfall:pb1".encode()]
    mock_redis.return_value.type.return_value = "zset".encode()
    mock_redis.return_value.zcount.return_value = 1
    mock_redis.return_value.zrange.return_value = [[1234]]
    mock_redis.return_value.zremrangebyscore.return_value = 1

    run_clean_procedure()

    assert mock_redis.mock_calls == [
        call(host="localhost", port="6379"),
        call().keys("waterfall:*"),
        call().type(b"waterfall:pb1"),
        call().zcount(b"waterfall:pb1", min=172234872, max=17223487240),
        call().zrange(b"waterfall:pb1", -1, -1, withscores=True),
        call().zremrangebyscore(b"waterfall:pb1", 172234872, 8745),
    ]

    assert mock_time.mock_calls == [call(), call()]
