"""Test Task Receive Spectrum Waterfalls"""

import asyncio
from pathlib import Path
from unittest.mock import AsyncMock, call, patch

import pytest

from ska_sdp_qa_data_api.server.tasks.calibration_data import ReceiveCalibrationData
from tests.test_data.helpers import KafkaFakeFile

# pylint: disable=protected-access


@pytest.mark.asyncio
@patch("ska_sdp_qa_data_api.server.tasks.kafka_task.DataQueueConsumer")
@patch("ska_sdp_qa_data_api.server.tasks.calibration_data.DataQueueProducer")
@patch("ska_sdp_qa_data_api.server.tasks.kafka_task.SDPConfig", autospec=True)
async def test_full(mock_config, mock_producer, mock_data_queue):
    """Test the Calibration data flow."""
    file = Path("tests/test_data/pointing_offset.gz")

    mock_data_queue.return_value = KafkaFakeFile("", file)
    mock_producer.return_value = AsyncMock()

    task = ReceiveCalibrationData()

    async def set_topics():
        task.topics = ["pointing-offset-01"]
        await task._data_queue.update_topics(task.topics)

    task.watch_for_flows = set_topics

    await task.run_consumer()
    await asyncio.sleep(1)

    # Using the default, because of the background task
    assert task.topics == ["pointing-offset-01"]
    assert mock_config.mock_calls == []

    assert mock_data_queue.return_value.calls == [
        ("aenter",),
        ("update", ["pointing-offset-01"]),
        ("load",),
        ("aexit",),
    ]
    assert mock_producer.return_value.mock_calls == [
        call._producer_start(),
        call.send(
            data={
                "antenna_name": ["10", "110"],
                "xel_offset": [2.2, 32.2],
                "xel_offset_std": [3.3, 43.3],
                "el_offset": [4.4, 54.4],
                "el_offset_std": [5.5, 65.5],
                "expected_width_h": [6.6, 76.6],
                "expected_width_v": [10.1, 1110.1],
                "fitted_width_h": [8.8, 98.8],
                "fitted_width_h_std": [9.9, 109.9],
                "fitted_width_v": [10.1, 1110.1],
                "fitted_width_v_std": [11.11, 1211.11],
                "fitted_height": [12.12, 1312.12],
                "fitted_height_std": [13.13, 1413.13],
            },
            topic="metrics-pointing_offset_out-",
        ),
        call._producer_stop(),
    ]
