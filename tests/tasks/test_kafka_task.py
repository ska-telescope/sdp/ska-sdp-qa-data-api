"""Test the kafka helper class."""

# pylint: disable=invalid-name

from unittest.mock import AsyncMock, call, patch

import pytest
from ska_sdp_dataqueues import Encoding

from ska_sdp_qa_data_api.server.core.config import BROKER_INSTANCE
from ska_sdp_qa_data_api.server.tasks.kafka_task import KafkaTask
from tests.test_data.helpers import FakeDataQueue


@pytest.mark.asyncio
@patch("ska_sdp_qa_data_api.server.tasks.kafka_task.DataQueueConsumer")
async def test_kafka_run_no_pattern(mock_data_queue):
    """Make sure that if no pattern is set, we get an error."""

    # pylint: disable=unnecessary-dunder-call

    my_consumer = FakeDataQueue("my-topic", '{"id": "q"}', count=0)
    mock_data_queue.return_value = my_consumer

    kt = KafkaTask()
    kt.setup = AsyncMock()
    kt.on_exit = AsyncMock()
    await kt.run_consumer()
    assert mock_data_queue.mock_calls == [
        call(
            server=BROKER_INSTANCE,
            set_group_id=False,
            auto_offset_reset="latest",
            set_consumer_offsets=False,
            encoding=Encoding.MSGPACK_NUMPY,
        )
    ]
    kt.setup.assert_called_once()
    kt.on_exit.assert_called_once()


@pytest.mark.asyncio
@patch("ska_sdp_qa_data_api.server.tasks.kafka_task.DataQueueConsumer")
async def test_kafka_no_messages(mock_data_queue):
    """Test that things work with no messages."""
    my_consumer = FakeDataQueue("my-topic", '{"id": "q"}', count=0)
    mock_data_queue.return_value = my_consumer

    kt = KafkaTask()
    kt.setup = AsyncMock()
    kt.on_exit = AsyncMock()
    kt.receive = AsyncMock()
    kt.topics = ["my-topic"]
    await kt.run_consumer()
    assert mock_data_queue.mock_calls == [
        call(
            server=BROKER_INSTANCE,
            set_group_id=False,
            auto_offset_reset="latest",
            set_consumer_offsets=False,
            encoding=Encoding.MSGPACK_NUMPY,
        )
    ]
    kt.setup.assert_called_once()
    kt.on_exit.assert_called_once()
    assert kt.receive.mock_calls == []
    assert my_consumer.calls == [
        ("aenter",),
        ("load",),
        ("aexit",),
    ]


@pytest.mark.asyncio
@patch("ska_sdp_qa_data_api.server.tasks.kafka_task.DataQueueConsumer")
async def test_kafka_single_message_throw_error(mock_data_queue):
    """Check that if there is a message but we have not overwritten the
    receive method, that we get an error."""
    my_consumer = FakeDataQueue("my-topic", '{"id": "q"}', count=1)
    mock_data_queue.return_value = my_consumer

    kt = KafkaTask()
    kt.setup = AsyncMock()
    kt.on_exit = AsyncMock()
    kt.topics = ["my-topic"]
    with pytest.raises(NotImplementedError, match="'receive' not overwritten"):
        await kt.run_consumer()
    assert mock_data_queue.mock_calls == [
        call(
            server=BROKER_INSTANCE,
            set_group_id=False,
            auto_offset_reset="latest",
            set_consumer_offsets=False,
            encoding=Encoding.MSGPACK_NUMPY,
        )
    ]
    kt.setup.assert_called_once()
    kt.on_exit.assert_called_once()
    assert my_consumer.calls == [
        ("aenter",),
        ("load",),
        ("aexit",),
    ]


@pytest.mark.asyncio
@patch("ska_sdp_qa_data_api.server.tasks.kafka_task.DataQueueConsumer")
async def test_kafka_single_message(mock_data_queue):
    """Check things work."""
    my_consumer = FakeDataQueue("my-topic", '{"id": "q"}', count=1)
    mock_data_queue.return_value = my_consumer

    kt = KafkaTask()
    kt.setup = AsyncMock()
    kt.on_exit = AsyncMock()
    kt.receive = AsyncMock()
    kt.topics = ["my-topic"]
    await kt.run_consumer()
    assert mock_data_queue.mock_calls == [
        call(
            server=BROKER_INSTANCE,
            set_group_id=False,
            auto_offset_reset="latest",
            set_consumer_offsets=False,
            encoding=Encoding.MSGPACK_NUMPY,
        )
    ]
    kt.setup.assert_called_once()
    kt.on_exit.assert_called_once()
    assert kt.receive.mock_calls == [call("my-topic", '{"id": "q"}')]
    assert my_consumer.calls == [
        ("aenter",),
        ("load",),
        ("aexit",),
    ]
