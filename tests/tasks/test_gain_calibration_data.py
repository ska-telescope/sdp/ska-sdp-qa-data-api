"""Test Task Receive Spectrum Waterfalls"""

import asyncio
from io import BytesIO
from unittest.mock import AsyncMock, call, patch

import numpy
import pytest

from ska_sdp_qa_data_api.server.tasks.gain_calibration_data import ReceiveGainCalibrationData
from tests.test_data.helpers import FakeDataQueue

# pylint: disable=protected-access


@pytest.mark.asyncio
@patch("ska_sdp_qa_data_api.server.data.calibration_data.time")
@patch("ska_sdp_qa_data_api.server.tasks.kafka_task.DataQueueConsumer")
@patch("ska_sdp_qa_data_api.server.tasks.gain_calibration_data.DataQueueProducer")
@patch("ska_sdp_qa_data_api.server.tasks.kafka_task.SDPConfig", autospec=True)
async def test_full(mock_config, mock_producer, mock_data_queue, mock_time):
    """Test the Calibration data flow."""

    mock_time.time.return_value = 123456.123

    shape = (5, 50, 4)
    n_ant, n_freq, n_pol = shape

    data = (
        numpy.array(
            [
                a * 100 + f + p * 1000
                for a in range(n_ant)
                for f in range(n_freq)
                for p in range(n_pol)
            ]
        )
        + 1.5j
    )

    # reform gain matrices as follows:
    gains = data.view(numpy.complex128).reshape(shape)
    bio = BytesIO()
    numpy.save(
        bio,
        gains.reshape(n_ant, n_freq, n_pol).view(numpy.float64),
    )

    mock_data_queue.return_value = FakeDataQueue("bandpass-gains-01", bio.getvalue())
    mock_producer.return_value = AsyncMock()

    task = ReceiveGainCalibrationData()

    async def set_topics(subarray):
        task.topics = ["bandpass-gains-01"]
        await task._data_queue.update_topics(task.topics)

    task.watch_for_subarrays = set_topics

    await task.run_consumer()
    await asyncio.sleep(1)

    # Using the default, because of the background task
    assert task.topics == ["bandpass-gains-01"]
    assert mock_config.mock_calls == []

    assert mock_data_queue.return_value.calls == [
        ("aenter",),
        ("update", ["bandpass-gains-01"]),
        ("load",),
        ("aexit",),
    ]
    assert mock_producer.return_value.mock_calls == [
        call._producer_start(),
        call.send(
            data={
                "time": [123456.123],
                "gains": [
                    [0.0, 1.5, 1000.0, 1.5, 2000.0, 1.5, 3000.0, 1.5],
                    [100.0, 1.5, 1100.0, 1.5, 2100.0, 1.5, 3100.0, 1.5],
                    [200.0, 1.5, 1200.0, 1.5, 2200.0, 1.5, 3200.0, 1.5],
                    [300.0, 1.5, 1300.0, 1.5, 2300.0, 1.5, 3300.0, 1.5],
                    [400.0, 1.5, 1400.0, 1.5, 2400.0, 1.5, 3400.0, 1.5],
                ],
                "phases": [
                    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                ],
            },
            topic="metrics-gain_calibration_out-01",
        ),
        call._producer_stop(),
    ]
