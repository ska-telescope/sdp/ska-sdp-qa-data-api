"""Test the auth functions."""

import httpx
import pytest
import respx
from starlette.requests import Request

from ska_sdp_qa_data_api.server.core.authorisation import (
    get_token_from_auth_header,
    get_token_from_request,
    user_profile,
)
from ska_sdp_qa_data_api.server.utils.exceptions import AuthError


@pytest.mark.asyncio
async def test_get_token_from_auth_header_no_header():
    """No header sent."""
    r = Request(
        scope={
            "type": "http",
            "headers": [],
        }
    )
    with pytest.raises(
        AuthError, match="authorization_header_missing: Authorization header is expected."
    ):
        await get_token_from_auth_header(r)
        assert False, "This should not hit"


@pytest.mark.asyncio
async def test_get_token_from_auth_header_no_token():
    """Header exists, but no token."""
    r = Request(
        scope={
            "type": "http",
            "headers": [("authorization".encode(), "Bearer".encode())],
        }
    )
    with pytest.raises(
        AuthError, match="invalid_header: Authorization header must be in the format Bearer token."
    ):
        await get_token_from_auth_header(r)
        assert False, "This should not hit"


@pytest.mark.asyncio
async def test_get_token_from_auth_header_not_bearer():
    """Wrong type of token"""
    r = Request(
        scope={
            "type": "http",
            "headers": [("authorization".encode(), "Random token".encode())],
        }
    )
    with pytest.raises(
        AuthError, match="invalid_header: Authorization header must be in the format Bearer token."
    ):
        await get_token_from_auth_header(r)
        assert False, "This should not hit"


@pytest.mark.asyncio
async def test_get_token_from_auth_header_success():
    """Correctly get a token"""
    r = Request(
        scope={
            "type": "http",
            "headers": [("authorization".encode(), "Bearer token".encode())],
        }
    )
    assert await get_token_from_auth_header(r) == "token"


@pytest.mark.asyncio
async def test_get_token_from_request_no_token():
    """Check that we get the required error."""
    r = Request(
        scope={
            "type": "http",
            "headers": [("authorization".encode(), "Random token".encode())],
        }
    )
    with pytest.raises(
        AuthError, match="invalid_header: Authorization header must be in the format Bearer token."
    ):
        await get_token_from_auth_header(r)
        assert False, "This should not hit"


@pytest.mark.asyncio
async def test_get_token_from_request():
    """Correctly get a token"""
    r = Request(
        scope={
            "type": "http",
            "headers": [("authorization".encode(), "Bearer token".encode())],
        }
    )
    assert await get_token_from_request(r) == "token"


@pytest.mark.asyncio
async def test_user_profile_none_token():
    """Return a None"""


@pytest.mark.asyncio
async def test_user_profile_raise_error():
    """Throw an exception."""
    r = Request(
        scope={
            "type": "http",
            "headers": [("authorization".encode(), "Random token".encode())],
        }
    )

    # pylint: disable=duplicate-code
    assert await user_profile(r) == {
        "user_profile": {
            "displayName": "Guest",
            "givenName": "Guest",
            "surname": "Guest",
            "userPrincipalName": "guest@community.skao.int",
            "id": "aaaabbbb-cccc-dddd-eeee-ffffgggghhhh",
        }
    }


@pytest.mark.asyncio
@respx.mock
async def test_user_profile(respx_mock):
    """Get the actual user."""
    r = Request(
        scope={
            "type": "http",
            "headers": [("authorization".encode(), "Bearer token".encode())],
        }
    )
    my_route = respx_mock.get("http://localhost:8000/v2/user/profile").mock(
        return_value=httpx.Response(204, json={"user_profile": {"display_name": "test_name"}})
    )

    user = await user_profile(r)

    assert my_route.called
    assert user == {"user_profile": {"display_name": "test_name"}}
