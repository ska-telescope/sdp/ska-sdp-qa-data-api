API Overview
============

This documentation is going to assume that you have the API setup somewhere. And that the base URL
is going to be http://localhost:8002 .

For local development you can use the `Swagger Documentation <http://localhost:8002/docs>`_ link.
Th Swagger documentation is also available for any deployed instance.


REST Endpoints
--------------

These endpoints can all be accessed directly from the browser, and only allow for ``GET`` requests.
This is only going to be a subset of the endpoints, for more details descriptions for to the Swagger
Documentation, on any install this can be retrieved by going to ``/docs``.

Ping
~~~~

This endpoint can be used to determine if the API is responding to requests.

.. code-block:: bash

    $ curl http://localhost:8002/ping
    {"ping":"live"}


Config
~~~~~~

This endpoint provides you with details on the current setup. It also lists some important endpoints
that can be used to get further details.

.. code-block:: bash

    $ curl http://localhost:8002/config

    {
      "api_format": "msgpack",
      "app_version": "0.26.0",
      "project_name": "SDP Signal Metrics API",
      "start_time": "2024-09-20T08:12:13.588802+00:00",
      "uptime": 1547.5995180606842,
      "paths": {
        "websocket": "/ws",
        "subarrays": "/stats/subarrays",
        "processing_blocks": "/stats/processing_block/blocks",
        "spead2_scans": "/stats/spead2/scans",
        "spectrogram_thumbnail_path": "/spectrograms/thumbnail_pb",
        "lag_plot_thumbnail_path": "/lag_plots/thumbnail_pb",
        "spectrum_waterfalls_thumbnail_path": "/spectrum_waterfalls/thumbnail_pb",
        "log_url": "/log-dashboards",
        "mask_data": "/tel_model/mask_data"
      },
      "topics": {
        "phase_and_amplitude": "metrics-phase_and_amplitude-<subarray>",
        "spectrograms": "metrics-spectrograms-<subarray>",
        "spectrum": "metrics-spectrum-<subarray>",
        "phase": "metrics-phase-<subarray>",
        "amplitude": "metrics-amplitude-<subarray>",
        "lagplot": "metrics-lagplot-<subarray>",
        "uvcoverage": "metrics-uvcoverage-<subarray>",
        "pointing-offsets": "pointing-offsets-<subarray>",
        "pointing_offset_out": "metrics-pointing_offset_out-<subarray>",
        "bandpass-gains": "bandpass-gains-<subarray>",
        "gain_calibration_out": "metrics-gain_calibration_out-<subarray>",
        "bandaveragedxcorr": "metrics-bandaveragedxcorr-<subarray>",
        "configuration": "ws"
      },
      "waterfall_plots": {
        "thumbnail_width": 512,
        "thumbnail_max_height": 164
      }
    }


From the above you can see the expected data format of the websocket data in ``api_format``. As well
as what the links are to the various endpoints. The ``topics`` key also lists the topics that are
available in the websocket.

Dashboard and Monitoring links
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This endpoint will give you links to where you can get the logs from Kibana and the namespace
monitoring in Grafana.

.. code-block:: bash

    $ curl http://localhost:8002/log-dashboards/01

    {
      "kibana": "https://k8s.stfc.skao.int/kibana/app/discover#/?_g=(filters:!(),refreshInterval:(pause:!t,value:0),time:(from:now-15m,to:now))&_a=(columns:!(kubernetes.namespace,kubernetes.pod.name,kubernetes.container.name,message),filters:!(),grid:(columns:(kubernetes.namespace:(width:(width:120)),kubernetes.pod.name:(width:(width:150)),kubernetes.container.name:(width:(width:120)))),index:cbb05bec-ed81-45f8-b11a-eab26a3df6b1,interval:auto,query:(language:kuery,query:'kubernetes.namespace: \"default\" or kubernetes.namespace: \"default-p\"'),sort:!(!('@timestamp',desc)))",
      "grafana": {
        "sdp": "https://k8s.stfc.skao.int/grafana/d/dad0e09f-32f5-4181-b273-c7a1017221ff/kubernetes-resource-monitoring-dashboard?orgId=1&refresh=5s&var-namespace=default",
        "pipeline": "https://k8s.stfc.skao.int/grafana/d/dad0e09f-32f5-4181-b273-c7a1017221ff/kubernetes-resource-monitoring-dashboard?orgId=1&refresh=5s&var-namespace=default-p"
      }
    }

Known Execution Blocks
~~~~~~~~~~~~~~~~~~~~~~

This endpoint will provide you with a list of the known execution blocks as well as which is the
current latest.

.. code-block:: bash

    $ curl http://localhost:8002/config/execution_blocks
    ["eb-test-20240913-95421"]

Known Subarrays
~~~~~~~~~~~~~~~

This endpoint will provide you with a list of the known Subarrays as well as which is the current
latest.

.. code-block:: bash

    $ curl http://localhost:8002/config/subarrays
    ["01"]


Known Processing Block IDs
~~~~~~~~~~~~~~~~~~~~~~~~~~

This endpoint will provide you with a list of the known Processing Block IDs as well as which is the
current latest.

.. code-block:: bash

    $ curl http://localhost:8002/config/processing_blocks
    ["pb-testrealtime-20240913-95421"]

WebSocket Endpoints
-------------------

These endpoints should be connected to using a websocket to get data. The format of the data in any
websocket call is dependent on the topic, based on the flow configuration.

The API can currently support ``json`` and ``msgpack`` formats (with ``msgpack`` being the most
common). Regardless of which format is chosen the output of the first message is always the same,
an object with a single key/value pair. That looks like  ``{"status": "connected"}``.

Once that is received the websocket will start sending data as is available.

(deprecated) The topics that can be accessed can be found in the :ref:`Config` endpoint, with what
is required at the end. For example if you want to get the Spectrum data from subarray ``01`` you
would connect to the topic ``metrics-spectrum-01``.

The topics that can be used should be retrieved by using the :ref:`Flows` endpoint.

The URL to get to a websocket is ``/ws/<topic>``.

Flows
-----

Flows are the method with which kafka topics are retrieved from configured data flows. The currently
setup topics are specified per flow entry.

.. code-block:: bash

    $ curl http://localhost:8002/config/flows
    [
      {
        "processing_block_id": "pb-testrealtime-20240913-95421",
        "name": "metrics-amplitude-01",
        "kind": "data-queue",
        "topic": "metrics-amplitude-01",
        "format": "msgpack_numpy",
        "metric": "amplitude",
        "nchan_avg": 50,
        "additional_windows": 5,
        "rounding_sensitivity": 5,
        "data_model": "MetricPayload"
      },
      ...
      {
        "processing_block_id": "pb-testrealtime-20240913-95421",
        "name": "metrics-uvcoverage-01",
        "kind": "data-queue",
        "topic": "metrics-uvcoverage-01",
        "format": "msgpack_numpy",
        "metric": "uvcoverage",
        "nchan_avg": 50,
        "additional_windows": 5,
        "rounding_sensitivity": 5,
        "data_model": "MetricPayload"
      }
    ]



User authentication
-------------------

The create_window endpoint need to have the authenticated requesting user profile details, to
obtain these the QA Display deployment need to be registered on the SKAO MS Entra instance. The
following details then need to be updated in the helm chart to enable the Signal Display to load
the secrets from the vault, and configure the API to request the user details from the
SKA Permissions API

- URL of a deployed instance of the SKA Permissions API:

.. code-block:: yaml

    api.skaPermissionsApiUrl: "http://ska-permissions-api.integration-ska-permissions-api.svc:80"

- The Signal display need to have the path to the vault where the REACT_APP_MSENTRA_CLIENT_ID &
  REACT_APP_MSENTRA_TENANT_ID is saved. When deployed, environment variables linked to the values
  client_id and tenant_id will be updated with the values from the vault.

.. code-block:: yaml

    display.vault.pathToSecretVault: kv/data/users/andre_odendaal/sdhp-stfc/ska-sdp-qa-display/integration
