Signal Display
==============

This documentation is going to assume that you have the Display setup somewhere. And that the base URL
is going to be http://localhost:3333

This connects to the Signal Data API via a WebSocket and REST API
and visualises the received Signal metrics (e.g, spectrum plot, waterfall plot etc) in real-time.

For complete documentation related to the display go `here
<https://developer.skao.int/projects/ska-sdp-qa-display/en/latest/>`_

