Helm Overview
=============

This is the documentation for the Signal Metrics Helm Chart.

The helm chart has quite a few different options, and can used in multiple use cases, on this page
you will find both the most common use cases as well as all the options.

SDP Usage
---------

In the SDP installation, we have other settings enabled and disabled. To be able to install the
Signal Helm chart in the same way as in SDP you would run the following:

.. code-block:: bash

    helm install qa-api ska/ska-sdp-qa \
      --namespace <deploy-namespace> \
      --set api.workerCount=2 \
      --set kafka.enabled=false \
      --set api.sdp_config.host=ska-sdp-etcd \
      --set api.sdp_config.port=2379 \
      --set config.kafka.overrideUrl=ska-sdp-kafka:9092

.. warning::

    An important note is that the API needs to know about the ETCD database. And as such the above ``sdp_config`` values are very important to have set correctly.

All Configuration Options
-------------------------

Currently the full list of configuration options are:

.. list-table::
    :widths: auto
    :header-rows: 1

    * - Value
      - Default
      - Comment
    * - ``global.sdp.processingNamespace``
      - ``sdp``
      - The name of the Processing Namespace, for monitoring URLs
    * - ``ingress.enabled``
      - ``true``
      - Whether the Ingress should be enabled
    * - ``ingress.pathStart``
      - ``"/signal"``
      - What the prefix for the path should be
    * - ``ingress.namespaced``
      - ``true``
      - Whether the namespace should be added to the ingress prefix
    * - ``api.enabled``
      - ``true``
      - If the API is enabled
    * - ``api.container``
      - ``artefact.skao.int/ska-sdp-qa-data-api``
      - The docker link of the API container
    * - ``api.version``
      - ``0.22.1``
      - The API version to use
    * - ``api.imagePullPolicy``
      - ``IfNotPresent``
      - The pull policy of the API container
    * - ``api.workerCount``
      - ``2``
      - The amount of processes to start up for the API
    * - ``api.allowProfiling``
      - ``false``
      - Should profiling be enabled
    * - ``api.messagetype``
      - ``"msgpack"``
      - What type of data should the websocket use?
    * - ``api.dataExpiryHours``
      - ``12``
      - What type of data should the websocket use?
    * - ``api.waterfallPlotsExpireSeconds``
      - ``3600``
      - How much history should waterfall plots have at maximum?
    * - ``api.thumbnailWidth``
      - ``500``
      - How wide should we generate thumbnails
    * - ``api.thumbnailHeight``
      - ``164``
      - How high should we generate thumbnails
    * - ``api.sdp_config.host``
      - ``ska-sdp-etcd``
      - The hostname for the SKA SDP Config DB
    * - ``api.sdp_config.port``
      - ``2379``
      - The port for the SKA SDP Config DB
    * - ``api.grafanaDashboard``
      - ``"dad0e09f-32f5-4181-b273-c7a1017221ff/kubernetes-resource-monitoring-dashboard"``
      - The ID of the Grafana Dasboard to link to
    * - ``api.grafanaBaseUrl``
      - ``"https://monitoring.skao.int"``
      - The host link for the Grafana monitoring server.
    * - ``api.kibanaBaseUrl``
      - ``"https://k8s.stfc.skao.int"``
      - The hostname of the Kibana monitoring server.
    * - ``api.skaPermissionsApiUrl``
      - ``"https://sdhp.stfc.skao.int/integration-ska-permissions-api/skapermissionsapi"``
      - The URL of the SKA Permissions API service.
    * - ``api.resources.requests.cpu``
      - ``1000m``
      - The requested minimum CPU usage
    * - ``api.resources.requests.memory``
      - ``600Mi``
      - The requested minimum memory usage
    * - ``api.resources.limits.cpu``
      - ``2000m``
      - The maximum CPU usage
    * - ``api.resources.limits.memory``
      - ``1024Mi``
      - The maximum memory usage
    * - ``display.enabled``
      - ``true``
      - If the display is enabled
    * - ``display.container``
      - ``artefact.skao.int/ska-sdp-qa-display``
      - The docker link for the Display container
    * - ``display.version``
      - ``0.12.0``
      - The version of the Display image to use
    * - ``display.imagePullPolicy``
      - ``IfNotPresent``
      - The pull policy for the Display image.
    * - ``display.workflowStatisticsIntervalSeconds``
      - ``10``
      - How often to pull the statistics
    * - ``display.skipPreflightCheck``
      - ``true``
      - If we should skip the preflight check
    * - ``display.vault.useVault``
      - ``true``
      - Enables the deployment to retrieve SPA API MS Entra registration details from the SKAO vault.
    * - ``display.vault.pathToSecretVault``
      - ``kv/data/users/andre_odendaal/sdhp-stfc/ska-sdp-qa-display/integration``
      - Path to the secrets in the vault.
    * - ``display.vault.client_id``
      - ``placeholder_for_client_id``
      - Placeholder env variable for MS Entra application registration client ID.
    * - ``display.vault.tenant_id``
      - ``placeholder_for_tenant_id``
      - Placeholder env variable for MS Entra application registration tenant ID.
    * - ``display.urls.override``
      - ``false``
      - If the URLs should be manually overridden
    * - ``display.urls.apiUrl``
      - ``"http://localhost:8002"``
      - The URL for the API if overridden.
    * - ``display.urls.wsAPI``
      - ``"ws://localhost:8002/consumer"``
      - The URL for the websocket if overriden
    * - ``config.kafka.override``
      - ``false``
      - Is the Kafka URL getting overriden
    * - ``config.kafka.overrideUrl``
      - ``""``
      - The new URL for Kafka
    * - ``config.redis.override``
      - ``false``
      - Is the redis URL getting overridden
    * - ``config.redis.overrideUrl``
      - ``""``
      - The new URL for redis
    * - ``kafka.enabled``
      - ``true``
      - Is the Kafka system enabled
    * - ``kafka.fullnameOverride``
      - ``ska-sdp-qa-kafka``
      - What should the name be of the Kafka instance
    * - ``kafka.maxMessageBytes``
      - ``"524288000"``
      - What is the maximum message size allowed
    * - ``kafka.socketRequestMaxBytes``
      - ``"524288000"``
      - What is the maximum payload to be sent to Kafka
    * - ``kafka.persistence.enabled``
      - ``false``
      - Should kafka have persistence
    * - ``kafka.zookeeper.fullnameOverride``
      - ``ska-sdp-qa-zookeeper``
      - What should zookeepers full name be
    * - ``kafka.zookeeper.persistence.enabled``
      - ``false``
      - Should zookeeper have persistence
    * - ``redis.enabled``
      - ``true``
      - Should redis be enabled
    * - ``redis.fullnameOverride``
      - ``ska-sdp-qa-redis``
      - What the full name should be of redis
    * - ``redis.architecture``
      - ``standalone``
      - Which architecture should be used
    * - ``redis.auth.enabled``
      - ``false``
      - Should auth be enabled
    * - ``redis.master.persistence.enabled``
      - ``false``
      - Should we use persistence
    * - ``redis.master.resources.requests.cpu``
      - ``1000m``
      - What the requested minimum CPU usage be
    * - ``redis.master.resources.requests.memory``
      - ``10240Mi``
      - What the requested minimum memory usage be
    * - ``redis.master.resources.limits.cpu``
      - ``1000m``
      - What the maximum CPU usage be
    * - ``redis.master.resources.limits.memory``
      - ``10240Mi``
      - What the maximum memory usage be

