Documentation for the SKA Signal Metrics Data API and Helm Chart
================================================================

The purpose of the Signal Display is to display any signal metrics received from telescope and
display them in a concise manner.

The Signal Display comes in three parts: the display, the API and the generator.

#. The generator receives the data in the form of XArray Datasets, processors the Visibility data into the metrics (using multi threads) and sends them via Kafka to the API.
   The generator is built as part of the `Realtime receive processors <https://gitlab.com/ska-telescope/sdp/ska-sdp-realtime-receive-processors/-/blob/main/src/realtime/receive/processors/sdp/metrics/signal_display_metrics.py?ref_type=heads>`_
#. The API acts as a middleware. It receives the data from the generator and sends the data on to the display.
#. The display then displays the data in different sections, as multiple graphs and general statistics.

The `ska-sdp-qa-data-api <https://gitlab.com/ska-telescope/sdp/ska-sdp-qa-data-api/-/blob/main/README.md>`_ repository contains the code for the main API image,
and also contains the Helm chart for this system.

The `ska-sdp-qa-display <https://gitlab.com/ska-telescope/sdp/ska-sdp-qa-display/-/blob/master/README.md>`_ repository contains the code for the main Signal Display image.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   changelog
   development
   api/overview
   helm/overview
   display/overview
