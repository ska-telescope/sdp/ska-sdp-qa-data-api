#!/bin/sh

while true; do
  echo "Heartbeat: $(date +"%Y-%m-%d %T.%N%z")"
  flock -n -E 0 /tmp/lock-trws signal-task-receive-waterfall-spectrogram --verbose &
  flock -n -E 0 /tmp/lock-trwl signal-task-receive-waterfall-lag --verbose &
  flock -n -E 0 /tmp/lock-tppc signal-task-process-pointing-calibration-receive --verbose &
  flock -n -E 0 /tmp/lock-tpgc signal-task-process-gain-calibration-receive --verbose &
  flock -n -E 0 /tmp/lock-trsw signal-task-receive-spectrum-waterfalls --verbose &
  flock -n -E 0 /tmp/lock-trc signal-task-redis-clean --verbose &
  sleep 60
done
