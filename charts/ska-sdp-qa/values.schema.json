{
  "type": "object",
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "required": ["global"],
  "properties": {
    "global": {
      "type": "object",
      "required": ["sdp"],
      "properties": {
        "sdp": {
          "type": "object",
          "required": ["processingNamespace"],
          "properties": {
            "processingNamespace": {
              "type": [
                "string"
              ],
              "default": "sdp"
            }
          }
        }
      }
    },
    "ingress": {
      "type": "object",
      "required": [],
      "properties": {
        "enabled": {
          "type": [
            "boolean"
          ],
          "default": "true"
        },
        "pathStart": {
          "type": [
            "string"
          ],
          "pattern": "^/[a-z0-9-_/]*$",
          "default": "\"/signal\""
        },
        "namespaced": {
          "type": [
            "boolean"
          ],
          "default": "true"
        },
        "hostname": {
          "type": [
            "string"
          ],
          "default": "\"https://sdhp.stfc.skao.int\""
        }
      }
    },
    "api": {
      "type": "object",
      "required": [],
      "properties": {
        "enabled": {
          "type": [
            "boolean"
          ],
          "default": "true"
        },
        "container": {
          "type": [
            "string"
          ],
          "default": "artefact.skao.int/ska-sdp-qa-data-api"
        },
        "version": {
          "type": [
            "string"
          ],
          "default": "0.29.0"
        },
        "imagePullPolicy": {
          "type": [
            "string"
          ],
          "pattern": "^(Always|Never|IfNotPresent)$",
          "default": "IfNotPresent"
        },
        "workerCount": {
          "type": [
            "integer"
          ],
          "minimum": 1,
          "exclusiveMaximum": 10,
          "default": "2"
        },
        "allowProfiling": {
          "type": [
            "boolean"
          ],
          "default": "false"
        },
        "dataExpiryHours": {
          "type": [
            "integer"
          ],
          "minimum": 1,
          "default": "12"
        },
        "waterfallPlotsExpireSeconds": {
          "type": [
            "integer"
          ],
          "minimum": 1,
          "default": "3600"
        },
        "thumbnailWidth": {
          "type": [
            "integer"
          ],
          "minimum": 50,
          "exclusiveMaximum": 1024,
          "default": "500"
        },
        "thumbnailHeight": {
          "type": [
            "integer"
          ],
          "minimum": 50,
          "exclusiveMaximum": 1024,
          "default": "164"
        },
        "sdp_config": {
          "type": "object",
          "required": [],
          "properties": {
            "host": {
              "type": [
                "string"
              ],
              "default": "ska-sdp-etcd"
            },
            "port": {
              "type": [
                "integer"
              ],
              "minimum": 1,
              "default": 2379
            }
          }
        },
        "messagetype": {
          "type": [
            "string"
          ],
          "pattern": "^(json|msgpack)$",
          "default": "\"msgpack\""
        },
        "grafanaDashboard": {
          "type": [
            "string"
          ],
          "default": "\"dad0e09f-32f5-4181-b273-c7a1017221ff/kubernetes-resource-monitoring-dashboard\""
        },
        "grafanaBaseUrl": {
          "type": [
            "string"
          ],
          "default": "\"https://monitoring.skao.int\""
        },
        "kibanaBaseUrl": {
          "type": [
            "string"
          ],
          "default": "\"https://k8s.stfc.skao.int\""
        },
        "skaPermissionsApiUrl": {
          "type": [
            "string"
          ],
          "default": "\"http://ska-permissions-api.integration-ska-permissions-api.svc:80\""
        },
        "resources": {
          "type": "object",
          "required": [],
          "properties": {
            "requests": {
              "type": "object",
              "required": [],
              "properties": {
                "cpu": {
                  "type": [
                    "string"
                  ],
                  "pattern": "^[0-9]+m$",
                  "default": "500m"
                },
                "memory": {
                  "type": [
                    "string"
                  ],
                  "pattern": "^[0-9]+Mi$",
                  "default": "1024Mi"
                }
              }
            },
            "limits": {
              "type": "object",
              "required": [],
              "properties": {
                "cpu": {
                  "type": [
                    "string"
                  ],
                  "pattern": "^[0-9]+m$",
                  "default": "500m"
                },
                "memory": {
                  "type": [
                    "string"
                  ],
                  "pattern": "^[0-9]+Mi$",
                  "default": "1024Mi"
                }
              }
            }
          }
        }
      }
    },
    "display": {
      "type": "object",
      "required": [],
      "properties": {
        "enabled": {
          "type": [
            "boolean"
          ],
          "default": "true"
        },
        "container": {
          "type": [
            "string"
          ],
          "default": "artefact.skao.int/ska-sdp-qa-display"
        },
        "version": {
          "type": [
            "string"
          ],
          "default": "0.19.0"
        },
        "imagePullPolicy": {
          "type": [
            "string"
          ],
          "pattern": "^(Always|Never|IfNotPresent)$",
          "default": "IfNotPresent"
        },
        "urls": {
          "type": "object",
          "required": [],
          "properties": {
            "override": {
              "type": [
                "boolean"
              ],
              "default": "false"
            },
            "apiUrl": {
              "type": [
                "string"
              ],
              "default": "\"http://localhost:8002\""
            },
            "wsAPI": {
              "type": [
                "string"
              ],
              "default": "\"ws://localhost:8002/consumer\""
            }
          }
        },
        "workflowStatisticsIntervalSeconds": {
          "type": [
            "integer"
          ],
          "minimum": 1,
          "exclusiveMaximum": 120,
          "default": "10"
        },
        "skipPreflightCheck": {
          "type": [
            "boolean"
          ],
          "default": "true"
        },
        "vault": {
          "type": "object",
          "required": [],
          "properties": {
            "useVault": {
              "type": [
                "boolean"
              ],
              "default": "true"
            },
            "pathToSecretVault": {
              "type": [
                "string"
              ],
              "default": "\"kv/data/users/andre_odendaal/sdhp-stfc/ska-sdp-qa-display/integration\""
            },
            "client_id": {
              "type": [
                "string"
              ],
              "default": "\"placeholder_for_client_id\""
            },
            "tenant_id": {
              "type": [
                "string"
              ],
              "default": "\"placeholder_for_tenant_id\""
            }
          }
        }
      }
    },
    "config": {
      "type": "object",
      "required": [],
      "properties": {
        "kafka": {
          "type": "object",
          "required": [],
          "properties": {
            "override": {
              "type": [
                "boolean"
              ],
              "default": "false"
            },
            "overrideUrl": {
              "type": [
                "string",
                "null"
              ]
            }
          }
        },
        "redis": {
          "type": "object",
          "required": [],
          "properties": {
            "override": {
              "type": [
                "boolean"
              ],
              "default": "false"
            },
            "overrideUrl": {
              "type": [
                "string",
                "null"
              ]
            }
          }
        }
      }
    },
    "kafka": {
      "type": "object",
      "required": [],
      "properties": {
        "enabled": {
          "type": [
            "boolean"
          ],
          "default": "true"
        },
        "fullnameOverride": {
          "type": [
            "string"
          ],
          "default": "ska-sdp-qa-kafka"
        },
        "maxMessageBytes": {
          "type": [
            "string"
          ],
          "default": "524288000"
        },
        "socketRequestMaxBytes": {
          "type": [
            "string"
          ],
          "default": "524288000"
        },
        "persistence": {
          "type": "object",
          "required": [],
          "properties": {
            "enabled": {
              "type": [
                "boolean"
              ],
              "default": "false"
            }
          }
        },
        "zookeeper": {
          "type": "object",
          "required": [],
          "properties": {
            "fullnameOverride": {
              "type": [
                "string"
              ],
              "default": "ska-sdp-qa-zookeeper"
            },
            "persistence": {
              "type": "object",
              "required": [],
              "properties": {
                "enabled": {
                  "type": [
                    "boolean"
                  ],
                  "default": "false"
                }
              }
            }
          }
        }
      }
    },
    "redis": {
      "type": "object",
      "required": [],
      "properties": {
        "enabled": {
          "type": [
            "boolean"
          ],
          "default": "true"
        },
        "fullnameOverride": {
          "type": [
            "string"
          ],
          "default": "ska-sdp-qa-redis"
        },
        "architecture": {
          "type": [
            "string"
          ],
          "default": "standalone"
        },
        "auth": {
          "type": "object",
          "required": [],
          "properties": {
            "enabled": {
              "type": [
                "boolean"
              ],
              "default": "false"
            }
          }
        },
        "master": {
          "type": "object",
          "required": [],
          "properties": {
            "persistence": {
              "type": "object",
              "required": [],
              "properties": {
                "enabled": {
                  "type": [
                    "boolean"
                  ],
                  "default": "false"
                }
              }
            },
            "resources": {
              "type": "object",
              "required": [],
              "properties": {
                "requests": {
                  "type": "object",
                  "required": [],
                  "properties": {
                    "cpu": {
                      "type": [
                        "string"
                      ],
                      "pattern": "^[0-9]+m$",
                      "default": "1000m"
                    },
                    "memory": {
                      "type": [
                        "string"
                      ],
                      "pattern": "^[0-9]+Mi$",
                      "default": "10240Mi"
                    }
                  }
                },
                "limits": {
                  "type": "object",
                  "required": [],
                  "properties": {
                    "cpu": {
                      "type": [
                        "string"
                      ],
                      "pattern": "^[0-9]+m$",
                      "default": "1000m"
                    },
                    "memory": {
                      "type": [
                        "string"
                      ],
                      "pattern": "^[0-9]+Mi$",
                      "default": "10240Mi"
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
