# Signal Metrics Helm Chart

This helm chart can spin up all requirements for the Signal Metrics System.

This can include the following:
* Signal Metrics Data API
* Signal Metrics Dashboard (currently defunct)
* Kafka Instance

## Values

| Value                                      | Type      | Required   | Default                 | Comment                                                   |
| ------------------------------------------ | --------- | ---------- | --------------          | --------------------------------------------------------- |
| qa_metrics.api.enabled                     | boolean   | Yes        | True                    | Should the API be created                                 |
| qa_metrics.api.container                   | string    | Yes        | link                    | The Docker image for the API                              |
| qa_metrics.api.version                     | string    | Yes        | latest                  | The version of the Docker image to use                    |
| qa_metrics.api.imagePullPolicy             | string    | Yes        | IfNotPresent            | When should the image be pulled                           |
| qa_metrics.dashboard.enabled               | boolean   | Yes        | False                   | Should the API be created                                 |
| qa_metrics.dashboard.container             | string    | Yes        | link                    | The Docker image for the Display                          |
| qa_metrics.dashboard.version               | string    | Yes        | latest                  | The version of the Docker image to use                    |
| qa_metrics.dashboard.imagePullPolicy       | string    | Yes        | IfNotPresent            | When should the image be pulled                           |
| qa_metrics.dashboard.apiUrl                | string    | Yes        | "http://localhost:8002" | When should the image be pulled                           |
| qa_metrics.kafka.enabled                   | boolean   | Yes        | True                    | Should Kafka be started                                   |
| qa_metrics.kafka.override                  | boolean   | No         | False                   | Should we override the kafka URL                          |
| qa_metrics.kafka.overrideUrl               | string    | No         | ""                      | The overridden Kafka URL                                  |
| kafka.maxMessageBytes                      | string    | No         | "62914560"              | The size of a maximum packet to be sent (~50MB)           |
| kafka.persistence.enabled                  | boolean   | No         | False                   | Whether Kafka should use a disk                           |
| kafka.persistence.storageClass             | string    | No         | "nfss1"                 | The storage class name to be used if enabled              |
| kafka.zookeeper.persistence.enabled        | boolean   | No         | False                   | Whether Zookeeper should use a disk                       |
| kafka.zookeeper.persistence.storageClass   | string    | No         | "nfss1"                 | The storage class name to be used if enabled              |


## Example Usage

This assumes you have the following setup:

* Minikube installed

Run the following (uses default namespace):

```bash
helm dependency build charts/ska-qa-metrics --skip-refresh
helm install qa-api charts/ska-qa-metrics --values values.yaml --wait
```

After awhile you should see the following as output:

```
NAME: qa-api
LAST DEPLOYED: Fri Jun 24 14:42:03 2022
NAMESPACE: default
STATUS: deployed
REVISION: 1
TEST SUITE: None
```

You can check the status of the pods with:

```bash
kubectl get pods --all-namespaces
```

And you should see:

```
$ kubectl get pods --all-namespaces
NAMESPACE     NAME                                   READY   STATUS    RESTARTS      AGE
default       qa-api-kafka-0                         1/1     Running   0             2m34s
default       qa-api-zookeeper-0                     1/1     Running   0             2m34s
default       qa-metric-api-0                        1/1     Running   0             2m34s
```

To be able to use this setup you can run a port forward with:

```bash
kubectl port-forward -n default qa-metric-api-0 8002:8002
```

Which will produce no output, but you should now be able to connect to port 8002 on your local machine and see the API.
