{{- define "ska-sdp-signal-display.kafka_url" }}
    {{- if $.Values.kafka.enabled }}
        {{- if $.Values.config.kafka.override }}
            {{- required "Overriden Kafka URL missing" $.Values.config.kafka.overrideUrl }}
        {{- else }}
            {{- printf "ska-sdp-qa-kafka.%s.svc:9092" .Release.Namespace }}
        {{- end }}
    {{- else }}
        {{- $.Values.config.kafka.overrideUrl }}
    {{- end }}
{{- end }}

{{- define "ska-sdp-signal-display.redis_url" }}
    {{- if $.Values.redis.enabled }}
        {{- if $.Values.config.redis.override }}
            {{- required "Overriden Redis URL missing" $.Values.config.redis.overrideUrl }}
        {{- else }}
            {{- printf "ska-sdp-qa-redis-headless.%s.svc" .Release.Namespace }}
        {{- end }}
    {{- else }}
        {{- $.Values.config.redis.overrideUrl }}
    {{- end }}
{{- end }}

{{- define "ska-sdp-signal-display.ingress_path_prepend" }}
    {{- if $.Values.ingress.namespaced }}
        {{- printf "/%s%s" .Release.Namespace .Values.ingress.pathStart }}
    {{- else }}
        {{- printf "%s" $.Values.ingress.pathStart }}
    {{- end }}
{{- end }}


{{- define "ska-sdp-signal-display.labels" }}
helm.sh/chart: "{{ .Chart.Name }}-{{ .Chart.Version }}"
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
component: signal-display-metrics
subsystem: execution-control
function: observation-monitoring
domain: science-data-processing
{{- end }}

{{- define "ska-sdp-signal-display.api_url_http" }}
    {{- if $.Values.display.urls.override }}
        {{- printf "%s" $.Values.display.urls.apiUrl }}
    {{- else }}
        {{- printf "%s/api" (include "ska-sdp-signal-display.ingress_path_prepend" .) }}
    {{- end }}
{{- end }}

{{- define "ska-sdp-signal-display.api_url_ws" }}
    {{- if $.Values.display.urls.override }}
        {{- printf "%s" $.Values.display.urls.wsAPI }}
    {{- else }}
        {{- printf "%s/api" (include "ska-sdp-signal-display.ingress_path_prepend" .) }}
    {{- end }}
{{- end }}
